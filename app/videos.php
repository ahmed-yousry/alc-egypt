<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class videos extends Model
{
  protected $table='videos';
  protected $fillable = [
      'course_id', 'video_link','video_type',
  ];

  public function get_courses()
  {
    
      return $this->belongsTo('App\courses','course_id');

  }


}

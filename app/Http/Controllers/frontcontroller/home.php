<?php

namespace App\Http\Controllers\frontcontroller;

use App\Http\Controllers\Controller;
use Facades\App\Helper\IceHelper;
use App\events;
use App\courses;
use App\Models\Slider;
use App\peoplesay;

use Illuminate\Http\Request;

class home extends Controller
{

  public function forntindex()
  {
      //
      $Slider =   Slider::all();
      $courses =   courses::all();
      $events = events::orderBy('id', 'DESC')->limit(3)->get();
      $events2 = events::orderBy('id', 'DESC')->get();

      $people =  peoplesay::orderBy('id', 'DESC')->get();

      return view('front.index',compact('Slider','courses','events','events2','people'));


  }



}

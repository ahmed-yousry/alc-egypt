<?php

namespace App\Http\Controllers\AdminController;

use App\Http\Controllers\Controller;
use Facades\App\Helper\IceHelper;
use App\videos;
use App\courses;
use Validator;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class videosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
//        $this->middleware('admin:admin');
    }




    public function index()
    {
        //
        $videos = videos::all();
        $courses = courses::all();

        return view('admin.videos.index',compact('videos','courses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

      $videos = videos::all();
      $courses = courses::all();

              return view('admin.videos.create',compact('videos','courses'));




    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request,[
        'course'=> 'required|max:50',
          'video_name' => 'required|max:50',
          'video_type' => 'required',
            'video' => 'required',
        //  'video' => 'required|mimes:mp4,mov,ogg,qt | max:150000'
      ]);

//   $this->validate(request(),['file.*'=>'required|video|mimes:mp4']);
//    //$this->validate(request(),['file'=>'required|array']);
//   $file = request()->file('video');
//
// $name = IceHelper::uplodevideo($file);

          $slider = new videos();
        $slider->course_id = $request->course;
        $slider->video_name = $request->video_name;

        $slider->video_link = $request->video;
          $slider->video_type = $request->video_type;
        $slider->save();
        return redirect('/admin/videos')->withFlashMessage('videos Added !!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function show($id)
    // {
    //     //
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //


        //

            $videos = videos::find($id);
              $courses = courses::all();
            return view('admin.videos.edit',compact('videos','courses'));



    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {


     $videos = videos::find($id);

    //     $validator = Validator::make($request->all(), [
    //     'video' => 'max:150000', //5MB
    // ]);

         // $file =  request()->file('video');
         // if (!empty($file)) {
         //   $name = IceHelper::uplodevideo($file);


                     videos::where('id', $id)->update(array(
                               'course_id' 	  =>  $request->course,
                               'video_name' 	  =>  $request->video_name,
                               'video_link' 	  => $request->video,

                               'video_type' 	  => $request->video_type


                   		));

         // }else {
         //
         //
         //   videos::where('id', $id)->update(array(
         //             'course_id' 	  =>  $request->course,
         //             'video_name' 	  =>  $request->video_name,
         //             'video_link' 	  =>  $videos->video_link,
         //             'video_type' 	  =>  $request->video_type
         //
         //
         //    ));
         // }
         //


        return redirect('/admin/videos')->withFlashMessage('videos Edited !!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        // if (Auth::guard('admin')->user()->can('sliders.delete')) {
            $slider = videos::find($id);
            //unLink(base_path().'/public/uploads/videos/'.$slider->videos);
            $slider->delete();
            return redirect()->back()->withFlashMessage('videos Deleted !!');
        // }
        // return redirect()->back();
    }
}

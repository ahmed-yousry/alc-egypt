<?php
namespace App\Http\Controllers\AdminController;
use App\Http\Controllers\Controller;
use Facades\App\Helper\IceHelper;
use App\servicess;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class servicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
//         $this->middleware('admin:admin');
    }


    public function index()
    {
        //
        $courses = servicess::all();

        return view('admin.services.index',compact('courses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        //
//        dd(auth()->user());
        // if (Auth::guard('admin')->user()->can('sliders.create')) {
            return view('admin.services.create');
        // }
        // return redirect()->back();

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[

            'photo' => 'required',
            'description'=>'required'
        ]);
//dd($request);
        $slider = new servicess();

        $slider->image = IceHelper::uploadImage($request->file('photo'),'services/');
        $slider->dscription = $request->description;

        $slider->save();
        return redirect('/admin/services')->withFlashMessage('services Added !!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //


        //

            $events = servicess::find($id);
            return view('admin.services.edit',compact('events'));



    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, $id)
    {
        //

        // $this->validate($request,[
        //     'title' => 'required|max:50',
        //     'description' => 'required'
        // ]);

        $slider = servicess::find($id);

        $file = $request->file('photo');

        if(!empty($file)){
            $slider->image = IceHelper::uploadImage($request->file('photo'),'services/');
              $slider->dscription = $request['description'];
        }else{
          $slider->image = $slider->image;
            $slider->dscription = $request['description'];


        }

        $slider->save();

        return redirect('/admin/services')->withFlashMessage('services Edited !!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        // if (Auth::guard('admin')->user()->can('sliders.delete')) {
            $courses = servicess::find($id);

          if (base_path().'/public/uploads/services/'.$courses->photo==null) {
          unLink(base_path().'/public/uploads/services/'.$courses->photo);
          }


            $courses->delete();
            return redirect()->back()->withFlashMessage('services Deleted !!');
        // }
        // return redirect()->back();
    }
}

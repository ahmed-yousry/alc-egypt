<?php

namespace App\Http\Controllers\AdminController;

use App\Http\Controllers\Controller;
use Facades\App\Helper\IceHelper;
use App\peoplesay;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class peopleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
//        $this->middleware('admin:admin');
    }
    public function index()
    {
        //
        $locale= \Illuminate\Support\Facades\App::setLocale('ar');
        $sliders = peoplesay::all();
        return view('admin.people.index',compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        //
//        dd(auth()->user());
        // if (Auth::guard('admin')->user()->can('sliders.create')) {
            return view('admin.people.create');
        // }
        // return redirect()->back();

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required|max:50',
            'description' => 'required',
            'photo' => 'required',
            'name' => 'required'
        ]);

        $slider = new peoplesay();
           $slider->name = $request->name;

        $slider->title = $request->title;
        $slider->description = $request->description;
        $slider->photo = IceHelper::uploadImage($request->file('photo'),'people/');
        $slider->save();
        return redirect('/admin/people')->withFlashMessage('people Added !!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //


        //

            $slider = peoplesay::find($id);
            return view('admin.people.edit',compact('slider'));



    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $this->validate($request,[
            'title' => 'required|max:50',
            'description' => 'required',
              'name' => 'required'
        ]);

        $slider = peoplesay::find($id);


           $slider->name = $request->name;

        $slider->title = $request->title;
        $slider->description = $request->description;


        if($file = $request->file('photo')){
            $slider->photo = IceHelper::uploadImage($request->file('photo'),'people/');
        }else{
            $slider->photo = $slider->photo;
        }
        $slider->save();

        return redirect('/admin/people')->withFlashMessage('Slider Edited !!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        // if (Auth::guard('admin')->user()->can('sliders.delete')) {
            $slider = peoplesay::find($id);
          //  unLink(base_path().'/public/uploads/people/'.$slider->photo);
            $slider->delete();
            return redirect()->back()->withFlashMessage('people Deleted !!');
        // }
        // return redirect()->back();
    }
}

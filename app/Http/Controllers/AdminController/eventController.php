<?php

namespace App\Http\Controllers\AdminController;

use App\Http\Controllers\Controller;
use Facades\App\Helper\IceHelper;
use App\events;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class eventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
//         $this->middleware('admin:admin');
    }


    public function index()
    {
        //
        $courses = events::all();

        return view('admin.event.index',compact('courses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        //
//        dd(auth()->user());
        // if (Auth::guard('admin')->user()->can('sliders.create')) {
            return view('admin.event.create');
        // }
        // return redirect()->back();

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required|max:60',
            'start_time' => 'required',
            'finish_time' => 'required',
            'address' => 'required',
            'photo' => 'required',
            'description'=>'required',
             'dates'=>'required',
              'month'=>'required',
        ]);

        $slider = new events();
          $slider->title = $request->title;


        $slider->start_time = $request->start_time;
        $slider->finish_time = $request->finish_time;
        $slider->address = $request->address;
           $slider->dates = $request->dates;
              $slider->month = $request->month;

        $slider->image = IceHelper::uploadImage($request->file('photo'),'events/');
        $slider->dscription = $request->description;

        $slider->save();
        return redirect('/admin/event')->withFlashMessage('event Added !!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //


        //

            $events = events::find($id);
            return view('admin.event.edit',compact('events'));



    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, $id)
    {
        //

        // $this->validate($request,[
        //     'title' => 'required|max:50',
        //     'description' => 'required'
        // ]);

        $slider = events::find($id);
        $slider->title = $request['title'];

        $slider->start_time = $request['start_time'];
        $slider->finish_time = $request['finish_time'];
                $slider->dates = $request['dates'];

        $slider->month = $request['month'];

        
        $slider->address = $request['address'];
        $file = $request->file('photo');

        if(!empty($file)){
            $slider->image = IceHelper::uploadImage($request->file('photo'),'events/');
              $slider->dscription = $request['description'];
        }else{
          $slider->image = $slider->image;
            $slider->dscription = $request['description'];


        }

        $slider->save();

        return redirect('/admin/event')->withFlashMessage('events Edited !!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        // if (Auth::guard('admin')->user()->can('sliders.delete')) {
            $courses = events::find($id);

          if (base_path().'/public/uploads/events/'.$courses->photo==null) {
          unLink(base_path().'/public/uploads/events/'.$courses->photo);
          }


            $courses->delete();
            return redirect()->back()->withFlashMessage('events Deleted !!');
        // }
        // return redirect()->back();
    }
}

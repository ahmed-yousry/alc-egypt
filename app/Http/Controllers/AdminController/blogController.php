<?php

namespace App\Http\Controllers\AdminController;

use App\Http\Controllers\Controller;
use Facades\App\Helper\IceHelper;
use App\blog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class blogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
//        $this->middleware('admin:admin');
    }
    public function index()
    {
        //

        $blogs = blog::all();

        return view('admin.blog.index',compact('blogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        //
//        dd(auth()->user());
        // if (Auth::guard('admin')->user()->can('sliders.create')) {
            return view('admin.blog.create');
        // }
        // return redirect()->back();

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required|max:50',
            'description' => 'required',
            'photo' => 'required'
        ]);

        $slider = new blog();


        $slider->title = $request->title;
        $slider->description = $request->description;
        $slider->image = IceHelper::uploadImage($request->file('photo'),'blog/');
        $slider->save();
        return redirect('/admin/blog')->withFlashMessage('blog Added !!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //


        //

            $blog= blog::find($id);
            return view('admin.blog.edit',compact('blog'));



    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $this->validate($request,[
            'title' => 'required|max:50',
            'description' => 'required'
        ]);

        $slider = blog::find($id);

        $slider->title       = $request['title'];
        $slider->description = $request['description'];

        if($file = $request->file('photo')){
            $slider->image = IceHelper::uploadImage($request->file('photo'),'blog/');
        }else{
            $slider->image = $slider->photo;
        }
        $slider->save();

        return redirect('/admin/blog')->withFlashMessage('blog Edited !!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        // if (Auth::guard('admin')->user()->can('sliders.delete')) {
            $slider = blog::find($id);
          //  unLink(base_path().'/public/uploads/blog/'.$slider->photo);
            $slider->delete();
            return redirect()->back()->withFlashMessage('blog Deleted !!');
        // }
        // return redirect()->back();
    }
}

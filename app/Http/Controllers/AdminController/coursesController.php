<?php

namespace App\Http\Controllers\AdminController;

use App\Http\Controllers\Controller;
use Facades\App\Helper\IceHelper;
use App\courses;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class coursesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
//        $this->middleware('admin:admin');
    }


    public function index()
    {
        //
        $courses = courses::all();
        return view('admin.courses.index',compact('courses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        //
//        dd(auth()->user());
        // if (Auth::guard('admin')->user()->can('sliders.create')) {
            return view('admin.courses.create');
        // }
        // return redirect()->back();

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $this->validate($request,[
        //     'courses_title' => 'required|max:60',
        //     'courses_description' => 'required',
        //     'photo' => 'required'
        // ]);

        $slider = new courses();


        $slider->courses_title = $request->courses_title;
        $slider->courses_description = $request->description;
        $slider->photo = IceHelper::uploadImage($request->file('photo'),'courses/');
        $slider->save();
        return redirect('/admin/courses')->withFlashMessage('courses Added !!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //


        //

            $courses = courses::find($id);
            return view('admin.courses.edit',compact('courses'));



    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $this->validate($request,[
            
            'description' => 'required'
        ]);

        $slider = courses::find($id);

        $slider->courses_title = $request['courses_title'];
        $slider->courses_description = $request['description'];
        $file = $request->file('photo');

        if(!empty($file)){

          //  unLink(base_path().'/public/uploads/courses/'.$slider->photo);
            $slider->photo = IceHelper::uploadImage($request->file('photo'),'courses/');



        }else{
            $slider->photo = $slider->photo;
        }
        $slider->save();

        return redirect('/admin/courses')->withFlashMessage('courses Edited !!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        // if (Auth::guard('admin')->user()->can('sliders.delete')) {
            $courses = courses::find($id);

          if (base_path().'/public/uploads/courses/'.$courses->photo==null) {
          unLink(base_path().'/public/uploads/courses/'.$courses->photo);
          }


            $courses->delete();
            return redirect()->back()->withFlashMessage('courses Deleted !!');
        // }
        // return redirect()->back();
    }
}

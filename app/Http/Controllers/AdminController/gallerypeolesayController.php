<?php

namespace App\Http\Controllers\AdminController;

use App\Http\Controllers\Controller;
use Facades\App\Helper\IceHelper;
use App\gallerypeolesay;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class gallerypeolesayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
//         $this->middleware('admin:admin');
    }


    public function index()
    {
        //
        $courses = gallerypeolesay::all();

        return view('admin.peolesay.index',compact('courses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        //
//        dd(auth()->user());
        // if (Auth::guard('admin')->user()->can('sliders.create')) {
            return view('admin.peolesay.create');
        // }
        // return redirect()->back();

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required|max:60',

            'photo' => 'required',

        ]);

        $slider = new gallerypeolesay();
        $slider->title = $request->title;



        $slider->image = IceHelper::uploadImage($request->file('photo'),'gallerypeolesay/');


        $slider->save();
        return redirect('/admin/gallery/peolesay')->withFlashMessage('gallery  Added !!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //


        //

            $events = gallerypeolesay::find($id);
            return view('admin.peolesay.edit',compact('events'));



    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, $id)
    {
        //

        // $this->validate($request,[
        //     'title' => 'required|max:50',
        //     'description' => 'required'
        // ]);

        $slider = gallerypeolesay::find($id);
        $slider->title = $request['title'];


        $file = $request->file('photo');

        if(!empty($file)){
            $slider->image = IceHelper::uploadImage($request->file('photo'),'gallerypeolesay/');

        }else{
          $slider->image = $slider->image;



        }

        $slider->save();

        return redirect('/admin/gallery/peolesay/')->withFlashMessage('image Edited !!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        // if (Auth::guard('admin')->user()->can('sliders.delete')) {
            $courses = gallerypeolesay::find($id);

          if (base_path().'/public/uploads/gallerypeolesay/'.$courses->photo==null) {
          unLink(base_path().'/public/uploads/gallerypeolesay/'.$courses->photo);
          }


            $courses->delete();
            return redirect()->back()->withFlashMessage('image Deleted !!');
        // }
        // return redirect()->back();
    }
}

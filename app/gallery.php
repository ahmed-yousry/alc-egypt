<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class gallery extends Model
{

 protected $table='gallery';
 protected $fillable = [
     'courses_title', 'courses_description',
 ];

 public function get_videos()
 {
     return $this->belongsTo(videos::class);
 }


}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class events extends Model
{


  protected $table='events';
 protected $fillable = [
     'month','dates','event_partner', 'start_time','finish_time','address','image','dscription'
 ];

 public function get_events()
 {
     return $this->belongsTo(videos::class);
 }


}

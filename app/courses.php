<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class courses extends Model
{

 protected $table='courses';
 protected $fillable = [
     'courses_title', 'courses_description',
 ];

 public function get_videos()
 {
     return $this->belongsTo(videos::class);
 }


}

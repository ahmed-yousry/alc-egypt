<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Psr\Container\ContainerInterface;
//use Auth;
use App\Notification;

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//for user routes
// Route::get('/', function () {
//     return view('front.index');
// });

Route::get('/','frontcontroller\home@forntindex');


Route::get('/account', function () {
    return view('front.account.index');
});


Route::get('/account', function () {
    return view('front.account.index');
});


Route::get('/register', function () {
    return view('front.account.register');
});

Route::get('/about', function () {
    return view('front.about-us.index');
});

Route::get('/courses','frontcontroller\course@forntindex');

Route::get('/courses','frontcontroller\course@forntindex');

Route::get('/services','frontcontroller\services@forntindex');


Route::get('learnpress/{id}','frontcontroller\learnpress@forntindex');
Route::get('learnpress/list/{id}','frontcontroller\lists@forntindex');

Route::post('contact/send','frontcontroller\contactsendmail@forntindex');


// Route::get('/gallery', function () {
//     return view('front.gallery.index');
// });

// Route::get('/events', function () {
//     return view('front.events.index');
// });

Route::get('/gallery','frontcontroller\gallerys@forntindex');



Route::get('/activities','frontcontroller\event@forntindex');

Route::get('/blog','frontcontroller\blogs@forntindex');

//
// Route::get('/blog', function () {
//     return view('front.blog.index');
// });

Route::get('/contact', function () {
    return view('front.contact/.index');
});



//SocialLite
Route::get('login/{provider}', 'AuthSocController@redirectToProvider');
Route::get('login/{provider}/callback', 'AuthSocController@handleProviderCallback');







/// USER Routes

Route::get('home/user/logout', function () {
    Auth()->guard('web')->logout();
    return redirect('/login');
});




///////////////////////////////////////////

//Admin Routes
Route::get('/admin/login', 'admin@index');
Route::post('/admin/login', 'admin@loginpost')->name('admin.login.submit');

Route::get('/admin/forget/password','admin@forgetpassword');
Route::get('/admin/reset/password/{token}','admin@resetpassword');
Route::post('/admin/reset/password/{token}','admin@postresetpassword');
Route::post('/admin/forget/password','admin@postforgetpassword');


Config::set('auth.defines','admin');
Route::get('admin/logout', function () {
    Auth()->guard('admin')->logout();
    return redirect('/admin/login');
});


/// user route
Route::group(['prefix' => LaravelLocalization::setLocale().'/home','middleware'=>'auth:web'],function(){





 Route::get('invoice/show/{search?}','userController\createinvoice@showinvoice');


 });






///admin route
 Route::group(['prefix' => LaravelLocalization::setLocale().'/admin','middleware'=>'admin:admin'],function(){







     Route::get('','AdminController\AdminController@index')->name('admin.dashboard');

     //Slider Route
          Route::resource('slider','AdminController\SliderController');
          Route::get('slider/{id}/delete','AdminController\SliderController@destroy');

   //Slider courses
          Route::resource('courses','AdminController\coursesController');
          Route::get('courses/{id}/delete','AdminController\coursesController@destroy');
  //Slider videos
                 Route::resource('videos','AdminController\videosController');
                 Route::get('videos/{id}/delete','AdminController\videosController@destroy');

  //events
  Route::resource('event','AdminController\eventController');
  Route::get('event/{id}/delete','AdminController\eventController@destroy');



  Route::resource('services','AdminController\servicesController');
  Route::get('services/{id}/delete','AdminController\servicesController@destroy');






  //blog
  Route::resource('blog','AdminController\blogController');
  Route::get('blog/{id}/delete','AdminController\blogController@destroy');
  //blog
  Route::resource('blog-vedio','AdminController\blogvedioController');
  Route::get('blog-vedio/{id}/delete','AdminController\blogvedioController@destroy');



  //Slider Route
       Route::resource('people','AdminController\peopleController');
       Route::get('people/{id}/delete','AdminController\peopleController@destroy');




//gallary peolesay
Route::resource('gallery/peolesay','AdminController\gallerypeolesayController');
Route::get('gallery/peolesay/{id}/delete','AdminController\gallerypeolesayController@destroy');


// gallary inclass
Route::resource('gallery/inclass','AdminController\galleryinclassController');
Route::get('gallery/inclass/{id}/delete','AdminController\galleryinclassController@destroy');
// gallary outclass
Route::resource('gallery/outclass','AdminController\galleryoutclassController');
Route::get('gallery/outclass/{id}/delete','AdminController\galleryoutclassController@destroy');


 });






/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//


Route::get('/ahmed',function (){



    dd(\Mcamara\LaravelLocalization\Facades\LaravelLocalization::getSupportedLocales());

});



Route::get('/order', function () {
    return view('front.order');
});

Route::get('/welcome/{locale}', function ($local) {
    \Illuminate\Support\Facades\App::setLocale($local);
    return view('welcome');
});





























//////////////////////////////////////////
/*
 *
 *

Route::group(['middleware'=>'news'],function(){


Route::get('insert', 'newscontroller@showdb');
Route::post('insertdb', 'newscontroller@insertdb');
});



Route::get('send/mail', function () {
//Mail::to('ayousry943@gmail.com')->send(new App\Mail\testmail());
//	\App\Jobs\sendmailjob::dispatch();

	$job = (new \App\Jobs\sendmailjob)->delay(\Carbon\Carbon::now()->addSeconds(1));
	dispatch($job);
	return  'mail sent';
});

 */

//Route::get('data/user', function () {
//
//if (Gate::allows('showdata',auth()->user())) {
// return  view('welcome');
//}else{
//  return  'you dont have pertmation  ';
//}
//});

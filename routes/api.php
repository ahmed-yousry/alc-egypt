<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/
//http://localhost/blog/public/index.php/api/user?api_token=150150
Route::group(['namespace'=>'Api'],function(){


Route::group(['middleware'=>'auth:api'],
	function(){
	Route::get('/user', function (Request $request) {
    return $request->user();
});
	



});
Route::get('login','users@login');
Route::get('getnews','apicontroller@get_news');
Route::get('getnews/{id}','apicontroller@getcomments');
Route::get('addcomment','apicontroller@addcomment');

Route::get('education',function (){
        $education = \App\EducationYear::all();
        return response($education);
    });
    Route::get('getGroups',function (){
        $groups = \App\Group::all();
        foreach ($groups as $g){
            $g->teacher = $g->teacher->fname;
            $g->subject = $g->subject->name;
            $g->classes   = $g->classes->name;
        }
        return response()->json($groups);
    });

    Route::post('searchGroup',function (Request $request){
        $search = $request->data;
        $groups = \App\Group::where('name','like',$request->data.'%')
            ->orWhere('name','like','%'.$request->data)
            ->orWhere('name','like','%'.$request->data.'%')
            ->orWhere('from','like','%'.$request->data.'%')
            ->orWhere('to','like','%'.$request->data.'%')
            ->orWhereHas('teacher', function ($query) use ($search) {
                $query->where('fname', 'like', '%' . $search . '%');
            })->orWhereHas('subject', function ($query) use ($search) {
                $query->where('name', 'like', '%' . $search . '%');
            })->get();
        if($groups){
            foreach ($groups as $g){
                $g->teacher = $g->teacher->fname;
                $g->subject = $g->subject->name;
                $g->classes   = $g->classes->name;
            }
        }

        return response()->json($groups);
    });

    Route::get('group/{id}/delete',function ($id){
        $group = \App\Group::destroy($id);
        return response()->json($group);
    });
Route::get('classes/{id}',function ($id){

        $education = \App\EducationYear::find($id);
        $classes = $education->classes;

        return response($classes);
    });
    Route::get('groups/{id}',function ($id){

        $class = \App\ClassRoom::find($id);
        $groups = $class->groups;
        foreach ($groups as $g){
            $g->teacher_id = $g->teacher->fname;
            $g->subject_id = $g->subject->name;

        }

        return response($groups);
    });
    Route::get('subjects/{id}',function ($id){

        $class = \App\ClassRoom::find($id);
        $subjects = $class->subjects;


        return response($subjects);
    });

    Route::get('teacher/{id}',function ($id){

        $subject = \App\Subject::find($id);
        $teachers = $subject->teachers;
        return response($teachers);
    });
    Route::get('student/{id}',function ($id){

        $education = \App\ClassRoom::find($id);
        $students = $education->students;
        $data=[];
        foreach ($students as $student){
            $s['id']= $student->id;
            $s['fname'] =$student->id.' - '.$student->fname;
            array_push($data,$s);
        }

        return response($data);
    });
    Route::post('addgroup',function (Request $request){
        $group = App\Group::create($request->all());
        return response($group,200);

    });
    Route::post('attendance-student',function (Request $request){

        $all = \App\AttendaceStudent::all();
        $group = \App\Group::find($request->group_id);
        $subject_id= $group->subject_id;
        foreach ($all as $a){
            if($request->group_id == $a->group_id && $request->student_id == $a->student_id && $request->date == $a->date){
                return response()->json("found",302);
            }
            if($a->student_id == $request->student_id && $a->date == $request->date && $a->group->subject_id == $subject_id){
                return response()->json("foundInSubject",302);

            }
        }

        $attent = \App\AttendaceStudent::create($request->all());
        if($attent){
            return response($attent,200);
        }
        return response($attent,404);
    });
    Route::get('test',function (){

    });

    Route::post('attendance-student/get',function (Request $request){

        $atts = \App\AttendaceStudent::all();
        $data=[];
        $studentTrue=[];
        foreach ($atts as $a ){
            if($a->group->subject_id == $request->subject_id && $a->group->class_room_id == $request->class_id &&
                $a->date == $request->date){
                $s['studentName']= $a->student->fname;
                $s['groupName']=$a->group->name;
                $s['studentID']=$a->student_id;
                array_push($studentTrue,$a->student_id);
                array_push($data,$s);
            }
        }
        $allStudent = \App\ClassRoom::find($request->class_id);
        $allStudent = $allStudent->students;
        $notStudent=[];
        foreach ($allStudent as $student){
            if(!in_array($student->id,$studentTrue)){
                array_push($notStudent,$student);
            }
        }
        $response=[
            'student'=>$data,
            'notStudent'=>$notStudent
        ];

        return response()->json($response);



//
//        $attent = \App\AttendaceStudent::where('group_id',$request->group_id)
//            ->where('date',$request->date)->get();
//        $data=[];
//        foreach ($attent as $s){
//            $student = \App\student::find($s->student_id);
//            $x['id']= $s->student_id;
//            $x['name'] = $student->fname;
//            array_push($data,$x);
//        }
//        return response($data);
    });


});

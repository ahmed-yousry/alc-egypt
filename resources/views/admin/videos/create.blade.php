@extends('admin.layouts.master')
add|videos
@section('page-header')
    <section class="content-header">
        <h1>
add videos           <small></small>
        </h1>

    </section>
@endsection

@section('content')

    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">   </h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->


                    <form class="form-horizontal" method="post"  action ="{{url('/admin/videos')}}"enctype="multipart/form-data">
                        {{csrf_field()}}
                    <div class="box-body">





                      <div class="form-group">

                    <label for="education_year" class="col-sm-4 control-label"> course name </label>

                      <div class="col-sm-4 {{ $errors->has('education_year') ? ' has-error' : '' }}">


                          <!--<input type="text" name="education_year" class="form-control" id="education_year" placeholder="قم الموبيل" value="{{ old('education_year') }}">-->
                          <select class="form-control" id="course" name="course">
                              <option> </option>

                                    @foreach($courses as $course)

                              <option value="{{$course->id}}"> {{$course->courses_title}} </option>


                          @endforeach


                    </select>


                          @if ($errors->has('course'))
                              <span class="help-block">
                    <strong>{{ $errors->first('course') }}</strong>
                    </span>
                          @endif
                      </div>

                    </div>




















                        <div class="form-group">
                    <label for="education_year" class="col-sm-4 control-label">  video name</label>

                        <div class="col-sm-4 {{ $errors->has('education_year') ? ' has-error' : '' }}">


              <input type="text" name="video_name" class="form-control"  placeholder="video name" value="{{ old('video_name') }}">



                  </select>


                            @if ($errors->has('video_name'))
                                <span class="help-block">
                    <strong>{{ $errors->first('video_name') }}</strong>
                    </span>
                            @endif
                        </div>

                    </div>




                    <div class="form-group">

                  <label for="education_year" class="col-sm-4 control-label"> video_type </label>

                    <div class="col-sm-4 {{ $errors->has('education_year') ? ' has-error' : '' }}">


                        <!--<input type="text" name="education_year" class="form-control" id="education_year" placeholder="قم الموبيل" value="{{ old('education_year') }}">-->
                        <select class="form-control"  name="video_type">
                            <option value="normal"> normal</option>
                            <option value="intro"> intro</option>





                  </select>


                        @if ($errors->has('video_type'))
                            <span class="help-block">
                  <strong>{{ $errors->first('video_type') }}</strong>
                  </span>
                        @endif
                    </div>

                  </div>








                  <div class="form-group">
              <label for="education_year" class="col-sm-4 control-label">  video link</label>

                  <div class="col-sm-4 {{ $errors->has('video') ? ' has-error' : '' }}">


        <input type="text" name="video" class="form-control"  placeholder="video link" value="{{ old('video') }}">



            </select>


                      @if ($errors->has('video'))
                          <span class="help-block">
              <strong>{{ $errors->first('video') }}</strong>
              </span>
                      @endif
                  </div>

              </div>









<div >

     </div>







                    </div>







                </div>










                    </div>



                    <div class="box-footer">
                    <button type="submit" class="btn btn-info center-block">save</button>
                    </div>



                    </form>
                </div>

            </div>
        </div>
    </section>

@endsection

@section('css')

    <link rel="stylesheet" href="{{ asset('assets/bower_components/select2/dist/css/select2.min.css')}}">
@endsection

@section('js')

    <script src="{{ asset('assets/bower_components/select2/dist/js/select2.min.js')}}"></script>


    <script>

  $('.select2').select2()


    </script>

    <script>
        $('#classes').change(function(event){

 var classes =  $('#classes').val();
$.ajax({
url:'{{url('admin/student/classes/classes').'/'}}'+classes,
data:classes,
async:false,
type:'get',
processData: false,
contentType: false,
success:function(data){
                $('#show_cities').html(data.html);
                console.log(data);


},

error: function (error) {
console.log(error);
}

});
});
    </script>
@endsection

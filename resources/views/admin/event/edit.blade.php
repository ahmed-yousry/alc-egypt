@extends('admin.layouts.master')
@section('title')
    Edit activities
@endsection
@section('page-header')
    <section class="content-header">
        <h1>
            edit activities
            <small></small>
        </h1>

    </section>
@endsection

@section('content')

    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">activities Form</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->

                    <form class="form-horizontal" method="post" action="{{url('/admin/event/'.$events->id)}}" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <input type="hidden" name="_method" value="patch">

                        <div class="box-body">

                          <div class="form-group">

                              <label for="title" class="col-sm-1 control-label">title</label>
                              <div class="col-sm-5 {{ $errors->has('title') ? ' has-error' : '' }}">
                                  <input type="text" name="title" class="form-control" id="title" placeholder="title" value="{{$events->title}}" required autofocus>
                                  @if ($errors->has('title'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('title') }}</strong>
                                      </span>
                                  @endif
                              </div>
                                </div>


                            <div class="form-group">

                                <label for="title" class="col-sm-1 control-label">start time</label>
                                <div class="col-sm-5 {{ $errors->has('title') ? ' has-error' : '' }}">
                                    <input type="text" name="start_time" class="form-control" id="title" placeholder="start time" value="{{$events->start_time}}" required autofocus>
                                    @if ($errors->has('start_time'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('start_time') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                  </div>

                                  <div class="form-group">
                                      <label for="title" class="col-sm-1 control-label">dates</label>
                                      <div class="col-sm-5 {{ $errors->has('title') ? ' has-error' : '' }}">
                                          <input type="text" name="dates" class="form-control" id="title" placeholder="dates" value="{{$events->dates}}" required autofocus>
                                          @if ($errors->has('dates'))
                                              <span class="help-block">
                                                  <strong>{{ $errors->first('dates') }}</strong>
                                              </span>
                                          @endif
                                      </div>
                                        </div>
                                        
                                        
                                           <div class="form-group">
                                      <label for="title" class="col-sm-1 control-label">month</label>
                                      <div class="col-sm-5 {{ $errors->has('title') ? ' has-error' : '' }}">
                                          <input type="text" name="month" class="form-control" id="title" placeholder="finish time" value="{{$events->month}}" required autofocus>
                                          @if ($errors->has('month'))
                                              <span class="help-block">
                                                  <strong>{{ $errors->first('month') }}</strong>
                                              </span>
                                          @endif
                                      </div>
                                        </div>



   <div class="form-group">
                                      <label for="title" class="col-sm-1 control-label">finish time</label>
                                      <div class="col-sm-5 {{ $errors->has('title') ? ' has-error' : '' }}">
                                          <input type="text" name="finish_time" class="form-control" id="title" placeholder="finish time" value="{{$events->finish_time}}" required autofocus>
                                          @if ($errors->has('finish_time'))
                                              <span class="help-block">
                                                  <strong>{{ $errors->first('finish_time') }}</strong>
                                              </span>
                                          @endif
                                      </div>
                                        </div>





                                        <div class="form-group">
                                            <label for="title" class="col-sm-1 control-label">address</label>
                                            <div class="col-sm-5 {{ $errors->has('title') ? ' has-error' : '' }}">
                                                <input type="text" name="address" class="form-control" id="address" placeholder="address" value="{{$events->address}}" required autofocus>
                                                @if ($errors->has('address'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('address') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                              </div>



                                <div class="form-group">
                                <label for="photo" class="col-sm-1 control-label">Photo</label>
                                <div class="col-sm-5 {{ $errors->has('photo') ? ' has-error' : '' }}">
                                    <input type="file" name="photo" id="photo" class="form-control">
                                    @if ($errors->has('photo'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('photo') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                </div>

                            <div class="form-group">

                                <label for="description" class="col-sm-1 control-label">Description</label>
                                <div class="col-sm-11 {{ $errors->has('description') ? ' has-error' : '' }}">
                                    <div class="box-body pad">
                                        <textarea name="description" class="form-control" placeholder="Description"  >  {{$events->dscription}}</textarea>
                                        @if ($errors->has('description'))
                                            <span class="help-block">
                                                    <strong>{{ $errors->first('description') }}</strong>
                                                </span>
                                        @endif
                                    </div>
                                </div>
                            </div>



                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-info center-block">Save <i class="fa fa-save" style="margin-left: 5px"></i></button>
                        </div>
                        <!-- /.box-footer -->

                    </form>

                </div>
                <!-- /.box -->
                <!-- general form elements disabled -->

                <!-- /.box -->
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>

@endsection

@section('css')

@endsection

@section('js')
    <script src="{{ asset('assets/bower_components/ckeditor/ckeditor.js')}}"></script>
    <script>
        $(function () {
            // Replace the <textarea id="editor1"> with a CKEditor
            // instance, using default configuration.
            CKEDITOR.replace('editor1')
            //bootstrap WYSIHTML5 - text editor
            $('.textarea').wysihtml5()
        })
    </script>

@endsection

<aside class="main-sidebar">


    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset('assets/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{\Illuminate\Support\Facades\Auth::guard('admin')->user()->username}}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li class="treeview">
            </li>

            @if(\Illuminate\Support\Facades\Auth::guard('admin')->user()->can('look'))
            <li class="treeview">
                <a href="{{url( LaravelLocalization::setLocale().'/admin/manager')}}">
                    <i class="fa fa-language"></i>
                    <span>slider</span>
                    <span class="pull-right-container">
            </span>
                </a>
                <ul class="treeview-menu">
                        <li>
                            <a rel="alternate" href="{{url( LaravelLocalization::setLocale().'/admin/slider')}}">
                                <i class="fa fa-circle-o"></i>  add slider
                            </a>
                        </li>

                </ul>
            </li>






            <li class="treeview">
                <a href="{{url( LaravelLocalization::setLocale().'/admin/manager')}}">
                    <i class="fa fa-language"></i>
                    <span>courses</span>
                    <span class="pull-right-container">
            </span>
                </a>
                <ul class="treeview-menu">
                        <li>
                            <a rel="alternate" href="{{url( LaravelLocalization::setLocale().'/admin/courses')}}">
                                <i class="fa fa-circle-o"></i>  add courses
                            </a>
                        </li>
                    <li>
                        <a rel="alternate" href="{{url( LaravelLocalization::setLocale().'/admin/videos')}}">
                            <i class="fa fa-circle-o"></i> add videos
                        </a>
                    </li>
                </ul>
            </li>








            <li class="treeview">
                <a href="{{url( LaravelLocalization::setLocale().'/admin/manager')}}">
                    <i class="fa fa-language"></i>
                    <span>activities</span>
                    <span class="pull-right-container">
            </span>
                </a>
                <ul class="treeview-menu">
                        <li>
                            <a rel="alternate" href="{{url( LaravelLocalization::setLocale().'/admin/event')}}">
                                <i class="fa fa-circle-o"></i>  add activities
                            </a>
                        </li>

                </ul>
            </li>



            <li class="treeview">
                <a href="{{url( LaravelLocalization::setLocale().'/admin/manager')}}">
                    <i class="fa fa-language"></i>
                    <span>services</span>
                    <span class="pull-right-container">
            </span>
                </a>
                <ul class="treeview-menu">
                        <li>
                            <a rel="alternate" href="{{url( LaravelLocalization::setLocale().'/admin/services')}}">
                                <i class="fa fa-circle-o"></i>  add services
                            </a>
                        </li>

                </ul>
            </li>










            <li class="treeview">
                <a href="{{url( LaravelLocalization::setLocale().'/admin/manager')}}">
                    <i class="fa fa-language"></i>
                    <span>gallery</span>
                    <span class="pull-right-container">
            </span>
                </a>
                <ul class="treeview-menu">
                        <li>
                            <a rel="alternate" href="{{url( LaravelLocalization::setLocale().'/admin/gallery/peolesay')}}">
                                <i class="fa fa-circle-o"></i>  add gallery people say
                            </a>
                        </li>
                    <li>
                        <a rel="alternate" href="{{url( LaravelLocalization::setLocale().'/admin/gallery/inclass')}}">
                            <i class="fa fa-circle-o"></i> add gallery in class
                        </a>
                    </li>

                    <li>
                        <a rel="alternate" href="{{url( LaravelLocalization::setLocale().'/admin/gallery/outclass')}}">
                            <i class="fa fa-circle-o"></i> add gallery out class
                        </a>
                    </li>
                </ul>
            </li>






            <li class="treeview">
                <a href="{{url( LaravelLocalization::setLocale().'/admin/manager')}}">
                    <i class="fa fa-language"></i>
                    <span>blog</span>
                    <span class="pull-right-container">
            </span>
                </a>
                <ul class="treeview-menu">
                        <li>
                            <a rel="alternate" href="{{url( LaravelLocalization::setLocale().'/admin/blog')}}">
                                <i class="fa fa-circle-o"></i>  add blog-image
                            </a>
                        </li>
                    <li>
                        <a rel="alternate" href="{{url( LaravelLocalization::setLocale().'/admin/blog-vedio')}}">
                            <i class="fa fa-circle-o"></i> add blog-vedio
                        </a>
                    </li>
                </ul>
            </li>


            <li class="treeview">
                <a href="{{url( LaravelLocalization::setLocale().'/admin/manager')}}">
                    <i class="fa fa-language"></i>
                    <span>people say</span>
                    <span class="pull-right-container">
            </span>
                </a>
                <ul class="treeview-menu">
                        <li>
                            <a rel="alternate" href="{{url( LaravelLocalization::setLocale().'/admin/people')}}">
                                <i class="fa fa-circle-o"></i>  add people say
                            </a>
                        </li>

                </ul>
            </li>









            @endif


    </section>
    <!-- radio -->

    <!-- /.sidebar -->
</aside>

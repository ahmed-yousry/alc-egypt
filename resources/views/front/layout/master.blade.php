<!DOCTYPE html>
<html itemscope itemtype="http://schema.org/WebPage" lang="en-US" prefix="og: http://ogp.me/ns#">
<!-- Mirrored from educationwp.thimpress.com/about-us/ by  Website Copier/3.x [XR&CO'2014], Sun, 11 Nov 2018 12:43:34 GMT -->
<!-- Added by  -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by  -->

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="pingback" href="../xmlrpc.php">
	<script type="text/javascript">
		var ajaxurl = "../wp-admin/admin-ajax.html";
	</script>

	<link type="text/css" media="screen" href="	 {{ asset('assets/wp-content/cache/autoptimize/1/css/autoptimize_bcb2f3a50e74b496f21b148cbe50d29c.css')}}
"
	 rel="stylesheet" />
	<style type="text/css" media="print">
		@media print {
			.pmpro_a-print {
				display: none;
				position: absolute;
				left: -9999px
			}
		}
    </style>
		
	<link type="text/css" media="all" href="    {{ asset('assets/wp-content/cache/autoptimize/1/css/autoptimize_d0c3f53760d5bcca06bcef61319ce777.css')}}
"

	 rel="stylesheet" />
     <title>@yield('title')</title>
	<link rel="canonical" href="index.html" />
	<meta property="og:locale" content="en_US" />
	<meta property="og:type" content="article" />
	<meta property="og:title" content="About Us - Education WP" />
	<meta property="og:description" content="Our StoryIt is a long established fact that a reade. 55000Foreign Followers 60Certified Teachers 2240Students Enrolled 215Complete Courses “Cras tristique turpis justo, eu consequat sem adipiscing ut. Donec posuere bibendum metus.” Tony Nguyen, Co-Founder Who We Are Lorem ipsum dolor &hellip;" />
	<meta property="og:url" content="localhost/about-us/" />
	<meta property="og:site_name" content="Education WP" />
    <meta property="og:image" content="{{ asset('assets/wp-content/wp-content/uploads/2015/11/slider-3.jpg')}}" />

	<meta property="og:image:secure_url" content="    {{ asset('assets/wp-content/wp-content/uploads/2015/11/slider-3.jpg')}}" />
	<meta property="og:image:width" content="1170" />
	<meta property="og:image:height" content="550" />
	<meta name="twitter:card" content="summary" />
	<meta name="twitter:description" content="Our StoryIt is a long established fact that a reade. 55000Foreign Followers 60Certified Teachers 2240Students Enrolled 215Complete Courses “Cras tristique turpis justo, eu consequat sem adipiscing ut. Donec posuere bibendum metus.” Tony Nguyen, Co-Founder Who We Are Lorem ipsum dolor &hellip;" />
	<meta name="twitter:title" content="About Us - Education WP" />
    <meta name="twitter:image" content=" {{ asset('assets/wp-content/uploads/2015/11/slider-3.jpg')}}" />

	<link rel='dns-prefetch' href='http://s.w.org/' />
	<link rel="alternate" type="application/rss+xml" title="Education WP &raquo; Feed" href="../feed/index.html" />
	<style id='woocommerce-inline-inline-css' type='text/css'>
		.woocommerce form .form-row .required {
			visibility: visible;
		}
	</style>
	<style id='rs-plugin-settings-inline-css' type='text/css'>
		#rs-demo-id {}
    </style>
      <script data-cfasync="false" src="{{ asset('assets/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js')}}"></script>

	<script type='text/javascript' src="   {{ asset('assets/wp-includes/js/jquery/jquery.js')}}"></script>
	<script type='text/javascript'>
		var userSettings = {
			"url": "\/",
			"uid": "0",
			"time": "1541939935",
			"secure": "1"
		};
	</script>
	<script type='text/javascript'>
		var panelsStyles = {
			"fullContainer": "body"
		};
	</script>
	<link rel='https://api.w.org/' href='../wp-json/index.html' />
	<link rel="EditURI" type="application/rsd+xml" title="RSD" href="../xmlrpc0db0.php?rsd" />
	<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="../wp-includes/wlwmanifest.xml" />
	<link rel='shortlink' href='../index0d26.html?p=2901' />
	<link rel="alternate" type="application/json+oembed" href="../wp-json/oembed/1.0/embed1734.json?url=https%3A%2F%2Feducationwp.thimpress.com%2Fabout-us%2F" />
	<link rel="alternate" type="text/xml+oembed" href="../wp-json/oembed/1.0/embedaca9?url=https%3A%2F%2Feducationwp.thimpress.com%2Fabout-us%2F&amp;format=xml" />
	<noscript>
		<style>.woocommerce-product-gallery{ opacity: 1 !important; }</style>
	</noscript>
	<script type="text/javascript">
		function tc_insert_internal_css(css) {
			var tc_style = document.createElement("style");
			tc_style.type = "text/css";
			tc_style.setAttribute('data-type', 'tc-internal-css');
			var tc_style_content = document.createTextNode(css);
			tc_style.appendChild(tc_style_content);
			document.head.appendChild(tc_style);
		}
	</script>
	<meta name="generator" content="Powered by Slider Revolution 5.4.8 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
	<style type="text/css" media="all" id="siteorigin-panels-layouts-head">
		/* Layout 2901 */
		#pgc-2901-0-0,
		#pgc-2901-2-0,
		#pgc-2901-4-0,
		#pgc-2901-5-0,
		#pgc-2901-6-0 {
			width: 100%;
			width: calc(100% - (0 * 30px))
		}

		#pl-2901 #panel-2901-0-0-0,
		#pl-2901 #panel-2901-1-0-0,
		#pl-2901 #panel-2901-1-1-0,
		#pl-2901 #panel-2901-1-2-0,
		#pl-2901 #panel-2901-1-3-0,
		#pl-2901 #panel-2901-2-0-0,
		#pl-2901 #panel-2901-2-0-1,
		#pl-2901 #panel-2901-3-0-0,
		#pl-2901 #panel-2901-3-2-0,
		#pl-2901 #panel-2901-5-0-0,
		#pl-2901 #panel-2901-6-0-0 {}

		#pg-2901-0,
		#pg-2901-1,
		#pg-2901-2,
		#pg-2901-4,
		#pg-2901-5,
		#pl-2901 .so-panel {
			margin-bottom: 30px
		}

		#pgc-2901-1-0,
		#pgc-2901-1-1,
		#pgc-2901-1-2,
		#pgc-2901-1-3 {
			width: 25%;
			width: calc(25% - (0.75 * 30px))
		}

		#pgc-2901-3-0,
		#pgc-2901-3-2 {
			width: 48%;
			width: calc(48% - (0.52 * 30px))
		}

		#pgc-2901-3-1 {
			width: 4%;
			width: calc(4% - (0.96 * 30px))
		}

		#pg-2901-3 {
			margin-bottom: 93px
		}

		#pl-2901 .so-panel:last-child {
			margin-bottom: 0px
		}

		#panel-2901-0-0-0>.panel-widget-style {
			padding: 30px 0px 0px
		}

		#pg-2901-4>.panel-row-style {
			background-image: url(https://localhost/empty-admin/wp-content/uploads/2015/11/bg-parallax-about-us.jpg);
			background-position: center center;
			background-repeat: no-repeat;
			min-height: 460px
		}

		@media (max-width:767px) {

			#pg-2901-0.panel-no-style,
			#pg-2901-0.panel-has-style>.panel-row-style,
			#pg-2901-1.panel-no-style,
			#pg-2901-1.panel-has-style>.panel-row-style,
			#pg-2901-2.panel-no-style,
			#pg-2901-2.panel-has-style>.panel-row-style,
			#pg-2901-3.panel-no-style,
			#pg-2901-3.panel-has-style>.panel-row-style,
			#pg-2901-4.panel-no-style,
			#pg-2901-4.panel-has-style>.panel-row-style,
			#pg-2901-5.panel-no-style,
			#pg-2901-5.panel-has-style>.panel-row-style,
			#pg-2901-6.panel-no-style,
			#pg-2901-6.panel-has-style>.panel-row-style {
				-webkit-flex-direction: column;
				-ms-flex-direction: column;
				flex-direction: column
			}

			#pg-2901-0 .panel-grid-cell,
			#pg-2901-1 .panel-grid-cell,
			#pg-2901-2 .panel-grid-cell,
			#pg-2901-3 .panel-grid-cell,
			#pg-2901-4 .panel-grid-cell,
			#pg-2901-5 .panel-grid-cell,
			#pg-2901-6 .panel-grid-cell {
				margin-right: 0
			}

			#pg-2901-0 .panel-grid-cell,
			#pg-2901-1 .panel-grid-cell,
			#pg-2901-2 .panel-grid-cell,
			#pg-2901-3 .panel-grid-cell,
			#pg-2901-4 .panel-grid-cell,
			#pg-2901-5 .panel-grid-cell,
			#pg-2901-6 .panel-grid-cell {
				width: 100%
			}

			#pgc-2901-1-0,
			#pgc-2901-1-1,
			#pgc-2901-1-2,
			#pgc-2901-3-0,
			#pgc-2901-3-1 {
				margin-bottom: 30px
			}

			#pl-2901 .panel-grid-cell {
				padding: 0
			}

			#pl-2901 .panel-grid .panel-grid-cell-empty {
				display: none
			}

			#pl-2901 .panel-grid .panel-grid-cell-mobile-last {
				margin-bottom: 0px
			}
		}
    </style>

	<link rel="icon" href="   {{ asset('assets/wp-content/uploads/2015/10/favicon.png')}}"
     sizes="32x32" />

	<link rel="icon" href="{{ asset('assets/wp-content/uploads/2015/10/favicon.png')}}"
     sizes="192x192" />



	<link rel="apple-touch-icon-precomposed" href="{{ asset('assets/wp-content/uploads/2015/10/favicon.png')}}" />
	<meta name="msapplication-TileImage" content="{{ asset('assets/wp-content/uploads/2015/10/favicon.png')}}" />
	<script type="text/javascript">
		function setREVStartSize(e) {
			try {
				e.c = jQuery(e.c);
				var i = jQuery(window).width(),
					t = 9999,
					r = 0,
					n = 0,
					l = 0,
					f = 0,
					s = 0,
					h = 0;
				if (e.responsiveLevels && (jQuery.each(e.responsiveLevels, function (e, f) {
						f > i && (t = r = f, l = e), i > f && f > r && (r = f, n = e)
					}), t > r && (l = n)), f = e.gridheight[l] || e.gridheight[0] || e.gridheight, s = e.gridwidth[l] || e.gridwidth[0] ||
					e.gridwidth, h = i / s, h = h > 1 ? 1 : h, f = Math.round(h * f), "fullscreen" == e.sliderLayout) {
					var u = (e.c.width(), jQuery(window).height());
					if (void 0 != e.fullScreenOffsetContainer) {
						var c = e.fullScreenOffsetContainer.split(",");
						if (c) jQuery.each(c, function (e, i) {
								u = jQuery(i).length > 0 ? u - jQuery(i).outerHeight(!0) : u
							}), e.fullScreenOffset.split("%").length > 1 && void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 ?
							u -= jQuery(window).height() * parseInt(e.fullScreenOffset, 0) / 100 : void 0 != e.fullScreenOffset && e.fullScreenOffset
							.length > 0 && (u -= parseInt(e.fullScreenOffset, 0))
					}
					f = u
				} else void 0 != e.minHeight && f < e.minHeight && (f = e.minHeight);
				e.c.closest(".rev_slider_wrapper").css({
					height: f
				})
			} catch (d) {
				console.log("Failure at Presize of Slider:" + d)
			}
		};
	</script>
	<script type="text/javascript">
		if (typeof ajaxurl === 'undefined') {

			var ajaxurl = "../wp-admin/admin-ajax.html";

		}
	</script>
	<script>
		! function (f, b, e, v, n, t, s) {
			if (f.fbq) return;
			n = f.fbq = function () {
				n.callMethod ?
					n.callMethod.apply(n, arguments) : n.queue.push(arguments);
			};
			if (!f._fbq) f._fbq = n;
			n.push = n;
			n.loaded = !0;
			n.version = '2.0';
			n.queue = [];
			t = b.createElement(e);
			t.async = !0;
			t.src = v;
			s = b.getElementsByTagName(e)[0];
			s.parentNode.insertBefore(t, s);
		}(window, document, 'script',
			'https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '978118969000044');
		fbq('track', 'PageView');
	</script> <noscript> <img height="1" width="1" src="https://www.facebook.com/tr?id=978118969000044&amp;ev=PageView&amp;noscript=1" />
	</noscript>
</head>

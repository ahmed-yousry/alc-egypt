<!DOCTYPE html>
<html itemscope itemtype="http://schema.org/WebPage" lang="en-US" prefix="og: http://ogp.me/ns#">
<!-- Mirrored from educationwp.thimpress.com/account/?action=register by   Website Copier/3.x [XR&CO'2014], Sun, 11 Nov 2018 12:39:48 GMT -->
<!-- Added by   -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by   -->

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="pingback" href="../xmlrpc.php">
	<script type="text/javascript">
		var ajaxurl = "../wp-admin/admin-ajax.html";
	</script>

	<link type="text/css" media="screen" href="	{{ asset('assets/wp-content/cache/autoptimize/1/css/autoptimize_bcb2f3a50e74b496f21b148cbe50d29c.css')}}"
	 rel="stylesheet" />
	<style type="text/css" media="print">
		@media print {
			.pmpro_a-print {
				display: none;
				position: absolute;
				left: -9999px
			}
		}
	</style>

	<link type="text/css" media="all" href="	{{ asset('assets/wp-content/cache/autoptimize/1/css/autoptimize_d0c3f53760d5bcca06bcef61319ce777.css')}}"
	 rel="stylesheet" />
	<title>Account - Education WP</title>
	<link rel="canonical" href="index.html" />
	<meta property="og:locale" content="en_US" />
	<meta property="og:type" content="article" />
	<meta property="og:title" content="Account - Education WP" />
	<meta property="og:description" content="Login with social networks Facebook Google Twitter LinkedIn Register Are you a member? Login now" />
	<meta property="og:url" content="localhost/account/" />
	<meta property="og:site_name" content="Education WP" />
	<meta name="twitter:card" content="summary" />
	<meta name="twitter:description" content="Login with social networks Facebook Google Twitter LinkedIn Register Are you a member? Login now" />
	<meta name="twitter:title" content="Account - Education WP" />
	<link rel='dns-prefetch' href='http://s.w.org/' />
	<link rel="alternate" type="application/rss+xml" title="Education WP &raquo; Feed" href="../feed/index.html" />
	<style id='woocommerce-inline-inline-css' type='text/css'>
		.woocommerce form .form-row .required {
			visibility: visible;
		}
	</style>
	<style id='rs-plugin-settings-inline-css' type='text/css'>
		#rs-demo-id {}
	</style>

	<script type='text/javascript' src="{{ asset('assets/wp-includes/js/jquery/jquery.js')}}"></script>
	<script type='text/javascript'>
		var userSettings = {
			"url": "\/",
			"uid": "0",
			"time": "1541939912",
			"secure": "1"
		};
	</script>
	<link rel='https://api.w.org/' href='../wp-json/index.html' />
	<link rel="EditURI" type="application/rsd+xml" title="RSD" href="../xmlrpc0db0.php?rsd" />
	<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="../wp-includes/wlwmanifest.xml" />
	<link rel='shortlink' href='../indexc04f.html?p=2958' />
	<link rel="alternate" type="application/json+oembed" href="../wp-json/oembed/1.0/embedc554.json?url=https%3A%2F%2Feducationwp.thimpress.com%2Faccount%2F" />
	<link rel="alternate" type="text/xml+oembed" href="../wp-json/oembed/1.0/embed53aa?url=https%3A%2F%2Feducationwp.thimpress.com%2Faccount%2F&amp;format=xml" />
	<noscript>
		<style>.woocommerce-product-gallery{ opacity: 1 !important; }</style>
	</noscript>
	<script type="text/javascript">
		function tc_insert_internal_css(css) {
			var tc_style = document.createElement("style");
			tc_style.type = "text/css";
			tc_style.setAttribute('data-type', 'tc-internal-css');
			var tc_style_content = document.createTextNode(css);
			tc_style.appendChild(tc_style_content);
			document.head.appendChild(tc_style);
		}
	</script>
	<meta name="generator" content="Powered by Slider Revolution 5.4.8 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
	<style type="text/css" media="all" id="siteorigin-panels-layouts-head">
		/* Layout 2958 */
		#pgc-2958-0-0,
		#pgc-2958-1-0 {
			width: 100%;
			width: calc(100% - (0 * 30px))
		}

		#pl-2958 #panel-2958-0-0-0,
		#pl-2958 #panel-2958-1-0-0 {}

		#pg-2958-0,
		#pl-2958 .so-panel {
			margin-bottom: 30px
		}

		#pl-2958 .so-panel:last-child {
			margin-bottom: 0px
		}

		@media (max-width:767px) {

			#pg-2958-0.panel-no-style,
			#pg-2958-0.panel-has-style>.panel-row-style,
			#pg-2958-1.panel-no-style,
			#pg-2958-1.panel-has-style>.panel-row-style {
				-webkit-flex-direction: column;
				-ms-flex-direction: column;
				flex-direction: column
			}

			#pg-2958-0 .panel-grid-cell,
			#pg-2958-1 .panel-grid-cell {
				margin-right: 0
			}

			#pg-2958-0 .panel-grid-cell,
			#pg-2958-1 .panel-grid-cell {
				width: 100%
			}

			#pl-2958 .panel-grid-cell {
				padding: 0
			}

			#pl-2958 .panel-grid .panel-grid-cell-empty {
				display: none
			}

			#pl-2958 .panel-grid .panel-grid-cell-mobile-last {
				margin-bottom: 0px
			}
		}
	</style>

	<link rel="icon" href="		{{ asset('assets/wp-content/uploads/2015/10/favicon.png')}}"
	 sizes="32x32" />
	<link rel="icon" href="		{{ asset('assets/wp-content/uploads/2015/10/favicon.png')}}"
	 sizes="192x192" />

	<link rel="apple-touch-icon-precomposed" href="{{ asset('assets/wp-content/uploads/2015/10/favicon.png')}}" />
	<meta name="msapplication-TileImage" content="localhost/wp-content/uploads/2015/10/favicon.png" />
	<script type="text/javascript">
		function setREVStartSize(e) {
			try {
				e.c = jQuery(e.c);
				var i = jQuery(window).width(),
					t = 9999,
					r = 0,
					n = 0,
					l = 0,
					f = 0,
					s = 0,
					h = 0;
				if (e.responsiveLevels && (jQuery.each(e.responsiveLevels, function (e, f) {
						f > i && (t = r = f, l = e), i > f && f > r && (r = f, n = e)
					}), t > r && (l = n)), f = e.gridheight[l] || e.gridheight[0] || e.gridheight, s = e.gridwidth[l] || e.gridwidth[0] ||
					e.gridwidth, h = i / s, h = h > 1 ? 1 : h, f = Math.round(h * f), "fullscreen" == e.sliderLayout) {
					var u = (e.c.width(), jQuery(window).height());
					if (void 0 != e.fullScreenOffsetContainer) {
						var c = e.fullScreenOffsetContainer.split(",");
						if (c) jQuery.each(c, function (e, i) {
								u = jQuery(i).length > 0 ? u - jQuery(i).outerHeight(!0) : u
							}), e.fullScreenOffset.split("%").length > 1 && void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 ?
							u -= jQuery(window).height() * parseInt(e.fullScreenOffset, 0) / 100 : void 0 != e.fullScreenOffset && e.fullScreenOffset
							.length > 0 && (u -= parseInt(e.fullScreenOffset, 0))
					}
					f = u
				} else void 0 != e.minHeight && f < e.minHeight && (f = e.minHeight);
				e.c.closest(".rev_slider_wrapper").css({
					height: f
				})
			} catch (d) {
				console.log("Failure at Presize of Slider:" + d)
			}
		};
	</script>
	<script type="text/javascript">
		if (typeof ajaxurl === 'undefined') {

			var ajaxurl = "../wp-admin/admin-ajax.html";

		}
	</script>
	<script>
		! function (f, b, e, v, n, t, s) {
			if (f.fbq) return;
			n = f.fbq = function () {
				n.callMethod ?
					n.callMethod.apply(n, arguments) : n.queue.push(arguments);
			};
			if (!f._fbq) f._fbq = n;
			n.push = n;
			n.loaded = !0;
			n.version = '2.0';
			n.queue = [];
			t = b.createElement(e);
			t.async = !0;
			t.src = v;
			s = b.getElementsByTagName(e)[0];
			s.parentNode.insertBefore(t, s);
		}(window, document, 'script',
			'https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '978118969000044');
		fbq('track', 'PageView');
	</script> <noscript> <img height="1" width="1" src="https://www.facebook.com/tr?id=978118969000044&amp;ev=PageView&amp;noscript=1" />
	</noscript>
</head>

<body class="page-template-default page page-id-2958 courses eduma learnpress learnpress-page woocommerce-no-js pmpro-body-has-access siteorigin-panels siteorigin-panels-before-js group-blog thim-body-preload bg-boxed-image"
 id="thim-body">
	<div id="preload">
		<div class="sk-folding-cube">
			<div class="sk-cube1 sk-cube"></div>
			<div class="sk-cube2 sk-cube"></div>
			<div class="sk-cube4 sk-cube"></div>
			<div class="sk-cube3 sk-cube"></div>
		</div>
	</div>
	<div id="wrapper-container" class="wrapper-container">
		<div class="content-pusher">


		@include('front.includes.header')


		@include('front.includes.nav')
			<div id="main-content">
				<section class="content-area">
					<div class="top_heading  _out">
						<div class="top_site_main " style="color: #ffffff;background-image:url(https://3ek5k1tux0822q3g83e30fye-wpengine.netdna-ssl.com/wp-content/themes/eduma/images/bg-page.jpg);">
							<span class="overlay-top-header" style="background:rgba(0,0,0,0.5);"></span>
							<div class="page-title-wrapper">
								<div class="banner-wrapper container">
									<h1>Register</h1>
								</div>
							</div>
						</div>
					</div>
					<div class="container site-content">
						<div class="row">
							<main id="main" class="site-main col-sm-12 full-width">
								<article id="post-2958" class="post-2958 page type-page status-publish hentry pmpro-has-access">
									<div class="entry-content">
										<div id="pl-2958" class="panel-layout">
											<div id="pg-2958-0" class="panel-grid panel-no-style">
												<div id="pgc-2958-0-0" class="panel-grid-cell">
													<script>
														jQuery(".btn-mo").prop("disabled", false);
													</script>
													<script type="text/javascript">
														function mo_openid_on_consent_change(checkbox, value) {

															if (value == 0) {
																jQuery('#mo_openid_consent_checkbox').val(1);
																jQuery(".btn-mo").attr("disabled", true);
																jQuery(".login-button").addClass("dis");
															} else {
																jQuery('#mo_openid_consent_checkbox').val(0);
																jQuery(".btn-mo").attr("disabled", false);
																jQuery(".login-button").removeClass("dis");
																//jQuery(".btn-mo").removeAttr("disabled");
															}
														}

														function moOpenIdLogin(app_name, is_custom_app) {
															var current_url = window.location.href;
															var cookie_name = "redirect_current_url";
															var d = new Date();
															d.setTime(d.getTime() + (2 * 24 * 60 * 60 * 1000));
															var expires = "expires=" + d.toUTCString();
															document.cookie = cookie_name + "=" + current_url + ";" + expires + ";path=/";

															var base_url = '../index.html';
															var request_uri = 'index0ddc.html?action=register';
															var http = 'https:///';
															var http_host = 'educationwp.thimpress.com';

															if (is_custom_app == 'false') {

																if (request_uri.indexOf('wp-login.html') != -1) {
																	var redirect_url = base_url + '/?option=getmosociallogin&app_name=';


																} else {
																	var redirect_url = http + http_host + request_uri;
																	if (redirect_url.indexOf('?') != -1) {
																		redirect_url = redirect_url + '&option=getmosociallogin&app_name=';

																	} else {
																		redirect_url = redirect_url + '?option=getmosociallogin&app_name=';

																	}
																}
															} else {

																if (request_uri.indexOf('wp-login.html') != -1) {
																	var redirect_url = base_url + '/?option=oauthredirect&app_name=';

																} else {
																	var redirect_url = http + http_host + request_uri;
																	if (redirect_url.indexOf('?') != -1)
																		redirect_url = redirect_url + '&option=oauthredirect&app_name=';
																	else
																		redirect_url = redirect_url + '?option=oauthredirect&app_name=';
																}

															}
															window.location.href = redirect_url + app_name;
														}
													</script> <br> <br />
													<div id="panel-2958-0-0-0" class="so-panel widget widget_text panel-first-child panel-last-child"
													 data-index="0">
														<div class="textwidget">
															<div class='mo-openid-app-icons'>
																<p style='color:#000000'> Register with social networks</p><a rel='nofollow' style='width: 240px !important;padding-top:11px !important;padding-bottom:11px !important;margin-bottom: -1px !important;border-radius: 4px !important;'
																 class='btn btn-mo btn-block btn-social btn-facebook btn-custom-dec login-button' onClick="moOpenIdLogin('facebook','false');">
																	<i style='padding-top:5px !important' class='fa fa-facebook'></i> Facebook</a>
																	<a rel='nofollow' href="{{url('/login/google')}}" style='width: 240px !important;padding-top:11px !important;padding-bottom:11px !important;margin-bottom: -1px !important;border-radius: 4px !important;'
																 class='btn btn-mo btn-block btn-social btn-google btn-custom-dec login-button' onClick="moOpenIdLogin('google','false');">
																	<i style='padding-top:5px !important' class='fa fa-google-plus'></i> Google</a>

																	<a rel='nofollow' href="" style='width: 240px !important;padding-top:11px !important;padding-bottom:11px !important;margin-bottom: -1px !important;border-radius: 4px !important;'
																 class='btn btn-mo btn-block btn-social btn-twitter btn-custom-dec login-button'  >
																	<i style='padding-top:5px !important' class='fa fa-twitter'></i> Twitter</a><a rel='nofollow' style='width: 240px !important;padding-top:11px !important;padding-bottom:11px !important;margin-bottom: -1px !important;border-radius: 4px !important;'
																 class='btn btn-mo btn-block btn-social btn-linkedin btn-custom-dec login-button' onClick="moOpenIdLogin('linkedin','false');">
																	<i style='padding-top:5px !important' class='fa fa-linkedin'></i> LinkedIn</a>
															</div> <br>
														</div>
													</div>
												</div>
											</div>
											<div id="pg-2958-1" class="panel-grid panel-no-style">
												<div id="pgc-2958-1-0" class="panel-grid-cell">
													<div id="panel-2958-1-0-0" class="so-panel widget widget_login-form panel-first-child panel-last-child"
													 data-index="1">
														<div class="thim-widget-login-form thim-widget-login-form-base">
															<div class="thim-login">
																<h2 class="title">Register</h2>
	<form class="auto_login" name="registerform" id="registerform" action="{{ url('register') }}"
																 method="post" novalidate="novalidate">
																   @csrf

														<p>
<input placeholder="Name" type="text" name="username" id="user_name" class="input {{ $errors->has('name') ? ' is-invalid' : '' }}" value="{{ old('name') }}" required autofocus />
  @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif





																	<p>
<input placeholder="Email" type="email" name="email" id="user_email" class="input {{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}" required autofocus />
  @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif



</p>
																	<p>
<input placeholder="Password" type="password" name="password" id="password" class="input {{ $errors->has('password') ? ' is-invalid' : '' }}" value="{{ old('password') }}" required autofocus  />

   @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif

</p>
																	<p>
	<input placeholder="Repeat Password" type="password" name="password_confirmation" id="repeat_password"
																		 class="input" /></p>
																	<p> <input type="hidden" name="redirect_to" value="indexa1fc.html?result=registered" /></p>
																	<p style="display: none"> <input type="text" id="check_spam_register" value="" name="name" /></p>
																	<input type='hidden' name='signup_form_id' value='1310577029' /><input type="hidden" id="_signup_form"
																	 name="_signup_form" value="7244902569" />
																	<p class="submit"> <input type="submit" name="wp-submit" id="wp-submit" class="button button-primary button-large"
																		 value="Sign up" /></p>
																</form>
																<p class="link-bottom">Are you a member? <a href="index.html">Login now</a></p>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</article>
							</main>
						</div>
					</div>
				</section>


@include('front.includes.footer')

			</div>
		</div> <a href="#" id="back-to-top"> <i class="fa fa-chevron-up" aria-hidden="true"></i> </a>
	</div>

	<div class="tp-preview-images"> <img src="#"
		 alt="preview image"></div>
	<div id="tp_chameleon_list_google_fonts"></div>
	<div class="gallery-slider-content"></div>

	<script data-cfasync="false" src="{{ asset('cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js')}}"></script>
	<script data-cfasync="false" type="text/javascript">
		window.onload = function () {
			var thim_preload = document.getElementById("preload");
			if (thim_preload) {
				setTimeout(function () {
					var body = document.getElementById("thim-body"),
						len = body.childNodes.length,
						class_name = body.className.replace(/(?:^|\s)thim-body-preload(?!\S)/, '').replace(
							/(?:^|\s)thim-body-load-overlay(?!\S)/, '');

					body.className = class_name;
					if (typeof thim_preload !== 'undefined' && thim_preload !== null) {
						for (var i = 0; i < len; i++) {
							if (body.childNodes[i].id !== 'undefined' && body.childNodes[i].id == "preload") {
								body.removeChild(body.childNodes[i]);
								break;
							}
						}
					}
				}, 500);
			}
		};
	</script>
	<script>
		window.addEventListener('load', function () {
			/**
			 * Fix issue there is an empty spacing between image and title of owl-carousel
			 */
			setTimeout(function () {
				var $ = jQuery;
				var $carousel = $('.thim-owl-carousel-post').each(function () {
					$(this).find('.image').css('min-height', 0);
					$(window).trigger('resize');
				});
			}, 500);
		})
	</script>
	<script data-cfasync="true" type="text/javascript">
		(function ($) {
			"use strict";
			$(document).on('click',
				'body:not(".logged-in") .enroll-course .button-enroll-course, body:not(".logged-in") .purchase-course:not(".guest_checkout, .learn-press-pmpro-buy-membership") .button',
				function (e) {
					e.preventDefault();
					$(this).parent().find('[name="redirect_to"]').val(
						'indexc6d2.html?redirect_to=localhost/account/?enroll-course=2958');
					var redirect = $(this).parent().find('[name="redirect_to"]').val();
					window.location = redirect;
				});
		})(jQuery);
	</script>
	<style type="text/css" media="all" id="siteorigin-panels-layouts-footer">
		/* Layout tc-megamenu-7682 */
		#pgc-tc-megamenu-7682-0-0,
		#pgc-tc-megamenu-7682-0-1 {
			width: 31%;
			width: calc(31% - (0.69 * 30px))
		}

		#pl-tc-megamenu-7682 #panel-tc-megamenu-7682-0-0-0,
		#pl-tc-megamenu-7682 #panel-tc-megamenu-7682-0-1-0,
		#pl-tc-megamenu-7682 #panel-tc-megamenu-7682-0-2-0 {}

		#pgc-tc-megamenu-7682-0-2 {
			width: 38%;
			width: calc(38% - (0.62 * 30px))
		}

		#pl-tc-megamenu-7682 .so-panel {
			margin-bottom: 30px
		}

		#pl-tc-megamenu-7682 .so-panel:last-child {
			margin-bottom: 0px
		}

		@media (max-width:767px) {

			#pg-tc-megamenu-7682-0.panel-no-style,
			#pg-tc-megamenu-7682-0.panel-has-style>.panel-row-style {
				-webkit-flex-direction: column;
				-ms-flex-direction: column;
				flex-direction: column
			}

			#pg-tc-megamenu-7682-0 .panel-grid-cell {
				margin-right: 0
			}

			#pg-tc-megamenu-7682-0 .panel-grid-cell {
				width: 100%
			}

			#pgc-tc-megamenu-7682-0-0,
			#pgc-tc-megamenu-7682-0-1 {
				margin-bottom: 30px
			}

			#pl-tc-megamenu-7682 .panel-grid-cell {
				padding: 0
			}

			#pl-tc-megamenu-7682 .panel-grid .panel-grid-cell-empty {
				display: none
			}

			#pl-tc-megamenu-7682 .panel-grid .panel-grid-cell-mobile-last {
				margin-bottom: 0px
			}
		}

		/* Layout w57e9cc2c86af4 */
		#pgc-w57e9cc2c86af4-0-0 {
			width: 33.3%;
			width: calc(33.3% - (0.667 * 30px))
		}

		#pl-w57e9cc2c86af4 #panel-w57e9cc2c86af4-0-0-0,
		#pl-w57e9cc2c86af4 #panel-w57e9cc2c86af4-0-0-1,
		#pl-w57e9cc2c86af4 #panel-w57e9cc2c86af4-0-1-0,
		#pl-w57e9cc2c86af4 #panel-w57e9cc2c86af4-0-2-0,
		#pl-w57e9cc2c86af4 #panel-w57e9cc2c86af4-0-3-0,
		#pl-w57e9cc2c86af4 #panel-w57e9cc2c86af4-0-4-0 {}

		#pgc-w57e9cc2c86af4-0-1,
		#pgc-w57e9cc2c86af4-0-2,
		#pgc-w57e9cc2c86af4-0-3,
		#pgc-w57e9cc2c86af4-0-4 {
			width: 16.675%;
			width: calc(16.675% - (0.83325 * 30px))
		}

		#pl-w57e9cc2c86af4 .so-panel {
			margin-bottom: 30px
		}

		#pl-w57e9cc2c86af4 .so-panel:last-child {
			margin-bottom: 0px
		}

		@media (max-width:767px) {

			#pg-w57e9cc2c86af4-0.panel-no-style,
			#pg-w57e9cc2c86af4-0.panel-has-style>.panel-row-style {
				-webkit-flex-direction: column;
				-ms-flex-direction: column;
				flex-direction: column
			}

			#pg-w57e9cc2c86af4-0 .panel-grid-cell {
				margin-right: 0
			}

			#pg-w57e9cc2c86af4-0 .panel-grid-cell {
				width: 100%
			}

			#pgc-w57e9cc2c86af4-0-0,
			#pgc-w57e9cc2c86af4-0-1,
			#pgc-w57e9cc2c86af4-0-2,
			#pgc-w57e9cc2c86af4-0-3 {
				margin-bottom: 30px
			}

			#pl-w57e9cc2c86af4 .panel-grid-cell {
				padding: 0
			}

			#pl-w57e9cc2c86af4 .panel-grid .panel-grid-cell-empty {
				display: none
			}

			#pl-w57e9cc2c86af4 .panel-grid .panel-grid-cell-mobile-last {
				margin-bottom: 0px
			}
		}
	</style>
	<script type="text/javascript">
		var c = document.body.className;
		c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
		document.body.className = c;
	</script>

	<script type='text/javascript'>
		var thim_js_translate = {
			"login": "Username",
			"password": "Password",
			"close": "Close"
		};
	</script>
	<script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js?ver=3.0.22'></script>
	<script type='text/javascript'>
		WebFont.load({
			google: {
				families: ['Roboto Slab:300,400,700', 'Roboto:300,300i,400,400i,500,500i,700,700i']
			}
		});
	</script>




	<script type="text/javascript" defer src="https://3ek5k1tux0822q3g83e30fye-wpengine.netdna-ssl.com/wp-content/cache/autoptimize/1/js/autoptimize_9d579daab1d45e1d6e4f6ff0c4d9af5b.js"></script>
</body>

</html>

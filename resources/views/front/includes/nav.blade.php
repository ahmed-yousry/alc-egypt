
			<nav class="mobile-menu-container mobile-effect">
				<ul class="nav navbar-nav">
          <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7682 tc-menu-item tc-menu-depth-0 tc-menu-align-left tc-menu-layout-builder"><a
             href="{{url('/') }}" class="tc-menu-inner">Home</a>

					<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7682 tc-menu-item tc-menu-depth-0 tc-menu-align-left tc-menu-layout-builder"><a
						 href="{{url('courses')}}" class="tc-menu-inner">Courses</a>

             <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7682 tc-menu-item tc-menu-depth-0 tc-menu-align-left tc-menu-layout-builder"><a
                href="{{url('about')}}" class="tc-menu-inner">About-us</a>

								<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7682 tc-menu-item tc-menu-depth-0 tc-menu-align-left tc-menu-layout-builder"><a
									 href="{{url('services')}}" class="tc-menu-inner">Services and Facilities</a>


					<li class="menu-item menu-item-type-post_type_archive menu-item-object-tp_event menu-item-7679 tc-menu-item tc-menu-depth-0 tc-menu-align-left tc-menu-layout-default"><a
						 href="{{url('activities')}}" class="tc-menu-inner">Activities </a></li>
					<li class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-4526 current_page_item menu-item-4528 tc-menu-item tc-menu-depth-0 tc-menu-align-left tc-menu-layout-default"><a
						 href="{{url('gallery')}}" class="tc-menu-inner">Gallery</a></li>
					<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-127 tc-menu-item tc-menu-depth-0 tc-menu-align-left tc-menu-layout-default"><a
						 href="{{url('blog')}}" class="tc-menu-inner">Blog</a></li>
					<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-99 tc-menu-item tc-menu-depth-0 tc-menu-align-left tc-menu-layout-default"><a
						 href="{{url('contact')}}" class="tc-menu-inner">Contact</a></li>

				</ul>
			</nav>

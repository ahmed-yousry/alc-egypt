                <footer id="colophon" class=" site-footer has-footer-bottom">
                    <div class="footer">
                        <div class="container">
                            <div class="row">
                                <aside id="siteorigin-panels-builder-9" class="widget widget_siteorigin-panels-builder footer_widget">
                                    <div id="pl-w57e9cc2c86af4" class="panel-layout">
                                        <div id="pg-w57e9cc2c86af4-0" class="panel-grid panel-no-style">
                                            <div id="pgc-w57e9cc2c86af4-0-0" class="panel-grid-cell">
                                                <div id="panel-w57e9cc2c86af4-0-0-0" class="so-panel widget widget_black-studio-tinymce widget_black_studio_tinymce panel-first-child"
                                                 data-index="0">
                                                    <div class="textwidget">
                                                        <div class="thim-footer-location">
                                                            <p>
                        <img class="alignnone size-full wp-image-10" src="{{ asset('assets/wp-content/uploads/2015/11/logo.png')}}
"



                                                             alt="logo-eduma-the-best-lms-wordpress-theme"
                                                                 width="145" height="40" /></p>
                                                            <p class="info"><i class="fa fa-phone"></i><a href="tel:00123456789">(03) 3931507</a></p>
                                                            <p class="info"><i class="fa fa-envelope"></i><a ><span
                                                                     class="__cf_email__" > </span> info@acl-egypt.com</a></p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="panel-w57e9cc2c86af4-0-0-1" class="so-panel widget widget_social panel-last-child" data-index="1">
                                                    <div class="thim-widget-social thim-widget-social-base">
                                                        <div class="thim-social">
                                                            <ul class="social_link">
                                                                <li><a class="facebook hasTooltip" href="https://www.facebook.com/Alexandria-Centre-For-Languages-587844001352285/" target="_self"><i class="fa fa-facebook"></i></a></li>
                                                                <!-- <li><a class="twitter hasTooltip" href="https://twitter.com/thimpress" target="_self"><i class="fa fa-twitter"></i></a></li>
                                                                <li><a class="google-plus hasTooltip" href="https://plus.google.com/+Thimpress" target="_self"><i class="fa fa-google-plus"></i></a></li>
                                                                <li><a class="pinterest hasTooltip" href="#" target="_self"><i class="fa fa-pinterest"></i></a></li> -->
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="pgc-w57e9cc2c86af4-0-1" class="panel-grid-cell">
                                                <div id="panel-w57e9cc2c86af4-0-1-0" class="so-panel widget widget_nav_menu panel-first-child panel-last-child"
                                                 data-index="2">
                                                    <h3 class="widget-title">Company</h3>
                                                    <div class="menu-company-container">
                                                        <ul id="menu-company" class="menu">
                                                            <li id="menu-item-3421" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3421 tc-menu-item tc-menu-depth-0 tc-menu-layout-default"><a
                                                                 href="{{url('about')}}" class="tc-menu-inner">About Us</a></li>
                                                            <li id="menu-item-3424" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3424 tc-menu-item tc-menu-depth-0 tc-menu-layout-default"><a
                                                                 href="{{url('blog')}}" class="tc-menu-inner">Blog</a></li>
                                                            <li id="menu-item-3422" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3422 tc-menu-item tc-menu-depth-0 tc-menu-layout-default"><a
                                                                 href="{{url('contact')}}" class="tc-menu-inner">Contact</a></li>

                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="pgc-w57e9cc2c86af4-0-2" class="panel-grid-cell">
                                                <div id="panel-w57e9cc2c86af4-0-2-0" class="so-panel widget widget_nav_menu panel-first-child panel-last-child"
                                                 data-index="3">
                                                    <h3 class="widget-title">Links</h3>
                                                    <div class="menu-links-container">
                                                        <ul id="menu-links" class="menu">
                                                            <li id="menu-item-5749" class="menu-item menu-item-type-post_type_archive menu-item-object-lp_course menu-item-5749 tc-menu-item tc-menu-depth-0 tc-menu-layout-default"><a
                                                                 href="{{url('courses')}}" class="tc-menu-inner">Courses</a></li>
                                                            <li id="menu-item-3426" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3426 tc-menu-item tc-menu-depth-0 tc-menu-layout-default"><a
                                                                 href="{{url('activities')}}" class="tc-menu-inner">Activities</a></li>
                                                            <li id="menu-item-4591" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4591 tc-menu-item tc-menu-depth-0 tc-menu-layout-default"><a
                                                                 href="{{url('gallery')}}" class="tc-menu-inner">Gallery</a></li>

                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="pgc-w57e9cc2c86af4-0-3" class="panel-grid-cell">
                                                <div id="panel-w57e9cc2c86af4-0-3-0" class="so-panel widget widget_nav_menu panel-first-child panel-last-child"
                                                 data-index="4">
                                                    <h3 class="widget-title">Support</h3>
                                                    <div class="menu-support-container">
                                                        <ul id="menu-support" class="menu">
                                                            <li id="menu-item-1757" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1757 tc-menu-item tc-menu-depth-0 tc-menu-align-left tc-menu-layout-default"><a
                                                                 href="http://docspress.thimpress.com/eduma/" class="tc-menu-inner">Documentation</a></li>
                                                            <li id="menu-item-1758" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1758 tc-menu-item tc-menu-depth-0 tc-menu-align-left tc-menu-layout-default"><a
                                                                 href="https://thimpress.com/forums/" class="tc-menu-inner">Forums</a></li>
                                                            <li id="menu-item-1759" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1759 tc-menu-item tc-menu-depth-0 tc-menu-align-left tc-menu-layout-default"><a
                                                                 href="#" class="tc-menu-inner">Language Packs</a></li>
                                                            <li id="menu-item-1760" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1760 tc-menu-item tc-menu-depth-0 tc-menu-align-left tc-menu-layout-default"><a
                                                                 href="#" class="tc-menu-inner">Release Status</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- <div id="pgc-w57e9cc2c86af4-0-4" class="panel-grid-cell">
                                                <div id="panel-w57e9cc2c86af4-0-4-0" class="so-panel widget widget_nav_menu panel-first-child panel-last-child"
                                                 data-index="5">
                                                    <h3 class="widget-title">Recommend</h3>
                                                    <div class="menu-recommend-container">
                                                        <ul id="menu-recommend" class="menu">
                                                            <li id="menu-item-6718" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-6718 tc-menu-item tc-menu-depth-0 tc-menu-layout-default"><a
                                                                 href="https://themeforest.net/item/education-wordpress-theme-education-wp/14058034?ref=ThimPress&amp;utm_source=demo&amp;utm_medium=demofooter"
                                                                 class="tc-menu-inner">WordPress</a></li>
                                                            <li id="menu-item-6719" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-6719 tc-menu-item tc-menu-depth-0 tc-menu-layout-default"><a
                                                                 href="https://wordpress.org/plugins/learnpress/" class="tc-menu-inner">LearnPress</a></li>
                                                            <li id="menu-item-3429" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3429 tc-menu-item tc-menu-depth-0 tc-menu-layout-default"><a
                                                                 href="shop/index.html" class="tc-menu-inner">WooCommerce</a></li>
                                                            <li id="menu-item-5930" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-5930 tc-menu-item tc-menu-depth-0 tc-menu-layout-default"><a
                                                                 href="forums/index.html" class="tc-menu-inner">bbPress</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div> -->
                                        </div>
                                    </div>
                                </aside>
                            </div>
                        </div>
                    </div>
                    <div class="copyright-area">
                        <div class="container">
                            <div class="copyright-content">
                                <div class="row">
                                    <div class="col-sm-6">
                                  
                                    </div>
                                    <div class="col-sm-6 text-right">
                                        <aside id="nav_menu-14" class="widget widget_nav_menu">
                                            <div class="menu-privacy-container">
                                                <ul id="menu-privacy" class="menu">
                                                    <li id="menu-item-1765" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1765 tc-menu-item tc-menu-depth-0 tc-menu-layout-default"><a
                                                         href="#" class="tc-menu-inner">Privacy</a></li>
                                                    <li id="menu-item-1766" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1766 tc-menu-item tc-menu-depth-0 tc-menu-layout-default"><a
                                                         href="#" class="tc-menu-inner">Terms</a></li>
                                                    <li id="menu-item-1767" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1767 tc-menu-item tc-menu-depth-0 tc-menu-layout-default"><a
                                                         href="#" class="tc-menu-inner">Sitemap</a></li>
                                                    <li id="menu-item-6720" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-6720 tc-menu-item tc-menu-depth-0 tc-menu-layout-default"><a
                                                         href="https://themeforest.net/item/education-wordpress-theme-education-wp/14058034?ref=ThimPress&amp;utm_source=demo&amp;utm_medium=demofooter"
                                                         class="tc-menu-inner">Purchase</a></li>
                                                </ul>
                                            </div>
                                        </aside>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
            <div class="footer-bottom">
                <div class="container">
                    <aside id="siteorigin-panels-builder-7" class="widget widget_siteorigin-panels-builder footer_bottom_widget">
                        <div id="pl-w57babc3f18d14" class="panel-layout">
                            <div id="pg-w57babc3f18d14-0" class="panel-grid panel-has-style">
                                <div class="thim-bg-overlay-color-half siteorigin-panels-stretch panel-row-style panel-row-style-for-w57babc3f18d14-0"
                                 data-stretch-type="full">
                                    <div id="pgc-w57babc3f18d14-0-0" class="panel-grid-cell">
                                        <div id="panel-w57babc3f18d14-0-0-0" class="so-panel widget widget_heading panel-first-child" data-index="0">
                                            <div class="thim-widget-heading thim-widget-heading-base">
                                                <div class="sc_heading   text-center">
                                                    <h3 style="color:#333333;" class="title">Become an instructor?</h3>
                                                    <p class="sub-heading" style="color:#333333;">Join thousand of instructors and earn money hassle free!</p><span
                                                     class="line"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="panel-w57babc3f18d14-0-0-1" class="so-panel widget widget_button panel-last-child" data-index="1">
                                            <div class="thim-widget-button thim-widget-button-base"><a class="widget-button  normal" href="https://themeforest.net/item/education-wordpress-theme-education-wp/14058034?ref=ThimPress&amp;utm_source=demo&amp;utm_medium=demofooter"
                                                 style="" data-hover="">Get Started Now</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </aside>
                </div>
            </div>
        </div> <a href="#" id="back-to-top"> <i class="fa fa-chevron-up" aria-hidden="true"></i> </a>
    </div>

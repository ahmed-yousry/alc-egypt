<!DOCTYPE html>
<html itemscope itemtype="http://schema.org/WebPage" lang="en-US" prefix="og: http://ogp.me/ns#">
   <!-- Added by  **** -->
   <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
   <!-- /Added by  **** -->
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="profile" href="https://gmpg.org/xfn/11">
      <link rel="pingback" href="../xmlrpc.php">

      <link rel="stylesheet" type="text/css" href="https://js.api.here.com/v3/3.0/mapsjs-ui.css?dp-version=1542186754" />
      <script type="text/javascript" src="https://js.api.here.com/v3/3.0/mapsjs-core.js"></script>
      <script type="text/javascript" src="https://js.api.here.com/v3/3.0/mapsjs-service.js"></script>
      <script type="text/javascript" src="https://js.api.here.com/v3/3.0/mapsjs-ui.js"></script>
      <script type="text/javascript" src="https://js.api.here.com/v3/3.0/mapsjs-mapevents.js"></script>


      <script type="text/javascript">var ajaxurl = "{{ asset('assets/wp-admin/admin-ajax.html')}}";</script>
      <link type="text/css" media="screen" href="      {{ asset('assets/wp-content/cache/autoptimize/1/css/autoptimize_bcb2f3a50e74b496f21b148cbe50d29c.css')}}" rel="stylesheet" />
      <style type="text/css" media="print">@media print{.pmpro_a-print{display:none;position:absolute;left:-9999px}}</style>



      <link type="text/css" media="all" href="{{ asset('assets/wp-content/cache/autoptimize/1/css/autoptimize_d0c3f53760d5bcca06bcef61319ce777.css')}}
" rel="stylesheet" />
      <title>Contact - Education WP</title>
      <link rel="canonical" href="index.html" />
      <meta property="og:locale" content="en_US" />
      <meta property="og:type" content="article" />
      <meta property="og:title" content="Contact - Education WP" />
      <meta property="og:description" content="Contact InfoWelcome to our Website. We are glad to have you around.Phone+7 (800) 123 45 69Emailhello@eduma.comAddressPO Box 97845 Baker st. 567, Los Angeles, California, US. Send A MessageYour email address will not be published. Required fields are marked. Subscribe To &hellip;" />
      <meta property="og:url" content="https://educationwp.thimpress.com/contact/" />
      <meta property="og:site_name" content="Education WP" />
      <meta name="twitter:card" content="summary" />
      <meta name="twitter:description" content="Contact InfoWelcome to our Website. We are glad to have you around.Phone+7 (800) 123 45 69Emailhello@eduma.comAddressPO Box 97845 Baker st. 567, Los Angeles, California, US. Send A MessageYour email address will not be published. Required fields are marked. Subscribe To &hellip;" />
      <meta name="twitter:title" content="Contact - Education WP" />
      <link rel='dns-prefetch' href='http://s.w.org/' />
      <link rel="alternate" type="application/rss+xml" title="Education WP &raquo; Feed" href="../feed/index.html" />
      <style id='woocommerce-inline-inline-css' type='text/css'>.woocommerce form .form-row .required { visibility: visible; }</style>
      <style id='rs-plugin-settings-inline-css' type='text/css'>#rs-demo-id {}</style>

      <script type='text/javascript' src="{{ asset('assets/wp-includes/js/jquery/jquery.js')}}"></script> <script type='text/javascript'>var userSettings = {"url":"\/","uid":"0","time":"1541938161","secure":"1"};</script> <script type='text/javascript'>var panelsStyles = {"fullContainer":"body"};</script>
      <link rel='https://api.w.org/' href='../wp-json/index.html' />
      <link rel="EditURI" type="application/rsd+xml" title="RSD" href="../xmlrpc0db0.php?rsd" />
      <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="../wp-includes/wlwmanifest.xml" />
      <link rel='shortlink' href='../index5d38.html?p=87' />


      <link rel="alternate" type="application/json+oembed" href="{{ asset('assets/wp-json/oembed/1.0/embed0907.json?url=https%3A%2F%2Feducationwp.thimpress.com%2Fcontact%2F')}}
" />
      <link rel="alternate" type="text/xml+oembed" href="../wp-json/oembed/1.0/embed41b5?url=https%3A%2F%2Feducationwp.thimpress.com%2Fcontact%2F&amp;format=xml" />
      <noscript>
         <style>.woocommerce-product-gallery{ opacity: 1 !important; }</style>
      </noscript>
      <script type="text/javascript">function tc_insert_internal_css(css) {
         var tc_style = document.createElement("style");
         tc_style.type = "text/css";
         tc_style.setAttribute('data-type', 'tc-internal-css');
         var tc_style_content = document.createTextNode(css);
         tc_style.appendChild(tc_style_content);
         document.head.appendChild(tc_style);
         }
      </script>
      <meta name="generator" content="Powered by Slider Revolution 5.4.8 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
      <style type="text/css" media="all" id="siteorigin-panels-layouts-head">/* Layout 87 */ #pgc-87-0-0 { width:48%;width:calc(48% - ( 0.52 * 30px ) ) } #pl-87 #panel-87-0-0-0 , #pl-87 #panel-87-0-0-1 , #pl-87 #panel-87-0-0-2 , #pl-87 #panel-87-0-0-3 , #pl-87 #panel-87-0-2-0 , #pl-87 #panel-87-0-2-1 , #pl-87 #panel-87-1-0-0 , #pl-87 #panel-87-1-0-1 , #pl-87 #panel-87-2-0-0 , #pl-87 #panel-87-3-0-0 {  } #pgc-87-0-1 { width:4.0224%;width:calc(4.0224% - ( 0.959775870099 * 30px ) ) } #pgc-87-0-2 { width:47.9776%;width:calc(47.9776% - ( 0.520224129901 * 30px ) ) } #pg-87-0 { margin-bottom:80px } #pgc-87-1-0 , #pgc-87-2-0 , #pgc-87-3-0 { width:100%;width:calc(100% - ( 0 * 30px ) ) } #pg-87-1 , #pg-87-2 , #pl-87 .so-panel { margin-bottom:30px } #pl-87 .so-panel:last-child { margin-bottom:0px } #panel-87-0-0-0> .panel-widget-style , #panel-87-0-2-0> .panel-widget-style , #panel-87-1-0-0> .panel-widget-style { padding:30px 0px 0px } #pg-87-1> .panel-row-style { background-image:url(../wp-content/uploads/2015/10/bg-register-now.jpg);background-position:center center;background-size:cover;padding:0px 0px 100px } @media (max-width:767px){ #pg-87-0.panel-no-style, #pg-87-0.panel-has-style > .panel-row-style , #pg-87-1.panel-no-style, #pg-87-1.panel-has-style > .panel-row-style , #pg-87-2.panel-no-style, #pg-87-2.panel-has-style > .panel-row-style , #pg-87-3.panel-no-style, #pg-87-3.panel-has-style > .panel-row-style { -webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column } #pg-87-0 .panel-grid-cell , #pg-87-1 .panel-grid-cell , #pg-87-2 .panel-grid-cell , #pg-87-3 .panel-grid-cell { margin-right:0 } #pg-87-0 .panel-grid-cell , #pg-87-1 .panel-grid-cell , #pg-87-2 .panel-grid-cell , #pg-87-3 .panel-grid-cell { width:100% } #pgc-87-0-0 , #pgc-87-0-1 { margin-bottom:30px } #pl-87 .panel-grid-cell { padding:0 } #pl-87 .panel-grid .panel-grid-cell-empty { display:none } #pl-87 .panel-grid .panel-grid-cell-mobile-last { margin-bottom:0px }  } /* Layout w58292b77794b7 */ #pgc-w58292b77794b7-0-0 , #pgc-w58292b77794b7-0-1 { width:50%;width:calc(50% - ( 0.5 * 30px ) ) } #pl-w58292b77794b7 #panel-w58292b77794b7-0-0-0 , #pl-w58292b77794b7 #panel-w58292b77794b7-0-1-0 {  } #pl-w58292b77794b7 .so-panel { margin-bottom:30px } #pl-w58292b77794b7 .so-panel:last-child { margin-bottom:0px } @media (max-width:767px){ #pg-w58292b77794b7-0.panel-no-style, #pg-w58292b77794b7-0.panel-has-style > .panel-row-style { -webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column } #pg-w58292b77794b7-0 .panel-grid-cell { margin-right:0 } #pg-w58292b77794b7-0 .panel-grid-cell { width:100% } #pgc-w58292b77794b7-0-0 { margin-bottom:30px } #pl-w58292b77794b7 .panel-grid-cell { padding:0 } #pl-w58292b77794b7 .panel-grid .panel-grid-cell-empty { display:none } #pl-w58292b77794b7 .panel-grid .panel-grid-cell-mobile-last { margin-bottom:0px }  }</style>
      <link rel="icon" href="../wp-content/uploads/2015/10/favicon.png" sizes="32x32" />
      <link rel="icon" href="../wp-content/uploads/2015/10/favicon.png" sizes="192x192" />
      <link rel="apple-touch-icon-precomposed" href="../wp-content/uploads/2015/10/favicon.png" />
      <meta name="msapplication-TileImage" content="https://educationwp.thimpress.com/wp-content/uploads/2015/10/favicon.png" />
      <script type="text/javascript">function setREVStartSize(e){
         try{ e.c=jQuery(e.c);var i=jQuery(window).width(),t=9999,r=0,n=0,l=0,f=0,s=0,h=0;
         	if(e.responsiveLevels&&(jQuery.each(e.responsiveLevels,function(e,f){f>i&&(t=r=f,l=e),i>f&&f>r&&(r=f,n=e)}),t>r&&(l=n)),f=e.gridheight[l]||e.gridheight[0]||e.gridheight,s=e.gridwidth[l]||e.gridwidth[0]||e.gridwidth,h=i/s,h=h>1?1:h,f=Math.round(h*f),"fullscreen"==e.sliderLayout){var u=(e.c.width(),jQuery(window).height());if(void 0!=e.fullScreenOffsetContainer){var c=e.fullScreenOffsetContainer.split(",");if (c) jQuery.each(c,function(e,i){u=jQuery(i).length>0?u-jQuery(i).outerHeight(!0):u}),e.fullScreenOffset.split("%").length>1&&void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0?u-=jQuery(window).height()*parseInt(e.fullScreenOffset,0)/100:void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0&&(u-=parseInt(e.fullScreenOffset,0))}f=u}else void 0!=e.minHeight&&f<e.minHeight&&(f=e.minHeight);e.c.closest(".rev_slider_wrapper").css({height:f})
         }catch(d){console.log("Failure at Presize of Slider:"+d)}
         };
      </script> <script type="text/javascript">if (typeof ajaxurl === 'undefined') {
         var ajaxurl = "../wp-admin/admin-ajax.html";

         }
      </script> <script>!function(f, b, e, v, n, t, s) {
         if (f.fbq) return;
         n = f.fbq = function() {
             n.callMethod ?
                 n.callMethod.apply(n, arguments) : n.queue.push(arguments);
         };
         if (!f._fbq) f._fbq = n;
         n.push = n;
         n.loaded = !0;
         n.version = '2.0';
         n.queue = [];
         t = b.createElement(e);
         t.async = !0;
         t.src = v;
         s = b.getElementsByTagName(e)[0];
         s.parentNode.insertBefore(t, s);
         }(window, document, 'script',
         'https://connect.facebook.net/en_US/fbevents.js');
         fbq('init', '978118969000044');
         fbq('track', 'PageView');
      </script>
      <noscript> <img height="1" width="1"
         src="https://www.facebook.com/tr?id=978118969000044&amp;ev=PageView&amp;noscript=1" /> </noscript>
   </head>
   <body class="page-template-default page page-id-87 courses eduma learnpress learnpress-page woocommerce-no-js pmpro-body-has-access siteorigin-panels siteorigin-panels-before-js group-blog thim-body-preload bg-boxed-image" id="thim-body">
      <div id="preload">
         <div class="sk-folding-cube">
            <div class="sk-cube1 sk-cube"></div>
            <div class="sk-cube2 sk-cube"></div>
            <div class="sk-cube4 sk-cube"></div>
            <div class="sk-cube3 sk-cube"></div>
         </div>
      </div>
      <div id="wrapper-container" class="wrapper-container">
         <div class="content-pusher">

           @include('front.includes.header')

          @include('front.includes.nav')
            <div id="main-content">
               <section class="content-area">
                  <div class="top_heading  _out">

                     <div class="top_site_main " style="color: #ffffff;background-image:url(  {{ asset('assets/wp-content/themes/eduma/images/bg-page.jpg')}});">
                        <span class="overlay-top-header" style="background:rgba(0,0,0,0.5);"></span>
                        <div class="page-title-wrapper">
                           <div class="banner-wrapper container">
                              <h1>Contact</h1>
                           </div>
                        </div>
                     </div>
                     <div class="breadcrumbs-wrapper">
                        <div class="container">
                           <ul itemprop="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList" id="breadcrumbs" class="breadcrumbs">
                              <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" href="../index.html" title="Home"><span itemprop="name">Home</span></a></li>
                              <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name" title="Contact"> Contact</span></li>
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div class="container site-content no-padding-top">
                     <div class="row">
                        <main id="main" class="site-main col-sm-12 full-width">
                           <article id="post-87" class="post-87 page type-page status-publish hentry pmpro-has-access">
                              <div class="entry-content">
                                 <div id="pl-87" class="panel-layout">
                                    <div id="pg-87-0" class="panel-grid panel-no-style">
                                       <div id="pgc-87-0-0" class="panel-grid-cell">
                                          <div id="panel-87-0-0-0" class="so-panel widget widget_heading panel-first-child" data-index="0">
                                             <div class="panel-widget-style panel-widget-style-for-87-0-0-0">
                                                <div class="thim-widget-heading thim-widget-heading-base">
                                                   <div class="sc_heading   text-left">
                                                      <h3 class="title">Contact Info</h3>
                                                      <p class="sub-heading" style="">Welcome to our Website. We are glad to have you around.</p>
                                                      <span class="line"></span>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div id="panel-87-0-0-1" class="so-panel widget widget_siteorigin-panels-builder" data-index="1">
                                             <div class="thim-border-top panel-widget-style panel-widget-style-for-87-0-0-1">
                                                <div id="pl-w58292b77794b7" class="panel-layout">
                                                   <div id="pg-w58292b77794b7-0" class="panel-grid panel-no-style">
                                                      <div id="pgc-w58292b77794b7-0-0" class="panel-grid-cell">
                                                         <div id="panel-w58292b77794b7-0-0-0" class="so-panel widget widget_icon-box panel-first-child panel-last-child" data-index="0">
                                                            <div class="thim-widget-icon-box thim-widget-icon-box-base">
                                                               <div class="wrapper-box-icon text-left contact_info ">
                                                                  <div class="smicon-box iconbox-left">
                                                                     <div class="boxes-icon" style="width: 30px;"><span class="inner-icon"><span class="icon"><i class="fa fa-phone" style="color:#ffb606; font-size:24px; line-height:24px; vertical-align: middle;"></i></span></span></div>
                                                                     <div class="content-inner" style="width: calc( 100% - 30px - 15px);">
                                                                        <div class="sc-heading article_heading">
                                                                           <h3 class="heading__primary">Phone</h3>
                                                                        </div>
                                                                        <div class="desc-icon-box">
                                                                           <div class="desc-content">(03) 3931507 </div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div id="pgc-w58292b77794b7-0-1" class="panel-grid-cell">
                                                         <div id="panel-w58292b77794b7-0-1-0" class="so-panel widget widget_icon-box panel-first-child panel-last-child" data-index="1">
                                                            <div class="thim-widget-icon-box thim-widget-icon-box-base">
                                                               <div class="wrapper-box-icon text-left contact_info ">
                                                                  <div class="smicon-box iconbox-left">
                                                                     <div class="boxes-icon" style="width: 30px;"><span class="inner-icon"><span class="icon"><i class="fa fa-envelope" style="color:#ffb606; font-size:24px; line-height:24px; vertical-align: middle;"></i></span></span></div>
                                                                     <div class="content-inner" style="width: calc( 100% - 30px - 15px);">
                                                                        <div class="sc-heading article_heading">
                                                                           <h3 class="heading__primary">Email</h3>
                                                                        </div>
                                                                        <div class="desc-icon-box">
                                                                           <div class="desc-content"><a ><span class="__cf_email__" >magdaay@hotmail.com</span></a></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div id="panel-87-0-0-2" class="so-panel widget widget_icon-box" data-index="2">
                                             <div class="thim-border-top panel-widget-style panel-widget-style-for-87-0-0-2">
                                                <div class="thim-widget-icon-box thim-widget-icon-box-base">
                                                   <div class="wrapper-box-icon text-left contact_info ">
                                                      <div class="smicon-box iconbox-left">
                                                         <div class="boxes-icon" style="width: 30px;"><span class="inner-icon"><span class="icon"><i class="fa fa-map-marker" style="color:#ffb606; font-size:24px; line-height:24px; vertical-align: middle;"></i></span></span></div>
                                                         <div class="content-inner" style="width: calc( 100% - 30px - 15px);">
                                                            <div class="sc-heading article_heading">
                                                               <h3 class="heading__primary">Address</h3>
                                                            </div>
                                                            <div class="desc-icon-box">
                                                               <div class="desc-content">11 Mahmoud Hamdy Khatab St,forked Fouad St, bab shark</div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div id="panel-87-0-0-3" class="so-panel widget widget_social panel-last-child" data-index="3">
                                             <div class="thim-border-top panel-widget-style panel-widget-style-for-87-0-0-3">
                                                <div class="thim-widget-social thim-widget-social-base">
                                                   <div class="thim-social">
                                                      <ul class="social_link">
                                                         <li><a class="facebook hasTooltip" href="https://www.facebook.com/pages/category/Language-School/Alexandria-Centre-For-Languages-587844001352285/" target="_self"><i class="fa fa-facebook"></i></a></li>
                                                        </li>
                                                      </ul>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div id="pgc-87-0-1" class="panel-grid-cell panel-grid-cell-empty"></div>
                                       <div id="pgc-87-0-2" class="panel-grid-cell">
                                          <div id="panel-87-0-2-0" class="so-panel widget widget_heading panel-first-child" data-index="4">
                                             <div class="panel-widget-style panel-widget-style-for-87-0-2-0">
                                                <div class="thim-widget-heading thim-widget-heading-base">
                                                   <div class="sc_heading   text-left">
                                                      <h3 class="title">Send A Message</h3>
                                                      <p class="sub-heading" style="">Your email address will not be published. Required fields are marked.</p>
                                                      <span class="line"></span>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          
                                          <div>
                                                      <div class="textwidget">
                                                <div role="form">
                                                   <div class="screen-reader-response"></div>
                                                   <form action="{{url('contact/send')}}" method="post" class="wpcf7-form" >
                                                       {{csrf_field()}}
                                                      <div style="display: none;">
                                                        <input type="hidden" name="_wpcf7" value="4" />
                                                        <input type="hidden" name="_wpcf7_version" value="5.0.3" />
                                                        <input type="hidden" name="_wpcf7_locale" value="en_US" />
                                                        <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f4-p87-o3" />
                                                        <input type="hidden" name="_wpcf7_container_post" value="87" /></div>
                                                      <p><span class="wpcf7-form-control-wrap your-name">
                                                        <input type="text" name="name" value=""   size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Name *" required/></span></p>
                                                      <p><span class="wpcf7-form-control-wrap your-email">
                                                        <input type="email" name="email" value=""  size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Email *" required/>
                                                      </span></p>
                                                      <p><span class="wpcf7-form-control-wrap your-subject">
                                                        <input type="text" name="subject" value=""  size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" required placeholder="Subject *" /></span></p>
                                                      <p><span class="wpcf7-form-control-wrap your-message"><textarea name="message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Message *"></textarea></span></p>
                                                      <p><input type="submit" value="Submit" class="wpcf7-form-control wpcf7-submit" /></p>
                                                      <div class="wpcf7-response-output wpcf7-display-none"></div>
                                                   </form>
                                                </div>
                                             </div>
                                          </div>
                                          
                                          
                                          
                                       </div>
                                    </div>
                                    <div id="pg-87-1" class="panel-grid panel-has-style">
                                       <div class="thim-bg-overlay text-center siteorigin-panels-stretch panel-row-style panel-row-style-for-87-1" data-stretch-type="full">
                                          <div id="pgc-87-1-0" class="panel-grid-cell">
                                             <div id="panel-87-1-0-0" class="so-panel widget widget_heading panel-first-child" data-index="6">
                                                <div class="panel-widget-style panel-widget-style-for-87-1-0-0">
                                                   <div class="thim-widget-heading thim-widget-heading-base">
                                                      <div class="sc_heading   text-center">
                                                         <h3 style="color:#ffffff;" class="title">Subscribe To Our News</h3>
                                                         <span style="background-color:#ffffff" class="line"></span>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div id="panel-87-1-0-1" class="so-panel widget widget_text panel-last-child" data-index="7">
                                                <div class="textwidget">
                                                   <script data-cfasync="false" src="{{ asset('assets/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js')}}"></script><script>(function() {
                                                      if (!window.mc4wp) {
                                                      	window.mc4wp = {
                                                      		listeners: [],
                                                      		forms    : {
                                                      			on: function (event, callback) {
                                                      				window.mc4wp.listeners.push({
                                                      					event   : event,
                                                      					callback: callback
                                                      				});
                                                      			}
                                                      		}
                                                      	}
                                                      }
                                                      })();
                                                   </script>
                                                   <form id="mc4wp-form-3" class="mc4wp-form mc4wp-form-3101 mc4wp-form-basic" method="post" data-id="3101" data-name="Default sign-up form">
                                                      <div class="mc4wp-form-fields"><input type="email" id="mc4wp_email" name="EMAIL" placeholder="Your email here" required /> <input type="submit" value="Subscribe" /></div>
                                                      <label style="display: none !important;">Leave this field empty if you're human: <input type="text" name="_mc4wp_honeypot" value="" tabindex="-1" autocomplete="off" /></label><input type="hidden" name="_mc4wp_timestamp" value="1541938162" /><input type="hidden" name="_mc4wp_form_id" value="3101" /><input type="hidden" name="_mc4wp_form_element_id" value="mc4wp-form-3" />
                                                      <div class="mc4wp-response"></div>
                                                   </form>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div id="pg-87-2" class="panel-grid panel-no-style">
                                       <div id="pgc-87-2-0" class="panel-grid-cell">
                                          <div id="panel-87-2-0-0" class="so-panel widget widget_heading panel-first-child panel-last-child" data-index="8">
                                             <div class="thim-widget-heading thim-widget-heading-base">
                                                <div class="sc_heading   text-left">
                                                   <h3 class="title">Location on map</h3>
                                                   <span class="line"></span>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div id="pg-87-3" class="panel-grid panel-has-style">
                                       <div class="siteorigin-panels-stretch thim-fix-stretched panel-row-style panel-row-style-for-87-3" data-stretch-type="full-stretched">
                                          <div id="pgc-87-3-0" class="panel-grid-cell">
                                             <div id="panel-87-3-0-0" class="so-panel widget widget_google-map panel-first-child panel-last-child" data-index="9">
                                                <div class="thim-widget-google-map thim-widget-google-map-base">
                                                   <div class="kcf-module">

                                                     <div id="map" style="width: 150%; height: 400px; background: grey" />
<script  type="text/javascript" charset="UTF-8" >

/**
* Adds a  draggable marker to the map..
*
* @param {H.Map} map                      A HERE Map instance within the
*                                         application
* @param {H.mapevents.Behavior} behavior  Behavior implements
*                                         default interactions for pan/zoom
*/
function addDraggableMarker(map, behavior){

var marker = new H.map.Marker({lat:31.1993027, lng:29.9127572});
// Ensure that the marker can receive drag events
marker.draggable = true;
map.addObject(marker);

// disable the default draggability of the underlying map
// when starting to drag a marker object:
map.addEventListener('dragstart', function(ev) {
  var target = ev.target;
  if (target instanceof H.map.Marker) {
    behavior.disable();
  }
}, false);


// re-enable the default draggability of the underlying map
// when dragging has completed
map.addEventListener('dragend', function(ev) {
  var target = ev.target;
  if (target instanceof mapsjs.map.Marker) {
    behavior.enable();
  }
}, false);

// Listen to the drag event and move the position of the marker
// as necessary
 map.addEventListener('drag', function(ev) {
  var target = ev.target,
      pointer = ev.currentPointer;
  if (target instanceof mapsjs.map.Marker) {
    target.setPosition(map.screenToGeo(pointer.viewportX, pointer.viewportY));
  }
}, false);
}

/**
* Boilerplate map initialization code starts below:
*/

//Step 1: initialize communication with the platform
var platform = new H.service.Platform({
app_id: 'devportal-demo-20180625',
app_code: '9v2BkviRwi9Ot26kp2IysQ',
useHTTPS: true
});
var pixelRatio = window.devicePixelRatio || 1;
var defaultLayers = platform.createDefaultLayers({
tileSize: pixelRatio === 1 ? 256 : 512,
ppi: pixelRatio === 1 ? undefined : 320
});

//Step 2: initialize a map - this map is centered over Boston
var map = new H.Map(document.getElementById('map'),
defaultLayers.normal.map,{

center: {lat:31.1993027, lng:29.9127572},
zoom: 20,
pixelRatio: pixelRatio
});

//Step 3: make the map interactive
// MapEvents enables the event system
// Behavior implements default interactions for pan/zoom (also on mobile touch environments)
var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));

// Step 4: Create the default UI:
var ui = H.ui.UI.createDefault(map, defaultLayers, 'en-US');

// Add the click event listener.
addDraggableMarker(map, behavior);
</script>
</div>
                                                      <!-- <div class="ob-google-map-canvas" style="height:450px;" id="ob-map-canvas-8333a4bc9ebc4f6bca9807a7f3671767" data-display_by="address" data-lat="41.868626" data-lng="-74.104301" data-address="58 Sharrocks Road, Mount Egerton VIC 3352, Australia" data-zoom="12" data-draggable="1" data-marker-icon="https://educationwp.thimpress.com/wp-content/uploads/2015/10/maker-map.png" data-marker-at-center="1" data-marker-positions="false" data-api-key="AIzaSyDhFA3GbWtH3JBmvhWWrQ0Ngjig2c1vndU"></div> -->
                                                   </div>

                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </article>
                        </main>
                     </div>
                  </div>
               </section>




   @include('front.includes.footer')



            </div>
         </div>
         <a href="#" id="back-to-top"> <i class="fa fa-chevron-up" aria-hidden="true"></i> </a>
      </div>


      <script data-cfasync="false" src="{{ asset('assets/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js')}}"></script><script data-cfasync="false" type="text/javascript">window.onload = function () {
         var thim_preload = document.getElementById("preload");
         if (thim_preload) {
             setTimeout(function () {
                 var body = document.getElementById("thim-body"),
                     len = body.childNodes.length,
                     class_name = body.className.replace(/(?:^|\s)thim-body-preload(?!\S)/, '').replace(/(?:^|\s)thim-body-load-overlay(?!\S)/, '');

                 body.className = class_name;
                 if (typeof thim_preload !== 'undefined' && thim_preload !== null) {
                     for (var i = 0; i < len; i++) {
                         if (body.childNodes[i].id !== 'undefined' && body.childNodes[i].id == "preload") {
                             body.removeChild(body.childNodes[i]);
                             break;
                         }
                     }
                 }
             }, 500);
         }
         };
      </script> <script>window.addEventListener('load', function () {
         /**
          * Fix issue there is an empty spacing between image and title of owl-carousel
          */
         setTimeout(function () {
             var $ = jQuery;
             var $carousel = $('.thim-owl-carousel-post').each(function () {
                 $(this).find('.image').css('min-height', 0);
                 $(window).trigger('resize');
             });
         }, 500);
         })
      </script> <script data-cfasync="true" type="text/javascript">(function ($) {
         "use strict";
         $(document).on('click', 'body:not(".logged-in") .enroll-course .button-enroll-course, body:not(".logged-in") .purchase-course:not(".guest_checkout, .learn-press-pmpro-buy-membership") .button', function (e) {
             e.preventDefault();
             $(this).parent().find('[name="redirect_to"]').val('../account/index7828.html?redirect_to=https://educationwp.thimpress.com/contact/?enroll-course=87');
             var redirect = $(this).parent().find('[name="redirect_to"]').val();
             window.location = redirect;
         });
         })(jQuery);
      </script> <script>(function() {function addEventListener(element,event,handler) {
         if(element.addEventListener) {
         	element.addEventListener(event,handler, false);
         } else if(element.attachEvent){
         	element.attachEvent('on'+event,handler);
         }
         }function maybePrefixUrlField() {
         if(this.value.trim() !== '' && this.value.indexOf('http') !== 0) {
         	this.value = "http://" + this.value;
         }
         }

         var urlFields = document.querySelectorAll('.mc4wp-form input[type="url"]');
         if( urlFields && urlFields.length > 0 ) {
         for( var j=0; j < urlFields.length; j++ ) {
         	addEventListener(urlFields[j],'blur',maybePrefixUrlField);
         }
         }/* test if browser supports date fields */
         var testInput = document.createElement('input');
         testInput.setAttribute('type', 'date');
         if( testInput.type !== 'date') {

         /* add placeholder & pattern to all date fields */
         var dateFields = document.querySelectorAll('.mc4wp-form input[type="date"]');
         for(var i=0; i<dateFields.length; i++) {
         	if(!dateFields[i].placeholder) {
         		dateFields[i].placeholder = 'YYYY-MM-DD';
         	}
         	if(!dateFields[i].pattern) {
         		dateFields[i].pattern = '[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])';
         	}
         }
         }

         })();
      </script>
      <style type="text/css" media="all" id="siteorigin-panels-layouts-footer">/* Layout tc-megamenu-7682 */ #pgc-tc-megamenu-7682-0-0 , #pgc-tc-megamenu-7682-0-1 { width:31%;width:calc(31% - ( 0.69 * 30px ) ) } #pl-tc-megamenu-7682 #panel-tc-megamenu-7682-0-0-0 , #pl-tc-megamenu-7682 #panel-tc-megamenu-7682-0-1-0 , #pl-tc-megamenu-7682 #panel-tc-megamenu-7682-0-2-0 {  } #pgc-tc-megamenu-7682-0-2 { width:38%;width:calc(38% - ( 0.62 * 30px ) ) } #pl-tc-megamenu-7682 .so-panel { margin-bottom:30px } #pl-tc-megamenu-7682 .so-panel:last-child { margin-bottom:0px } @media (max-width:767px){ #pg-tc-megamenu-7682-0.panel-no-style, #pg-tc-megamenu-7682-0.panel-has-style > .panel-row-style { -webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column } #pg-tc-megamenu-7682-0 .panel-grid-cell { margin-right:0 } #pg-tc-megamenu-7682-0 .panel-grid-cell { width:100% } #pgc-tc-megamenu-7682-0-0 , #pgc-tc-megamenu-7682-0-1 { margin-bottom:30px } #pl-tc-megamenu-7682 .panel-grid-cell { padding:0 } #pl-tc-megamenu-7682 .panel-grid .panel-grid-cell-empty { display:none } #pl-tc-megamenu-7682 .panel-grid .panel-grid-cell-mobile-last { margin-bottom:0px }  } /* Layout w58292b77794b7 */ #pgc-w58292b77794b7-0-0 , #pgc-w58292b77794b7-0-1 { width:50%;width:calc(50% - ( 0.5 * 30px ) ) } #pl-w58292b77794b7 #panel-w58292b77794b7-0-0-0 , #pl-w58292b77794b7 #panel-w58292b77794b7-0-1-0 {  } #pl-w58292b77794b7 .so-panel { margin-bottom:30px } #pl-w58292b77794b7 .so-panel:last-child { margin-bottom:0px } @media (max-width:767px){ #pg-w58292b77794b7-0.panel-no-style, #pg-w58292b77794b7-0.panel-has-style > .panel-row-style { -webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column } #pg-w58292b77794b7-0 .panel-grid-cell { margin-right:0 } #pg-w58292b77794b7-0 .panel-grid-cell { width:100% } #pgc-w58292b77794b7-0-0 { margin-bottom:30px } #pl-w58292b77794b7 .panel-grid-cell { padding:0 } #pl-w58292b77794b7 .panel-grid .panel-grid-cell-empty { display:none } #pl-w58292b77794b7 .panel-grid .panel-grid-cell-mobile-last { margin-bottom:0px }  } /* Layout w57e9cc2c86af4 */ #pgc-w57e9cc2c86af4-0-0 { width:33.3%;width:calc(33.3% - ( 0.667 * 30px ) ) } #pl-w57e9cc2c86af4 #panel-w57e9cc2c86af4-0-0-0 , #pl-w57e9cc2c86af4 #panel-w57e9cc2c86af4-0-0-1 , #pl-w57e9cc2c86af4 #panel-w57e9cc2c86af4-0-1-0 , #pl-w57e9cc2c86af4 #panel-w57e9cc2c86af4-0-2-0 , #pl-w57e9cc2c86af4 #panel-w57e9cc2c86af4-0-3-0 , #pl-w57e9cc2c86af4 #panel-w57e9cc2c86af4-0-4-0 {  } #pgc-w57e9cc2c86af4-0-1 , #pgc-w57e9cc2c86af4-0-2 , #pgc-w57e9cc2c86af4-0-3 , #pgc-w57e9cc2c86af4-0-4 { width:16.675%;width:calc(16.675% - ( 0.83325 * 30px ) ) } #pl-w57e9cc2c86af4 .so-panel { margin-bottom:30px } #pl-w57e9cc2c86af4 .so-panel:last-child { margin-bottom:0px } @media (max-width:767px){ #pg-w57e9cc2c86af4-0.panel-no-style, #pg-w57e9cc2c86af4-0.panel-has-style > .panel-row-style { -webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column } #pg-w57e9cc2c86af4-0 .panel-grid-cell { margin-right:0 } #pg-w57e9cc2c86af4-0 .panel-grid-cell { width:100% } #pgc-w57e9cc2c86af4-0-0 , #pgc-w57e9cc2c86af4-0-1 , #pgc-w57e9cc2c86af4-0-2 , #pgc-w57e9cc2c86af4-0-3 { margin-bottom:30px } #pl-w57e9cc2c86af4 .panel-grid-cell { padding:0 } #pl-w57e9cc2c86af4 .panel-grid .panel-grid-cell-empty { display:none } #pl-w57e9cc2c86af4 .panel-grid .panel-grid-cell-mobile-last { margin-bottom:0px }  }</style>
      <script type="text/javascript">var c = document.body.className;
         c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
         document.body.className = c;
      </script> <script type='text/javascript'>var wpcf7 = {"apiSettings":{"root":"https:\/\/educationwp.thimpress.com\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"recaptcha":{"messages":{"empty":"Please verify that you are not a robot."}},"cached":0};</script> <script type='text/javascript'>var wc_add_to_cart_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","i18n_view_cart":"View cart","cart_url":"https:\/\/educationwp.thimpress.com\/cart\/","is_cart":"","cart_redirect_after_add":"no"};</script> <script type='text/javascript'>var woocommerce_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%"};</script> <script type='text/javascript'>var wc_cart_fragments_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","cart_hash_key":"wc_cart_hash_8653f1d19ec105315cba116db9d7fe2e","fragment_name":"wc_fragments_8653f1d19ec105315cba116db9d7fe2e"};</script> <script type='text/javascript'>var _wpUtilSettings = {"ajax":{"url":"\/wp-admin\/admin-ajax.php"}};</script> <script type='text/javascript'>var WPEMS = {"gmt_offset":"0","current_time":"Nov 11, 2018 12:09:00 +0000","l18n":{"labels":["Years","Months","Weeks","Days","Hours","Minutes","Seconds"],"labels1":["Year","Month","Week","Day","Hour","Minute","Second"]},"ajaxurl":"https:\/\/educationwp.thimpress.com\/wp-admin\/admin-ajax.php","something_wrong":"Something went wrong","register_button":"c20eb61364"};</script> <script type='text/javascript'>var thim_js_translate = {"login":"Username","password":"Password","close":"Close"};</script> <script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js?ver=3.0.22'></script> <script type='text/javascript'>WebFont.load({google:{families:['Roboto Slab:300,400,700', 'Roboto:300,300i,400,400i,500,500i,700,700i']}});</script> <script type='text/javascript'>var mc4wp_forms_config = [];</script> <!--[if lte IE 9]> <script type='text/javascript' src='https://educationwp.thimpress.com/wp-content/plugins/mailchimp-for-wp/assets/js/third-party/placeholders.min.js?ver=4.2.4'></script> <![endif]--> <script type='text/javascript'>var lpGlobalSettings = {"url":"https:\/\/educationwp.thimpress.com\/contact\/","siteurl":"https:\/\/educationwp.thimpress.com","ajax":"https:\/\/educationwp.thimpress.com\/wp-admin\/admin-ajax.php","theme":"eduma","localize":{"button_ok":"OK","button_cancel":"Cancel","button_yes":"Yes","button_no":"No"}};</script> <script type='text/javascript'>var lpCheckoutSettings = {"ajaxurl":"https:\/\/educationwp.thimpress.com","user_waiting_payment":null,"user_checkout":"","i18n_processing":"Processing","i18n_redirecting":"Redirecting","i18n_invalid_field":"Invalid field","i18n_unknown_error":"Unknown error","i18n_place_order":"Place order"};</script> <script type='text/javascript'>var lpProfileUserSettings = {"processing":"Processing","redirecting":"Redirecting","avatar_size":{"width":150,"height":150}};</script> <script type='text/javascript'>var lpCourseSettings = [""];</script> <script type='text/javascript'>var lpQuizSettings = [];</script> <script type="text/javascript">document.body.className = document.body.className.replace("siteorigin-panels-before-js","");</script>
      <div class="tp_chameleon_overlay">
         <div class="tp_chameleon_progress">
            <div class="tp_chameleon_heading">Processing!</div>
         </div>
      </div>
      <script type="text/javascript">var tp_chameleon_url_stylesheet = '../wp-content/themes/eduma/style-1.html';
         var tp_chameleon_url_admin_ajax = '../wp-admin/admin-ajax.html';
         var tp_chameleon_wp_nonce = 'e39f6066ef';
         var tp_chameleon_primary_color = 'rgb(255, 182, 6)';
         var tp_chameleon_selector_wrapper_box = '.content-pusher';
         var tp_chameleon_class_boxed = 'boxed-area';
         var tp_chameleon_src_patterns = '../wp-content/plugins/tp-chameleon/images/patterns/index.html';
         var tp_chameleon_setting = {
         	layout: tp_chameleon_getCookie('tp_chameleon_layout'),
         	pattern_type: tp_chameleon_getCookie('tp_chameleon_pattern_type'),
         	pattern_src: tp_chameleon_getCookie('tp_chameleon_pattern_src'),
         	primary_color: tp_chameleon_getCookie('tp_chameleon_primary_color'),
         	primary_color_rgb: tp_chameleon_getCookie('tp_chameleon_primary_color_rgb'),
         	body_font: tp_chameleon_getCookie('tp_chameleon_body_font'),
         	body_font_code: tp_chameleon_getCookie('tp_chameleon_body_font_code'),
         	heading_font: tp_chameleon_getCookie('tp_chameleon_heading_font'),
         	heading_font_code: tp_chameleon_getCookie('tp_chameleon_heading_font_code')
         };



         var tp_chameleon_site_url = '../index.html';

         function tp_chameleon_setCookie(cname, cvalue, exdays) {
         	var d = new Date();
         	d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
         	var expires = "expires=" + d.toUTCString();
         	document.cookie = cname + "=" + cvalue + "; " + expires;
         }

         function tp_chameleon_deleteCookie(cname) {
         	var d = new Date();
         	d.setTime(d.getTime() + (24 * 60 * 60 * 1000));
         	var expires = "expires=" + d.toUTCString();
         	document.cookie = cname + "=; " + expires;
         }

         function tp_chameleon_deleteAllCookie() {
         	var all_cookie = [
         		'tp_chameleon_layout',
         		'tp_chameleon_pattern_type',
         		'tp_chameleon_pattern_src',
         		'tp_chameleon_primary_color',
         		'tp_chameleon_primary_color_rgb',
         		'tp_chameleon_body_font',
         		'tp_chameleon_body_font_code',
         		'tp_chameleon_heading_font',
         		'tp_chameleon_heading_font_code'
         	];

         	for (var i = 0; i < all_cookie.length; i++) {
         		tp_chameleon_deleteCookie(all_cookie[i]);
         	}
         }

         function tp_chameleon_getCookie(cname) {
         	var name = cname + "=";
         	var ca = document.cookie.split(';');
         	for (var i = 0; i < ca.length; i++) {
         		var c = ca[i];
         		while (c.charAt(0) == ' ') c = c.substring(1);
         		if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
         	}

         	return '';
         }

         function tp_chameleon_set_first_visit() {
         	tp_chameleon_setCookie('tp_chameleon_first_visit', 1, 1);
         }

         function tp_chameleon_check_first_visit() {
         	return !(tp_chameleon_getCookie('tp_chameleon_first_visit') == '1' );
         }

         jQuery(document).ready(function ($) {
         	var $preview = $('.tp-preview-images');

         	$('.tp_demo').hover(
         		function (event) {
         			var url_prewview = $(this).attr('data-preview');
         			if( url_prewview ) {
         				$preview.find('img').attr('src', url_prewview);
         				$preview.show();
         			}
         		},
         		function () {
         			$preview.hide();
         		}
         	);

         	$('.tp_demo').mousemove(function (event) {
         		var y = (event.clientY);
         		$preview.css('top', y - 250);
         	});

         	function tp_chameleon_open() {
         		tp_chameleon_set_first_visit();
         		$('#tp_style_selector').addClass('show').animate({right: '0px'}, 'medium');
         		$('#tp_style_selector .open').hide();
         		$('#tp_style_selector .close').show();
         	}

         	function tp_chameleon_close() {
         		$('#tp_style_selector').removeClass('show').animate({right: '-300px'}, 'medium');
         		$('#tp_style_selector .close').hide();
         		$('#tp_style_selector .open').show();
         	}

         	function tp_form_newsletter_show() {
                    tp_chameleon_set_first_visit();
                    $(window).scroll(function () {
                        if ($(this).scrollTop() > 200) {
                            $('.tp-email-form .button-email').addClass('bounceInUp animated active');
                            var cookie_name = 'hide_form_email1';
                            $.cookie(cookie_name, '1', {expires: 3, path: '/'});
                        }
                    });
                }

         	function tp_change_background_pattern(url_pattern) {
         		var $body = $('body');
         		$body.removeClass('tp_background_image');
         		$body.addClass('tp_background_pattern');
         		$body.css('background-image', 'url("' + url_pattern + '")')
         	}

         	function tp_change_background_image(url_image) {
         		var $body = $('body');
         		$body.removeClass('tp_background_pattern');
         		$body.addClass('tp_background_image');
         		$body.css('background-image', 'url("' + url_image + '")')
         	}

         	function tp_chameleon_change_layout_wide() {
         		tp_chameleon_setCookie('tp_chameleon_layout', 'wide', 1);

         		var $body = $('body');
         		$('.tp-change-layout').removeClass('active');
         		$('.tp-change-layout.layout-wide').addClass('active');
         		$('#tp_style_selector .boxed-mode').slideUp(300);
         		$(tp_chameleon_selector_wrapper_box).removeClass(tp_chameleon_class_boxed);
         		$body.css('background-image', 'none');
         	}

         	function tp_chameleon_change_layout_boxed() {
         		tp_chameleon_setCookie('tp_chameleon_layout', 'boxed', 1);
         		$('.tp-change-layout').removeClass('active');
         		$('.tp-change-layout.layout-boxed').addClass('active');
         		$('#tp_style_selector .boxed-mode').slideDown(300);
         		$(tp_chameleon_selector_wrapper_box).addClass(tp_chameleon_class_boxed);
         	}

         	function tp_chameleon_change_background_pattern(pattern_src) {
         		tp_chameleon_setCookie('tp_chameleon_pattern_src', pattern_src, 1);
         		tp_chameleon_setCookie('tp_chameleon_pattern_type', 'pattern', 1);
         		var pattern_url = tp_chameleon_src_patterns + pattern_src;
         		tp_change_background_pattern(pattern_url);
         	}

         	function tp_chameleon_change_background_image(pattern_src) {
         		tp_chameleon_setCookie('tp_chameleon_pattern_src', pattern_src, 1);
         		tp_chameleon_setCookie('tp_chameleon_pattern_type', 'image', 1);
         		var pattern_url = tp_chameleon_src_patterns + pattern_src;
         		tp_change_background_image(pattern_url);
         	}

         	var $body_font = '<style id="tp_chameleon_body_font" type="text/css"></style>';
         	var $heading_font = '<style id="tp_chameleon_heading_font" type="text/css"></style>';
         	var $stylesheet = '<link id="tp_chameleon_stylesheet" type="text/css" rel="stylesheet">';

         	var $tp_head = $('head');
         	$tp_head.append($stylesheet);

         	var $tp_body = $('body');
         	$tp_body.append($body_font);
         	$tp_body.append($heading_font);

         	if (tp_chameleon_setting.layout == 'wide') {
         		tp_chameleon_change_layout_wide();
         	}
         	if (tp_chameleon_setting.layout == 'boxed') {
         		tp_chameleon_change_layout_boxed();

         		if (tp_chameleon_setting.pattern_type == 'pattern' && tp_chameleon_setting.pattern_src != '') {
         			tp_chameleon_change_background_pattern(tp_chameleon_setting.pattern_src);
         			$('.tp_pattern[data-src="' + tp_chameleon_setting.pattern_src + '"]').addClass('active');
         		}

         		if (tp_chameleon_setting.pattern_type == 'image' && tp_chameleon_setting.pattern_src != '') {
         			tp_chameleon_change_background_image(tp_chameleon_setting.pattern_src);
         			$('.tp_image[data-src="' + tp_chameleon_setting.pattern_src + '"]').addClass('active');
         		}
         	}

         	$('.tp-chameleon-clear').click(function (event) {
         		event.preventDefault();
         		tp_chameleon_deleteAllCookie();
         		document.location.reload();
         	});

         	$('.tp-btn.tp-change-layout').click(function (event) {
         		event.preventDefault();

         		if ($(this).hasClass('layout-wide')) {
         			tp_chameleon_change_layout_wide();

         						} else {
         			tp_chameleon_change_layout_boxed();

         						}
         	});

         	$('.tp_pattern').click(function (event) {
         		event.preventDefault();
         		$('.tp_pattern').removeClass('active');
         		$('.tp_image').removeClass('active');
         		$(this).addClass('active');
         		var pattern_src = $(this).attr('data-src');
         		tp_chameleon_change_background_pattern(pattern_src);
         	});

         	$('.tp_image').click(function (event) {
         		event.preventDefault();
         		$('.tp_pattern').removeClass('active');
         		$('.tp_image').removeClass('active');
         		$(this).addClass('active');
         		var pattern_src = $(this).attr('data-src');
         		tp_chameleon_change_background_image(pattern_src);
         	});

         	/**
         	 * Open/Close box
         	 */
         	$('#tp_style_selector .close').click(function (e) {
         		e.preventDefault();
         		tp_chameleon_close();
         	});

         	$('#tp_style_selector .open').click(function (e) {
         		e.preventDefault();
         		tp_chameleon_open();
         	});

         	$(window).load(function () {
         		var $btn = $('.tp-chameleon-btn-buy');
         		$btn.animate({
         			bottom: $btn.attr('data-bottom')
         		}, 500);

         		//Set view-more-slider button
         		$('.view-more-slider .text:last-child').hide();
         		setInterval(function(){
         			$('.view-more-slider .text:first-child').hide();
         			$('.view-more-slider .text:last-child').show();
         			setTimeout(function(){
         				$('.view-more-slider .text:first-child').show();
         				$('.view-more-slider .text:last-child').hide();
         			}, 2000);
         		}, 4500);

         	});

         	/**
         	 * Check firt visit
         	 */
         	if (tp_chameleon_check_first_visit()) {
         	    //set visit is second
         		setTimeout(tp_chameleon_open, 5000);
         	} else {
         		$('#tp_style_selector').click(function (event) {
         			tp_chameleon_set_first_visit();
         		});
         	}
                if (tp_chameleon_check_first_visit()) {
                    tp_form_newsletter_show();
                }


                $('.tp-email-form .button-email').click(function (e) {
                    e.preventDefault();
                    $('.tp-email-form .email-form-popup').addClass('show');
                });
                $('.tp-email-form .email-form-popup .close-popup').click(function (e) {
                    e.preventDefault();
                    $('.tp-email-form .email-form-popup').removeClass('show');
                    $('.tp-email-form .button-email').addClass('hide');
                });

                $('.tp-email-form .email-form-popup .email-form-subscribe form ').submit(function (e) {
                    e.preventDefault();
                    var form = $(this);
                    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                    form.append('<div class="loading"><i class="fa fa-spinner fa-pulse"></i></div>');
                    if(form.find('input[type="email"]').val()) {
                        $.ajax({
                            type: "POST",
                            url: 'https://preview.thimpress.com/m/mailster/subscribe',
                            data: form.serialize(),
                            complete: function (xhr, textStatus) {
                                form.find('.loading').remove();
                                form.append('<div class="message-success">Please check your inbox or spam folder for confirmation email!</div>');
                                setTimeout(function () {
                                    form.find('.message-success').remove();
                                }, 2000);
                            }
                        });
                    } else {
                        form.find('.loading').remove();
                        form.append('<div class="message-error">Please enter email address</div>');
                        setTimeout(function(){ form.find('.message-error').remove(); }, 2000);
                        form.find('input[type="email"]').addClass('error');
                    }


                });

                $('.tp-email-form .email-form-popup .email-form-subscribe form input[type="email"]').focus(function () {
                    $(this).removeClass('error');
                });
         });
      </script>
<script type="text/javascript" defer src="{{ asset('assets/wp-content/cache/autoptimize/1/js/autoptimize_4af6e292022a1e5d8764c643bd6aecae.js')}}
"></script>

<script type="text/javascript" src="{{ asset('assets/slider/ahmed2.js')}}" ></script>


   </body>
   <!-- Mirrored from educationwp.thimpress.com/contact/ by  **** Website Copier/3.x [XR&CO'2014], Sun, 11 Nov 2018 12:45:00 GMT -->
</html>

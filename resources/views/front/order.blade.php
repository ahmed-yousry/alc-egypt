@extends('front.layouts.master')
@section('title')
    طلبك
@endsection
@section('css')
    <link  rel="stylesheet" href="{{asset('assets/front/css/order.css')}}">
@endsection

@section('content')
<section class="secondry-page">
    <div class="main-text">
        <h2>طلبك</h2>
    </div>
</section>
<div class="order-form">
    <div class="container">
        <p class="text-center">لوريم ايبسوم هو نموذج افتراضي يوضع في التصاميم لتعرض على العميل ليتصور طريقه وضع النصوص بالتصاميم سواء كانت تصاميم مطبوعه ... بروشور او فلاير على سبيل المثال ... او نماذج مواقع انترنت ..</p>
        <div class="row">
            <div class="offset-md-1 col-md-10">
                <form  class="form">
                    <!-- <div class="text-center">
                    </div> -->
                    <h3 class="text-center"> بيانات الطلب </h3>
                    <div class="alert alert-success text-center" role="alert">
                        A simple success alert—check it out!
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label  class="col-sm-3 col-form-label col-form-label-sm">المدينة</label>
                            <select class="form-control col-sm-9" name="city">
                                <option>الاسكندرية</option>
                                <option>الاسكندرية</option>
                                <option>الاسكندرية</option>
                            </select>
                            <div class="offset-sm-3 col-sm-9 alert alert-danger" role="alert">
                                من فضلك املأ الحقل
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label  class="col-sm-3 col-form-label col-form-label-sm">نوع العقار</label>
                            <select class="form-control col-sm-9" name="type">
                                <option>شقة</option>
                                <option>شقة</option>
                                <option>شقة</option>
                            </select>
                            <div class="offset-sm-3 col-sm-9 alert alert-danger" role="alert">
                                من فضلك املأ الحقل
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label  class="col-sm-3 col-form-label col-form-label-sm">الحى</label>
                            <input type="text" class="form-control col-sm-9" name="district">
                            <div class="offset-sm-3 col-sm-9 alert alert-danger" role="alert">
                                من فضلك املأ الحقل
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label  class="col-sm-3 col-form-label col-form-label-sm">التشطيب</label>
                            <input type="text" class="form-control col-sm-9" name="final">
                            <div class="offset-sm-3 col-sm-9 alert alert-danger" role="alert">
                                من فضلك املأ الحقل
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label  class="col-sm-3 col-form-label col-form-label-sm">أقل سعر</label>
                            <input type="text" class="form-control col-sm-9" name="min-price">
                            <div class="offset-sm-3 col-sm-9 alert alert-danger" role="alert">
                                من فضلك املأ الحقل
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label  class="col-sm-3 col-form-label col-form-label-sm">أعلى سعر</label>
                            <input type="text" class="form-control col-sm-9" name="max-price">
                            <div class="offset-sm-3 col-sm-9 alert alert-danger" role="alert">
                                من فضلك املأ الحقل
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label  class="col-sm-3 col-form-label col-form-label-sm">أقل مساحة </label>
                            <input type="text" class="form-control col-sm-9" name="min-area">
                        </div>
                        <div class="form-group col-md-6">
                            <label  class="col-sm-3 col-form-label col-form-label-sm">أكبر مساحة </label>
                            <input type="text" class="form-control col-sm-9" name="max-area">
                        </div>
                        <div class="form-group col-md-6">
                            <label  class="col-sm-3 col-form-label col-form-label-sm"> بيع / ايجار </label>
                            <input type="text" class="form-control col-sm-9" name="rent">
                        </div>
                        <div class="form-group col-md-6">
                            <label  class="col-sm-3 col-form-label col-form-label-sm"> الاطلالة </label>
                            <input type="text" class="form-control col-sm-9" name="view">
                        </div>



                        <div class="btn-center">
                            <button type="button" class="btn btn-primary btn-block"data-toggle="modal" data-target="#acceptModal">  ارسال طلبك</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- Modal -->
            <div class="modal fade text-center" id="acceptModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <h2 class="title">رائع !! </h2>
                            <p>تم استلام طلبك وشنقوم بالرد عليك فى افرب وقت </p>
                            <a href="index.html"  class="btn btn-primary">العودة للصقخة الرئيسية</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
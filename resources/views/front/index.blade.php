<!DOCTYPE html>
<html itemscope itemtype="http://schema.org/WebPage" lang="en-US" prefix="og: http://ogp.me/ns#">
   <!-- Mirrored from educationwp.thimpress.com/ by  **** Website Copier/3.x [XR&CO'2014], Sun, 11 Nov 2018 12:38:09 GMT -->
   <!-- Added by  **** -->
   <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
   <!-- /Added by  **** -->
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="profile" href="https://gmpg.org/xfn/11">
      <link rel="pingback" href="xmlrpc.php">
      <script type="text/javascript">
         var ajaxurl = "wp-admin/admin-ajax.html";
      </script>
      <link type="text/css" media="screen" href="  {{ asset('assets/wp-content/cache/autoptimize/1/css/autoptimize_bcb2f3a50e74b496f21b148cbe50d29c.css')}}
         "
         rel="stylesheet" />
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <style type="text/css" media="print">
         @media print {
         .pmpro_a-print {
         display: none;
         position: absolute;
         left: -9999px
         }
         }
      </style>
      <link type="text/css" media="all" href="     {{ asset('assets/wp-content/cache/autoptimize/1/css/autoptimize_d0c3f53760d5bcca06bcef61319ce777.css')}}
         "
         rel="stylesheet" />
      <title>alexandria center  learn </title>
      <meta name="description" content="learn arabic, training center, education center, university, college, kindergarten, courses hub and academy." />
      <link rel="canonical" href="index.html" />
      <meta property="og:locale" content="en_US" />
      <meta property="og:type" content="website" />
      <meta property="og:title" content="acl-egypt.com" />
      <meta property="og:description" content="learn arabic training center, education center, university, college, kindergarten, courses hub and academy." />
      <!-- <meta property="og:url" content="localhost/" /> -->
      <meta property="og:site_name" content="Education WP" />
      <meta name="twitter:card" content="summary" />
      <meta name="twitter:description" content="learn arabic, training center, education center, university, college, kindergarten, courses hub and academy." />
      <meta name="twitter:title" content="#acl-egypt.com" />
      <!-- <meta name="twitter:image" content="localhost/wp-content/plugins/revslider/admin/assets/images/dummy.png" /> -->
      <script type='application/ld+json'>
         {
             "@context": "https:\/\/schema.org",
             "@type": "WebSite",
             "@id": "#website",
             "url": "https:\/\/educationwp.thimpress.com\/",
             "name": "Education WP",
             "potentialAction": {
                 "@type": "SearchAction",
                 "target": "https:\/\/educationwp.thimpress.com\/?s={search_term_string}",
                 "query-input": "required name=search_term_string"
             }
         }
      </script>
      <link rel='dns-prefetch' href='http://s.w.org/' />
      <link rel="alternate" type="application/rss+xml" title="Education WP &raquo; Feed" href="feed/index.html" />
      <style id='woocommerce-inline-inline-css' type='text/css'>
         .woocommerce form .form-row .required {
         visibility: visible;
         }
      </style>
      <style id='rs-plugin-settings-inline-css' type='text/css'>
         #rs-demo-id {}
      </style>
      <script type='text/javascript' src="{{ asset('assets/wp-includes/js/jquery/jquery.js')}}"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
      <script type="text/jscript" src="{{ asset('assets/slider/ahmed.js')}}" ></script>
      <!-- <script type="text/jscript" src="https://3ek5k1tux0822q3g83e30fye-wpengine.netdna-ssl.com/wp-content/cache/autoptimize/1/js/autoptimize_704b916c5e3900ea21f1833f144fad05.js" ></script> -->
      <script type='text/javascript'>
         var userSettings = {
             "url": "\/",
             "uid": "0",
             "time": "1541938698",
             "secure": "1"
         };
      </script>
      <link rel='https://api.w.org/' href='wp-json/index.html' />
      <link rel="EditURI" type="application/rsd+xml" title="RSD" href="xmlrpc0db0.php?rsd" />
      <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="   {{ asset('assets/wp-includes/wlwmanifest.xml')}}
         " />
      <link rel='shortlink' href='index.html' />
      <link rel="alternate" type="application/json+oembed" href="wp-json/oembed/1.0/embed2a82.json?url=https%3A%2F%2Feducationwp.thimpress.com%2F" />
      <link rel="alternate" type="text/xml+oembed" href="wp-json/oembed/1.0/embed85d0?url=https%3A%2F%2Feducationwp.thimpress.com%2F&amp;format=xml" />
      <noscript>
         <style>.woocommerce-product-gallery{ opacity: 1 !important; }</style>
      </noscript>
      <script type="text/javascript">
         function tc_insert_internal_css(css) {
             var tc_style = document.createElement("style");
             tc_style.type = "text/css";
             tc_style.setAttribute('data-type', 'tc-internal-css');
             var tc_style_content = document.createTextNode(css);
             tc_style.appendChild(tc_style_content);
             document.head.appendChild(tc_style);
         }
      </script>
      <script type="text/javascript"></script>
      <style type="text/css" media="all" id="siteorigin-panels-layouts-head">
         /* Layout 12 */
         #pgc-12-0-0,
         #pgc-12-2-0,
         #pgc-12-4-0,
         #pgc-12-5-0,
         #pgc-12-6-0,
         #pgc-12-7-0 {
         width: 100%;
         width: calc(100% - (0 * 30px))
         }
         #pl-12 #panel-12-0-0-0,
         #pl-12 #panel-12-1-0-0,
         #pl-12 #panel-12-1-1-0,
         #pl-12 #panel-12-1-2-0,
         #pl-12 #panel-12-2-0-0,
         #pl-12 #panel-12-2-0-1,
         #pl-12 #panel-12-3-0-0,
         #pl-12 #panel-12-3-0-1,
         #pl-12 #panel-12-3-1-0,
         #pl-12 #panel-12-4-0-0,
         #pl-12 #panel-12-4-0-1,
         #pl-12 #panel-12-5-0-0,
         #pl-12 #panel-12-5-0-1,
         #pl-12 #panel-12-6-0-0,
         #pl-12 #panel-12-6-0-1,
         #pl-12 #panel-12-7-0-0 {}
         #pg-12-0,
         #pg-12-1,
         #pg-12-3,
         #pg-12-4,
         #pg-12-5,
         #pl-12 .so-panel {
         margin-bottom: 30px
         }
         #pgc-12-1-0,
         #pgc-12-1-1,
         #pgc-12-1-2 {
         width: 33.3333%;
         width: calc(33.3333% - (0.666666666667 * 4px))
         }
         #pg-12-2,
         #pl-12 .so-panel:last-child {
         margin-bottom: 0px
         }
         #pgc-12-3-0,
         #pgc-12-3-1 {
         width: 50%;
         width: calc(50% - (0.5 * 30px))
         }
         #pg-12-6 {
         margin-bottom: 65px
         }
         #pg-12-0>.panel-row-style {
         height: 100vh
         }
         #pg-12-2>.panel-row-style {
         padding: 0px 0px 100px 0px
         }
         #pg-12-2.panel-no-style,
         #pg-12-2.panel-has-style>.panel-row-style,
         #pg-12-3.panel-no-style,
         #pg-12-3.panel-has-style>.panel-row-style {
         -webkit-align-items: flex-start;
         align-items: flex-start
         }
         #pg-12-3>.panel-row-style {
         background-image: url(http://localhost/empty-admin/assets/wp-content/uploads/2015/10/bg_register_now.jpg);
         background-position: center center;
         background-size: cover;
         min-height: 615px
         }
         #pgc-12-3-0 {
         align-self: auto
         }
         #pg-12-5>.panel-row-style {
         background-image: url(http://localhost/empty-admin/assets/wp-content/uploads/2015/10/bg_lastest_new.jpg);
         background-position: center center;
         background-size: cover;
         padding-bottom: 85px
         }
         #panel-12-5-0-0>.panel-widget-style {
         padding: 30px 0px 0px
         }
         @media (max-width:767px) {
         #pg-12-0.panel-no-style,
         #pg-12-0.panel-has-style>.panel-row-style,
         #pg-12-1.panel-no-style,
         #pg-12-1.panel-has-style>.panel-row-style,
         #pg-12-2.panel-no-style,
         #pg-12-2.panel-has-style>.panel-row-style,
         #pg-12-3.panel-no-style,
         #pg-12-3.panel-has-style>.panel-row-style,
         #pg-12-4.panel-no-style,
         #pg-12-4.panel-has-style>.panel-row-style,
         #pg-12-5.panel-no-style,
         #pg-12-5.panel-has-style>.panel-row-style,
         #pg-12-6.panel-no-style,
         #pg-12-6.panel-has-style>.panel-row-style,
         #pg-12-7.panel-no-style,
         #pg-12-7.panel-has-style>.panel-row-style {
         -webkit-flex-direction: column;
         -ms-flex-direction: column;
         flex-direction: column
         }
         #pg-12-0 .panel-grid-cell,
         #pg-12-1 .panel-grid-cell,
         #pg-12-2 .panel-grid-cell,
         #pg-12-3 .panel-grid-cell,
         #pg-12-4 .panel-grid-cell,
         #pg-12-5 .panel-grid-cell,
         #pg-12-6 .panel-grid-cell,
         #pg-12-7 .panel-grid-cell {
         margin-right: 0
         }
         #pg-12-0 .panel-grid-cell,
         #pg-12-1 .panel-grid-cell,
         #pg-12-2 .panel-grid-cell,
         #pg-12-3 .panel-grid-cell,
         #pg-12-4 .panel-grid-cell,
         #pg-12-5 .panel-grid-cell,
         #pg-12-6 .panel-grid-cell,
         #pg-12-7 .panel-grid-cell {
         width: 100%
         }
         #pgc-12-1-0,
         #pgc-12-1-1,
         #pgc-12-3-0 {
         margin-bottom: 30px
         }
         #pl-12 .panel-grid-cell {
         padding: 0
         }
         #pl-12 .panel-grid .panel-grid-cell-empty {
         display: none
         }
         #pl-12 .panel-grid .panel-grid-cell-mobile-last {
         margin-bottom: 0px
         }
         #pg-12-2>.panel-row-style {
         padding: 0px 0px 60px 0px
         }
         }
      </style>
      <link rel="icon" href="{{ asset('assets/wp-content/uploads/2015/10/favicon.png')}}" sizes="32x32" />
      <link rel="icon" href="{{ asset('assets/wp-content/uploads/2015/10/favicon.png')}}
         " sizes="192x192" />
      <link rel="apple-touch-icon-precomposed" href="" />
      <meta name="msapplication-TileImage" content="http://localhost/empty-admin/assets/wp-content/uploads/2015/10/favicon.png" />
      <script type="text/javascript">
         function setREVStartSize(e) {
             try {
                 e.c = jQuery(e.c);
                 var i = jQuery(window).width(),
                     t = 9999,
                     r = 0,
                     n = 0,
                     l = 0,
                     f = 0,
                     s = 0,
                     h = 0;
                 if (e.responsiveLevels && (jQuery.each(e.responsiveLevels, function (e, f) {
                         f > i && (t = r = f, l = e), i > f && f > r && (r = f, n = e)
                     }), t > r && (l = n)), f = e.gridheight[l] || e.gridheight[0] || e.gridheight, s = e.gridwidth[l] || e.gridwidth[0] ||
                     e.gridwidth, h = i / s, h = h > 1 ? 1 : h, f = Math.round(h * f), "fullscreen" == e.sliderLayout) {
                     var u = (e.c.width(), jQuery(window).height());
                     if (void 0 != e.fullScreenOffsetContainer) {
                         var c = e.fullScreenOffsetContainer.split(",");
                         if (c) jQuery.each(c, function (e, i) {
                                 u = jQuery(i).length > 0 ? u - jQuery(i).outerHeight(!0) : u
                             }), e.fullScreenOffset.split("%").length > 1 && void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 ?
                             u -= jQuery(window).height() * parseInt(e.fullScreenOffset, 0) / 100 : void 0 != e.fullScreenOffset && e.fullScreenOffset
                             .length > 0 && (u -= parseInt(e.fullScreenOffset, 0))
                     }
                     f = u
                 } else void 0 != e.minHeight && f < e.minHeight && (f = e.minHeight);
                 e.c.closest(".rev_slider_wrapper").css({
                     height: f
                 })
             } catch (d) {
                 console.log("Failure at Presize of Slider:" + d)
             }
         };
      </script>
      <script type="text/javascript">
         if (typeof ajaxurl === 'undefined') {

             var ajaxurl = "{{ asset('assets/wp-admin/admin-ajax.html')}}";

         }
      </script>
      <script>
         ! function (f, b, e, v, n, t, s) {
             if (f.fbq) return;
             n = f.fbq = function () {
                 n.callMethod ?
                     n.callMethod.apply(n, arguments) : n.queue.push(arguments);
             };
             if (!f._fbq) f._fbq = n;
             n.push = n;
             n.loaded = !0;
             n.version = '2.0';
             n.queue = [];
             t = b.createElement(e);
             t.async = !0;
             t.src = v;
             s = b.getElementsByTagName(e)[0];
             s.parentNode.insertBefore(t, s);
         }(window, document, 'script',
             'https://connect.facebook.net/en_US/fbevents.js');
         fbq('init', '978118969000044');
         fbq('track', 'PageView');
      </script>
      <noscript> <img height="1" width="1" src="https://www.facebook.com/tr?id=978118969000044&amp;ev=PageView&amp;noscript=1" />
      </noscript>
   </head>
   <body class="home page-template page-template-page-templates page-template-homepage page-template-page-templateshomepage-php page page-id-12 courses eduma learnpress learnpress-page woocommerce-no-js pmpro-body-has-access siteorigin-panels siteorigin-panels-before-js siteorigin-panels-home group-blog thim-body-preload bg-boxed-image"
      id="thim-body">
      <div id="preload">
         <div class="sk-folding-cube">
            <div class="sk-cube1 sk-cube"></div>
            <div class="sk-cube2 sk-cube"></div>
            <div class="sk-cube4 sk-cube"></div>
            <div class="sk-cube3 sk-cube"></div>
         </div>
      </div>
      <h1 class="title">Education WP</h1>
      <div id="wrapper-container" class="wrapper-container">
      <div class="content-pusher">
      @include('front.includes.header')
        @include('front.includes.nav')
      <div id="main-content">
      <div id="main-home-content" class="home-content home-page container" role="main">
         <div id="pl-12" class="panel-layout">
           <div id="pg-12-0" class="panel-grid panel-has-style">
             <div class="siteorigin-panels-stretch thim-fix-stretched panel-row-style panel-row-style-for-12-0"
              data-stretch-type="full-stretched">
               <div id="pgc-12-0-0" class="panel-grid-cell">
                 <div id="panel-12-0-0-0" class="so-panel widget widget_text panel-first-child panel-last-child" data-index="0">
                   <div class="textwidget">
                     <div id="rev_slider_6_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-source="gallery" style="background:transparent;padding:0px;background-image:url(wp-content/uploads/2016/02/top-slider.jpg);background-repeat:no-repeat;background-size:cover;background-position:center center;">
                       <div id="rev_slider_6_1" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.4.8">
                         <ul>


                          @foreach($Slider as $Sliders)


                           <li data-index="rs-24" data-transition="fade" data-slotamount="default" data-hideafterloop="0"
                            data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default"
                            data-thumb="https://educationwp.thimpress.com/wp-content/uploads/2016/02/slide_5-100x50.jpg" data-rotate="0"
                            data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4=""
                            data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10=""
                            data-description=""> <img src="wp-content/plugins/revslider/admin/assets/images/dummy.png" alt="" title="slide_5"

                              width="1600" height="900" data-lazyload="{{Request::root()}}/public/uploads/slider/{{$Sliders->photo}}"
                              data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg"
                              data-no-retina>
                             <!--<div class="tp-caption    thim-slider-sub-heading" id="slide-24-layer-1" data-x="['left','left','left','left']"-->
                             <!-- data-hoffset="['15','15','15','15']" data-y="['middle','middle','middle','middle']" data-voffset="['-146','-150','-45','-20']"-->
                             <!-- data-fontsize="['24','24','','16']" data-fontweight="['700','700','700','600']" data-width="none"-->
                             <!-- data-height="" data-whitespace="nowrap" data-type="text" data-responsive_offset="off" data-responsive="off"-->
                             <!-- data-frames='[{"delay":300,"split":"chars","split_direction":"forward","splitdelay":0.05,"speed":1800,"frame":"0","from":"y:[100%];z:0;rZ:-35deg;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":100,"frame":"999","to":"opacity:0;","ease":"Power2.easeIn"}]'-->
                             <!-- data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]"-->
                             <!-- data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5; max-width: px; max-width: px; white-space: nowrap; font-size: 24px; line-height: 30px; font-weight: 700; color: rgba(252,252,252,1); letter-spacing: px;">BETTER-->
                             <!--  EDUCATION FOR A BETTER</div>-->
                             <!--<h3 class="tp-caption    thim-slider-heading" id="slide-24-layer-2" data-x="['left','left','left','left']"-->
                             <!-- data-hoffset="['14','15','15','15']" data-y="['middle','middle','middle','middle']" data-voffset="['-75','-85','10','20']"-->
                             <!-- data-fontsize="['100','100','','48']" data-width="none" data-height="none" data-whitespace="nowrap"-->
                             <!-- data-type="text" data-responsive_offset="off" data-responsive="off" data-frames='[{"delay":500,"split":"chars","split_direction":"forward","splitdelay":0.05,"speed":2000,"frame":"0","from":"x:[105%];z:0;rX:45deg;rY:0deg;rZ:90deg;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":100,"frame":"999","to":"opacity:0;","ease":"Power2.easeIn"}]'-->
                             <!-- data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]"-->
                             <!-- data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 6; white-space: nowrap; font-size: 100px; line-height: 100px; font-weight: 700; color: rgba(255,255,255,1); letter-spacing: px;">WORLD</h3>-->
                     
                             <div class="tp-caption  " id="slide-24-layer-4" data-x="['center','center','center','center']"
                              data-hoffset="['0','0','0','0']" data-y="['bottom','bottom','bottom','bottom']" data-voffset="['40','40','30','20']"
                              data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-actions='[{"event":"click","action":"scrollbelow","offset":"0px","delay":"","speed":"500","ease":"Linear.easeNone"}]'
                              data-basealign="slide" data-responsive_offset="off" data-responsive="off" data-frames='[{"delay":-5,"speed":500,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"opacity:0;","ease":"nothing"}]'
                              data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]"
                              data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 8; white-space: nowrap; font-size: 14px; line-height: 14px; font-weight: 400; color: rgba(191,185,184,1); letter-spacing: 5;cursor:pointer;">
                               <div class="thim-click-to-bottom"><i class="fa  fa-chevron-down"></i></div>
                             </div>
                           </li>
                                   @endforeach

                         </ul>

                         <script>
                           var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
                           var htmlDivCss = "";
                           if (htmlDiv) {
                             htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                           } else {
                             var htmlDiv = document.createElement("div");
                             htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                             document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
                           }
                         </script>

                         <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
                       </div>
                       <script>
                         var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
                         var htmlDivCss = "";
                         if (htmlDiv) {
                           htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                         } else {
                           var htmlDiv = document.createElement("div");
                           htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                           document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
                         }
                       </script>
                       <script type="text/javascript">
                         if (setREVStartSize !== undefined) setREVStartSize({
                           c: '#rev_slider_6_1',
                           responsiveLevels: [1240, 1024, 778, 480],
                           gridwidth: [1200, 960, 768, 481],
                           gridheight: [500, 400, 400, 320],
                           sliderLayout: 'fullscreen',
                           fullScreenAutoWidth: 'on',
                           fullScreenAlignForce: 'off',
                           fullScreenOffsetContainer: '',
                           fullScreenOffset: ''
                         });

                         var revapi6,
                           tpj;
                         (function () {
                           if (!/loaded|interactive|complete/.test(document.readyState)) document.addEventListener(
                             "DOMContentLoaded", onLoad);
                           else onLoad();

                           function onLoad() {
                             if (tpj === undefined) {
                               tpj = jQuery;
                               if ("off" == "on") tpj.noConflict();
                             }
                             if (tpj("#rev_slider_6_1").revolution == undefined) {
                               revslider_showDoubleJqueryError("#rev_slider_6_1");
                             } else {
                               revapi6 = tpj("#rev_slider_6_1").show().revolution({
                                 sliderType: "standard",
                                 jsFileLocation: "//educationwp.thimpress.com/wp-content/plugins/revslider/public/assets/js/",
                                 sliderLayout: "fullscreen",
                                 dottedOverlay: "none",
                                 delay: 90,
                                 navigation: {
                                   keyboardNavigation: "off",
                                   keyboard_direction: "horizontal",
                                   mouseScrollNavigation: "off",
                                   mouseScrollReverse: "default",
                                   onHoverStop: "off",
                                   arrows: {
                                     style: "zeus",
                                     enable: true,
                                     hide_onmobile: true,
                                     hide_under: 1024,
                                     hide_onleave: true,
                                     hide_delay: 20,
                                     hide_delay_mobile: 100,
                                     tmp: '<div class="tp-title-wrap">  	<div class="tp-arr-imgholder"></div> </div>',
                                     left: {
                                       h_align: "left",
                                       v_align: "center",
                                       h_offset: 20,
                                       v_offset: 0
                                     },
                                     right: {
                                       h_align: "right",
                                       v_align: "center",
                                       h_offset: 20,
                                       v_offset: 0
                                     }
                                   }
                                 },
                                 responsiveLevels: [1240, 1024, 778, 480],
                                 visibilityLevels: [1240, 1024, 778, 480],
                                 gridwidth: [1200, 960, 768, 481],
                                 gridheight: [500, 400, 400, 320],
                                 lazyType: "single",
                                 shadow: 0,
                                 spinner: "spinner2",
                                 stopLoop: "off",
                                 stopAfterLoops: -1,
                                 stopAtSlide: -1,
                                 shuffle: "off",
                                 autoHeight: "off",
                                 fullScreenAutoWidth: "on",
                                 fullScreenAlignForce: "off",
                                 fullScreenOffsetContainer: "",
                                 fullScreenOffset: "",
                                 disableProgressBar: "on",
                                 hideThumbsOnMobile: "off",
                                 hideSliderAtLimit: 0,
                                 hideCaptionAtLimit: 0,
                                 hideAllCaptionAtLilmit: 0,
                                 debugMode: false,
                                 fallbacks: {
                                   simplifyAll: "off",
                                   nextSlideOnWindowFocus: "off",
                                   disableFocusListener: false,
                                   panZoomDisableOnMobile: "on",
                                 }
                               });
                             }; /* END OF revapi call */

                           }; /* END OF ON LOAD FUNCTION */
                         }()); /* END OF WRAPPING FUNCTION */
                       </script>
                       <script>
                         var htmlDivCss = ' #rev_slider_6_1_wrapper .tp-loader.spinner2{ background-color: #FFFFFF !important; } ';
                         var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
                         if (htmlDiv) {
                           htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                         } else {
                           var htmlDiv = document.createElement('div');
                           htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
                           document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
                         }
                       </script>
                       <script>
                         var htmlDivCss = unescape(
                           "%23rev_slider_6_1%20.zeus.tparrows%20%7B%0A%20%20cursor%3Apointer%3B%0A%20%20min-width%3A70px%3B%0A%20%20min-height%3A70px%3B%0A%20%20position%3Aabsolute%3B%0A%20%20display%3Ablock%3B%0A%20%20z-index%3A100%3B%0A%20%20border-radius%3A50%25%3B%20%20%20%0A%20%20overflow%3Ahidden%3B%0A%20%20background%3Argba%280%2C0%2C0%2C0.1%29%3B%0A%7D%0A%0A%23rev_slider_6_1%20.zeus.tparrows%3Abefore%20%7B%0A%20%20font-family%3A%20%22revicons%22%3B%0A%20%20font-size%3A20px%3B%0A%20%20color%3Argb%28255%2C%20255%2C%20255%29%3B%0A%20%20display%3Ablock%3B%0A%20%20line-height%3A%2070px%3B%0A%20%20text-align%3A%20center%3B%20%20%20%20%0A%20%20z-index%3A2%3B%0A%20%20position%3Arelative%3B%0A%7D%0A%23rev_slider_6_1%20.zeus.tparrows.tp-leftarrow%3Abefore%20%7B%0A%20%20content%3A%20%22%5Ce824%22%3B%0A%7D%0A%23rev_slider_6_1%20.zeus.tparrows.tp-rightarrow%3Abefore%20%7B%0A%20%20content%3A%20%22%5Ce825%22%3B%0A%7D%0A%0A%23rev_slider_6_1%20.zeus%20.tp-title-wrap%20%7B%0A%20%20background%3Argba%280%2C0%2C0%2C0.5%29%3B%0A%20%20width%3A100%25%3B%0A%20%20height%3A100%25%3B%0A%20%20top%3A0px%3B%0A%20%20left%3A0px%3B%0A%20%20position%3Aabsolute%3B%0A%20%20opacity%3A0%3B%0A%20%20transform%3Ascale%280%29%3B%0A%20%20-webkit-transform%3Ascale%280%29%3B%0A%20%20%20transition%3A%20all%200.3s%3B%0A%20%20-webkit-transition%3Aall%200.3s%3B%0A%20%20-moz-transition%3Aall%200.3s%3B%0A%20%20%20border-radius%3A50%25%3B%0A%20%7D%0A%23rev_slider_6_1%20.zeus%20.tp-arr-imgholder%20%7B%0A%20%20width%3A100%25%3B%0A%20%20height%3A100%25%3B%0A%20%20position%3Aabsolute%3B%0A%20%20top%3A0px%3B%0A%20%20left%3A0px%3B%0A%20%20background-position%3Acenter%20center%3B%0A%20%20background-size%3Acover%3B%0A%20%20border-radius%3A50%25%3B%0A%20%20transform%3Atranslatex%28-100%25%29%3B%0A%20%20-webkit-transform%3Atranslatex%28-100%25%29%3B%0A%20%20%20transition%3A%20all%200.3s%3B%0A%20%20-webkit-transition%3Aall%200.3s%3B%0A%20%20-moz-transition%3Aall%200.3s%3B%0A%0A%20%7D%0A%23rev_slider_6_1%20.zeus.tp-rightarrow%20.tp-arr-imgholder%20%7B%0A%20%20%20%20transform%3Atranslatex%28100%25%29%3B%0A%20%20-webkit-transform%3Atranslatex%28100%25%29%3B%0A%20%20%20%20%20%20%7D%0A%23rev_slider_6_1%20.zeus.tparrows%3Ahover%20.tp-arr-imgholder%20%7B%0A%20%20transform%3Atranslatex%280%29%3B%0A%20%20-webkit-transform%3Atranslatex%280%29%3B%0A%20%20opacity%3A1%3B%0A%7D%0A%20%20%20%20%20%20%0A%23rev_slider_6_1%20.zeus.tparrows%3Ahover%20.tp-title-wrap%20%7B%0A%20%20transform%3Ascale%281%29%3B%0A%20%20-webkit-transform%3Ascale%281%29%3B%0A%20%20opacity%3A1%3B%0A%7D%0A%20%0A"
                         );
                         var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
                         if (htmlDiv) {
                           htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                         } else {
                           var htmlDiv = document.createElement('div');
                           htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
                           document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
                         }
                       </script>
                     </div>
                   </div>
                 </div>
               </div>
             </div>
           </div>

        
            <div id="pg-12-2" class="panel-grid panel-has-style">
               <div class="panel-row-style panel-row-style-for-12-2">
                  <div id="pgc-12-2-0" class="panel-grid-cell">
                     <div id="panel-12-2-0-0" class="so-panel widget widget_heading panel-first-child" data-index="4">
                        <div class="thim-widget-heading thim-widget-heading-base">
                           <div class="sc_heading   text-left">
                              <h3 class="title">Popular Courses</h3>
                              <span class="line"></span>
                           </div>
                        </div>
                     </div>


                     <div id="panel-12-2-0-1" class="so-panel widget widget_courses panel-last-child" data-index="5">
                        <div class="thim-widget-courses thim-widget-courses-base">
                           <div class="thim-carousel-wrapper thim-course-carousel thim-course-grid" data-visible="4" data-pagination="0"
                              data-navigation="1" data-autoplay="0">
                                  @foreach($courses as $course)


                              <div class="course-item">
                                 <div class="course-thumbnail"> <a href="courses/node/index.html"> <img src="{{Request::root()}}/public/uploads/courses/{{$course->photo}}"
                                    alt="From Zero to Hero with Nodejs" title="course-1" width="400" height="300"> </a> <a class="course-readmore"
                                    href="{{url('learnpress')}}/{{$course->id}}">Read More</a></div>
                                 <div class="thim-course-content">
                                    <div class="course-author" itemscope itemtype="http://schema.org/Person">
                                       <img alt="Admin bar avatar" src="{{Request::root()}}/public/uploads/courses/{{$course->photo}}"
                                          class="avatar avatar-40 photo" height="40" width="40" />
                                       <div class="author-contain">
                                          <!--<div class="value" itemprop="name"> <a href="profile/keny/index.html">Keny White</a></div>-->
                                       </div>
                                    </div>
                                    <h2 class="course-title"><a href="{{url('learnpress')}}/{{$course->id}}" rel="bookmark">{{$course->courses_title}}</a></h2>
                                    <div class="course-meta">
                                       <div class="course-author" itemscope itemtype="http://schema.org/Person">
                                          <img alt="Admin bar avatar"
                                             src=" {{ asset('assets/wp-content/uploads/learn-press-profile/7/9c081444f942cc8fe0ddf55631b584e2.jpg')}}" class="avatar avatar-40 photo"
                                             height="40" width="40" />
                                          <div class="author-contain">
                                             <div class="value" itemprop="name"> <a href="profile/keny/index.html">Keny White</a></div>
                                          </div>
                                       </div>
                                       <div class="course-review">
                                          <label>Review</label>
                                          <div class="value">
                                             <div class="review-stars-rated">
                                                <ul class="review-stars">
                                                   <li><span class="fa fa-star-o"></span></li>
                                                   <li><span class="fa fa-star-o"></span></li>
                                                   <li><span class="fa fa-star-o"></span></li>
                                                   <li><span class="fa fa-star-o"></span></li>
                                                   <li><span class="fa fa-star-o"></span></li>
                                                </ul>
                                                <ul class="review-stars filled" style="width: calc(100% - 2px)">
                                                   <li><span class="fa fa-star"></span></li>
                                                   <li><span class="fa fa-star"></span></li>
                                                   <li><span class="fa fa-star"></span></li>
                                                   <li><span class="fa fa-star"></span></li>
                                                   <li><span class="fa fa-star"></span></li>
                                                </ul>
                                             </div>
                                             <span>(1 review)</span>
                                          </div>
                                       </div>
                                       <div class="course-students">
                                          <label>Students</label>
                                          <div class="value"><i class="fa fa-group"></i> 289</div>
                                          <span>students</span>
                                       </div>
                                       <div class="course-comments-count">
                                          <div class="value"><i class="fa fa-comment"></i>1</div>
                                       </div>
                                       <div class="course-price" itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
                                          <div class="value  free-course" itemprop="price"> Free</div>
                                          <meta itemprop="priceCurrency" content="USD" />
                                       </div>
                                    </div>
                                    <div class="course-description">
                                       <p>Build and deploy a few Nodejs, MongoDB & Expressjs apps while watching to lectures by the author of 9
                                          books on JS/Node.
                                       </p>
                                    </div>
                                    <div class="course-price" itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
                                       <div class="value  free-course" itemprop="price"> Free</div>
                                       <meta itemprop="priceCurrency" content="USD" />
                                    </div>
                                    <div class="course-readmore"> <a href="courses/node/index.html">Read More</a></div>
                                 </div>
                              </div>

                            @endforeach
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div id="pg-12-3" class="panel-grid panel-has-style">
               <div class="thim-bg-overlay siteorigin-panels-stretch panel-row-style panel-row-style-for-12-3"
                  data-stretch-type="full">
                  <div id="pgc-12-3-0" class="panel-grid-cell">
                     <div id="panel-12-3-0-0" class="so-panel widget widget_text panel-first-child" data-index="6">
                        <div class="textwidget">
                           <div class="thim-get-100s">
                              <p class="get-100s">Get 100s of online <span class="thim-color">Courses For Free</span></p>
                              <h2>Register Now</h2>
                           </div>
                        </div>
                     </div>
                     <div id="panel-12-3-0-1" class="so-panel widget widget_countdown-box panel-last-child" data-index="7">
                        <div class="thim-widget-countdown-box thim-widget-countdown-box-base">
                           <div class="text-left color-white" id="coming-soon-counter5be81e0c99cbd"></div>
                           <script type="text/javascript">
                              jQuery(function () {
                                  jQuery(document).ready(function () {
                                      jQuery("#coming-soon-counter5be81e0c99cbd").mbComingsoon({
                                          expiryDate: new Date(2018, 11, 20, 1),
                                          localization: {
                                              days: "days",
                                              hours: "hours",
                                              minutes: "minutes",
                                              seconds: "seconds"
                                          },
                                          speed: 100
                                      });
                                      setTimeout(function () {
                                          jQuery(window).resize();
                                      }, 200);
                                  });
                              });
                           </script>
                        </div>
                     </div>
                  </div>
                  <div id="pgc-12-3-1" class="panel-grid-cell">
                     <div id="panel-12-3-1-0" class="so-panel widget widget_text panel-first-child panel-last-child" data-index="8">
                        <div class="textwidget">
                           <div class="thim-register-now-form">
                              <h3 class="title"><span>Create your free account now and get immediate access to 100s of online courses.</span></h3>
                              <div role="form" class="wpcf7" id="wpcf7-f85-p12-o1" lang="en-US" dir="ltr">
                                 <div class="screen-reader-response"></div>
                                 <form action="localhost/#wpcf7-f85-p12-o1" method="post" class="wpcf7-form"
                                    novalidate="novalidate">
                                    <div style="display: none;"> <input type="hidden" name="_wpcf7" value="85" /> <input type="hidden" name="_wpcf7_version"
                                       value="5.0.3" /> <input type="hidden" name="_wpcf7_locale" value="en_US" /> <input type="hidden" name="_wpcf7_unit_tag"
                                       value="wpcf7-f85-p12-o1" /> <input type="hidden" name="_wpcf7_container_post" value="12" /></div>
                                    <p><span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40"
                                       class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false"
                                       placeholder="Your Name *" /></span></p>
                                    <p><span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40"
                                       class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                       aria-required="true" aria-invalid="false" placeholder="Email *" /></span></p>
                                    <p><span class="wpcf7-form-control-wrap phone"><input type="tel" name="phone" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel"
                                       aria-required="true" aria-invalid="false" placeholder="Phone *" /></span></p>
                                    <p><input type="submit" value="Get It Now" class="wpcf7-form-control wpcf7-submit" /></p>
                                    <div class="wpcf7-response-output wpcf7-display-none"></div>
                                 </form>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div id="pg-12-4" class="panel-grid panel-no-style">
               <div id="pgc-12-4-0" class="panel-grid-cell">
                  <div id="panel-12-4-0-0" class="so-panel widget widget_heading panel-first-child" data-index="9">
                     <div class="thim-widget-heading thim-widget-heading-base">
                        <div class="sc_heading   text-left">
                           <h3 class="title">ACTIVITIES</h3>
                           <p class="sub-heading" style="">Upcoming Education Activities to feed your brain.</p>
                           <span class="line"></span>
                        </div>
                     </div>
                  </div>





                  <div id="panel-12-4-0-1" class="so-panel widget widget_list-event panel-last-child" data-index="10">
                     <div class="thim-widget-list-event thim-widget-list-event-base">
                        <div class="thim-list-event">
                           <a class="view-all" href="{{url('activities')}}">View All</a>

@foreach($events as $event)
                           <div class="item-event post-2952 tp_event type-tp_event status-publish has-post-thumbnail hentry pmpro-has-access">
                              <div class="time-from">
                                 <div class="date"> {{$event->dates}}</div>
                                 <div class="month"> {{$event->month}}</div>
                              </div>
                              <div class="image"><img src="{{Request::root()}}/public/uploads/events/{{$event->image}}
                                 "
                                 alt="event-2" title="event-2"
                                 width="450" height="233"></div>
                              <div class="event-wrapper">
                                 <h5 class="title"> <a >{{$event->title}}</a>
                                 </h5>
                                 <div class="meta">
                                    <div class="time"> <i class="fa fa-clock-o"></i> {{$event->start_time}} - {{$event->finish_time}}</div>
                                    <div class="location"> <i class="fa fa-map-marker"></i> {{$event->address}}</div>
                                 </div>
                                 <div class="description">
                                    <p>{{$event->dscription}}
                                    </p>
                                 </div>
                              </div>
                           </div>
      @endforeach


                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div id="pg-12-5" class="panel-grid panel-has-style">
               <div class="thim-bg-overlay siteorigin-panels-stretch panel-row-style panel-row-style-for-12-5"
                  data-stretch-type="full">
                  <div id="pgc-12-5-0" class="panel-grid-cell">
                     <div id="panel-12-5-0-0" class="so-panel widget widget_heading panel-first-child" data-index="11">
                        <div class="panel-widget-style panel-widget-style-for-12-5-0-0">
                           <div class="thim-widget-heading thim-widget-heading-base">
                              <div class="sc_heading   text-left">
                                 <h3 style="color:#ffffff;" class="title">Latest actavites </h3>
                                 <p class="sub-heading" style="color:#ffffff;">Education actavites all over the world.</p>
                                 <span style="background-color:#ffffff"
                                    class="line"></span>
                              </div>
                           </div>
                        </div>
                     </div>


                     <div id="panel-12-5-0-1" class="so-panel widget widget_carousel-post panel-last-child" data-index="12">
                        <div class="thim-widget-carousel-post thim-widget-carousel-post-base">
                           <div class="thim-owl-carousel-post thim-carousel-wrapper" data-visible="3" data-pagination="0"
                              data-navigation="1" data-autoplay="0">
                              @foreach($events2 as $event)
                              <div class="item">
                                 <div class="image"> <a > <img src="{{Request::root()}}/public/uploads/events/{{$event->image}}"
                                    alt="Online Learning Glossary" title="blog-8" width="450" height="267"> </a></div>
                                 <div class="content">
                                    <div class="info">
                                       <div class="author"> <span>Anthony</span></div>
                                       <div class="date"> From:{{$event->start_time}}  To:{{$event->finish_time}}</div>
                                    </div>
                                    <h4 class="title"> <a >{{$event->title}}</a></h4>
                                 </div>
                              </div>
                            @endforeach
                           </div>
                        </div>
                     </div>



                  </div>
               </div>
            </div>
            <div id="pg-12-6" class="panel-grid panel-no-style">
               <div id="pgc-12-6-0" class="panel-grid-cell">
                  <div id="panel-12-6-0-0" class="so-panel widget widget_heading panel-first-child" data-index="13">
                     <div class="thim-widget-heading thim-widget-heading-base">
                        <div class="sc_heading   text-center">
                           <h3 class="title">our partner in success</h3>
                           <p class="sub-heading" style="">
We are proud to work with you</p>
                           <span class="line"></span>
                        </div>
                     </div>
                  </div>
                  <div id="panel-12-6-0-1" class="so-panel widget widget_testimonials panel-last-child" data-index="14">
                     <div class="thim-widget-testimonials thim-widget-testimonials-base">
                        <div class="thim-testimonial-slider" data-time="10000" data-visible="5" data-auto="1" data-mousewheel="0">

                            @foreach($people as $peoples)
                           <div class="item">
                              <div class="image"><img src=" {{Request::root()}}/public/uploads/people/{{$peoples->photo}}"
                                 alt="peter" title="peter" width="100"
                                 height="100"></div>
                              <div class="content">
                                 <h3 class="title">{{$peoples->name}}</h3>
                                 <div class="regency">{{$peoples->title}}</div>
                                 <div class="description">
                                    <p>
                                      {{$peoples->description}}
                                    </p>
                                 </div>
                              </div>
                           </div>

                             @endforeach




                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div id="pg-12-7" class="panel-grid panel-no-style">
               <div id="pgc-12-7-0" class="panel-grid-cell">
                  <div id="panel-12-7-0-0" class="so-panel widget widget_text panel-first-child panel-last-child" data-index="15">
                     <div class="textwidget">
                        <div class="thim-newlleter-homepage">
                           <p class="description">Subscribe now and receive weekly newsletter with educational materials, new courses,
                              interesting posts, popular books and much more!
                           </p>
                           <script>(function() {
                              if (!window.mc4wp) {
                                  window.mc4wp = {
                                      listeners: [],
                                      forms    : {
                                          on: function (event, callback) {
                                              window.mc4wp.listeners.push({
                                                  event   : event,
                                                  callback: callback
                                              });
                                          }
                                      }
                                  }
                              }
                              })();
                           </script>
                           <form id="mc4wp-form-1" class="mc4wp-form mc4wp-form-3101 mc4wp-form-basic" method="post" data-id="3101"
                              data-name="Default sign-up form">
                              <div class="mc4wp-form-fields"><input type="email" id="mc4wp_email" name="EMAIL" placeholder="Your email here"
                                 required /> <input type="submit" value="Subscribe" /></div>
                              <label style="display: none !important;">Leave
                              this field empty if you're human: <input type="text" name="_mc4wp_honeypot" value="" tabindex="-1"
                                 autocomplete="off" /></label><input type="hidden" name="_mc4wp_timestamp" value="1541938700" /><input
                                 type="hidden" name="_mc4wp_form_id" value="3101" /><input type="hidden" name="_mc4wp_form_element_id"
                                 value="mc4wp-form-1" />
                              <div class="mc4wp-response"></div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>

      </div>
      @include('front.includes.footer')
      <div class="tp-preview-images"> <img src="http://localhost/empty-admin/assets/wp-content/uploads/2017/06/eduma-demo-01.jpg"
         alt="preview image"></div>
      <div id="tp_chameleon_list_google_fonts"></div>
      <div class="gallery-slider-content"></div>
      <script data-cfasync="false" src="{{ asset('assets/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js')}}"></script>
      <script data-cfasync="false" type="text/javascript">
         window.onload = function () {
             var thim_preload = document.getElementById("preload");
             if (thim_preload) {
                 setTimeout(function () {
                     var body = document.getElementById("thim-body"),
                         len = body.childNodes.length,
                         class_name = body.className.replace(/(?:^|\s)thim-body-preload(?!\S)/, '').replace(
                             /(?:^|\s)thim-body-load-overlay(?!\S)/, '');

                     body.className = class_name;
                     if (typeof thim_preload !== 'undefined' && thim_preload !== null) {
                         for (var i = 0; i < len; i++) {
                             if (body.childNodes[i].id !== 'undefined' && body.childNodes[i].id == "preload") {
                                 body.removeChild(body.childNodes[i]);
                                 break;
                             }
                         }
                     }
                 }, 500);
             }
         };
      </script>
      <script>
         window.addEventListener('load', function () {
             /**
              * Fix issue there is an empty spacing between image and title of owl-carousel
              */
             setTimeout(function () {
                 var $ = jQuery;
                 var $carousel = $('.thim-owl-carousel-post').each(function () {
                     $(this).find('.image').css('min-height', 0);
                     $(window).trigger('resize');
                 });
             }, 500);
         })
      </script>
      <script data-cfasync="true" type="text/javascript">
         (function ($) {
             "use strict";
             $(document).on('click',
                 'body:not(".logged-in") .enroll-course .button-enroll-course, body:not(".logged-in") .purchase-course:not(".guest_checkout, .learn-press-pmpro-buy-membership") .button',
                 function (e) {
                     e.preventDefault();
                     $(this).parent().find('[name="redirect_to"]').val(
                         'account/indexc8f7.html?redirect_to=localhost/?enroll-course=12');
                     var redirect = $(this).parent().find('[name="redirect_to"]').val();
                     window.location = redirect;
                 });
         })(jQuery);
      </script>
      <script>(function() {function addEventListener(element,event,handler) {
         if(element.addEventListener) {
             element.addEventListener(event,handler, false);
         } else if(element.attachEvent){
             element.attachEvent('on'+event,handler);
         }
         }function maybePrefixUrlField() {
         if(this.value.trim() !== '' && this.value.indexOf('http') !== 0) {
             this.value = "http://" + this.value;
         }
         }

         var urlFields = document.querySelectorAll('.mc4wp-form input[type="url"]');
         if( urlFields && urlFields.length > 0 ) {
         for( var j=0; j < urlFields.length; j++ ) {
             addEventListener(urlFields[j],'blur',maybePrefixUrlField);
         }
         }/* test if browser supports date fields */
         var testInput = document.createElement('input');
         testInput.setAttribute('type', 'date');
         if( testInput.type !== 'date') {

         /* add placeholder & pattern to all date fields */
         var dateFields = document.querySelectorAll('.mc4wp-form input[type="date"]');
         for(var i=0; i<dateFields.length; i++) {
             if(!dateFields[i].placeholder) {
                 dateFields[i].placeholder = 'YYYY-MM-DD';
             }
             if(!dateFields[i].pattern) {
                 dateFields[i].pattern = '[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])';
             }
         }
         }

         })();
      </script>
      <style type="text/css" media="all" id="siteorigin-panels-layouts-footer">
         /* Layout tc-megamenu-7682 */
         #pgc-tc-megamenu-7682-0-0,
         #pgc-tc-megamenu-7682-0-1 {
         width: 31%;
         width: calc(31% - (0.69 * 30px))
         }
         #pl-tc-megamenu-7682 #panel-tc-megamenu-7682-0-0-0,
         #pl-tc-megamenu-7682 #panel-tc-megamenu-7682-0-1-0,
         #pl-tc-megamenu-7682 #panel-tc-megamenu-7682-0-2-0 {}
         #pgc-tc-megamenu-7682-0-2 {
         width: 38%;
         width: calc(38% - (0.62 * 30px))
         }
         #pl-tc-megamenu-7682 .so-panel {
         margin-bottom: 30px
         }
         #pl-tc-megamenu-7682 .so-panel:last-child {
         margin-bottom: 0px
         }
         @media (max-width:767px) {
         #pg-tc-megamenu-7682-0.panel-no-style,
         #pg-tc-megamenu-7682-0.panel-has-style>.panel-row-style {
         -webkit-flex-direction: column;
         -ms-flex-direction: column;
         flex-direction: column
         }
         #pg-tc-megamenu-7682-0 .panel-grid-cell {
         margin-right: 0
         }
         #pg-tc-megamenu-7682-0 .panel-grid-cell {
         width: 100%
         }
         #pgc-tc-megamenu-7682-0-0,
         #pgc-tc-megamenu-7682-0-1 {
         margin-bottom: 30px
         }
         #pl-tc-megamenu-7682 .panel-grid-cell {
         padding: 0
         }
         #pl-tc-megamenu-7682 .panel-grid .panel-grid-cell-empty {
         display: none
         }
         #pl-tc-megamenu-7682 .panel-grid .panel-grid-cell-mobile-last {
         margin-bottom: 0px
         }
         }
         /* Layout w57e9cc2c86af4 */
         #pgc-w57e9cc2c86af4-0-0 {
         width: 33.3%;
         width: calc(33.3% - (0.667 * 30px))
         }
         #pl-w57e9cc2c86af4 #panel-w57e9cc2c86af4-0-0-0,
         #pl-w57e9cc2c86af4 #panel-w57e9cc2c86af4-0-0-1,
         #pl-w57e9cc2c86af4 #panel-w57e9cc2c86af4-0-1-0,
         #pl-w57e9cc2c86af4 #panel-w57e9cc2c86af4-0-2-0,
         #pl-w57e9cc2c86af4 #panel-w57e9cc2c86af4-0-3-0,
         #pl-w57e9cc2c86af4 #panel-w57e9cc2c86af4-0-4-0 {}
         #pgc-w57e9cc2c86af4-0-1,
         #pgc-w57e9cc2c86af4-0-2,
         #pgc-w57e9cc2c86af4-0-3,
         #pgc-w57e9cc2c86af4-0-4 {
         width: 16.675%;
         width: calc(16.675% - (0.83325 * 30px))
         }
         #pl-w57e9cc2c86af4 .so-panel {
         margin-bottom: 30px
         }
         #pl-w57e9cc2c86af4 .so-panel:last-child {
         margin-bottom: 0px
         }
         @media (max-width:767px) {
         #pg-w57e9cc2c86af4-0.panel-no-style,
         #pg-w57e9cc2c86af4-0.panel-has-style>.panel-row-style {
         -webkit-flex-direction: column;
         -ms-flex-direction: column;
         flex-direction: column
         }
         #pg-w57e9cc2c86af4-0 .panel-grid-cell {
         margin-right: 0
         }
         #pg-w57e9cc2c86af4-0 .panel-grid-cell {
         width: 100%
         }
         #pgc-w57e9cc2c86af4-0-0,
         #pgc-w57e9cc2c86af4-0-1,
         #pgc-w57e9cc2c86af4-0-2,
         #pgc-w57e9cc2c86af4-0-3 {
         margin-bottom: 30px
         }
         #pl-w57e9cc2c86af4 .panel-grid-cell {
         padding: 0
         }
         #pl-w57e9cc2c86af4 .panel-grid .panel-grid-cell-empty {
         display: none
         }
         #pl-w57e9cc2c86af4 .panel-grid .panel-grid-cell-mobile-last {
         margin-bottom: 0px
         }
         }
         /* Layout w57babc3f18d14 */
         #pgc-w57babc3f18d14-0-0 {
         width: 100%;
         width: calc(100% - (0 * 30px))
         }
         #pl-w57babc3f18d14 #panel-w57babc3f18d14-0-0-0,
         #pl-w57babc3f18d14 #panel-w57babc3f18d14-0-0-1 {}
         #pg-w57babc3f18d14-0,
         #pl-w57babc3f18d14 .so-panel:last-child {
         margin-bottom: 0px
         }
         #pl-w57babc3f18d14 .so-panel {
         margin-bottom: 30px
         }
         #pg-w57babc3f18d14-0>.panel-row-style {
         background-image: url('http://localhost/empty-admin/assets/wp-content/uploads/2015/12/bg-footer.jpg');
         background-position: center center;
         background-size: cover;
         min-height: 450px
         }
         @media (max-width:767px) {
         #pg-w57babc3f18d14-0.panel-no-style,
         #pg-w57babc3f18d14-0.panel-has-style>.panel-row-style {
         -webkit-flex-direction: column;
         -ms-flex-direction: column;
         flex-direction: column
         }
         #pg-w57babc3f18d14-0 .panel-grid-cell {
         margin-right: 0
         }
         #pg-w57babc3f18d14-0 .panel-grid-cell {
         width: 100%
         }
         #pl-w57babc3f18d14 .panel-grid-cell {
         padding: 0
         }
         #pl-w57babc3f18d14 .panel-grid .panel-grid-cell-empty {
         display: none
         }
         #pl-w57babc3f18d14 .panel-grid .panel-grid-cell-mobile-last {
         margin-bottom: 0px
         }
         }
      </style>
      <script type="text/javascript">
         var c = document.body.className;
         c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
         document.body.className = c;
      </script>
      <script type="text/javascript">
         function revslider_showDoubleJqueryError(sliderID) {
             var errorMessage =
                 "Revolution Slider Error: You have some jquery.js library include that comes after the revolution files js include.";
             errorMessage += "<br> This includes make eliminates the revolution slider libraries, and make it not work.";
             errorMessage +=
                 "<br><br> To fix it you can:<br>&nbsp;&nbsp;&nbsp; 1. In the Slider Settings -> Troubleshooting set option:  <strong><b>Put JS Includes To Body</b></strong> option to true.";
             errorMessage += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jquery.js include and remove it.";
             errorMessage = "<span style='font-size:16px;color:#BC0C06;'>" + errorMessage + "</span>";
             jQuery(sliderID).show().html(errorMessage);
         }
      </script>
      <script type='text/javascript'>
         var wpcf7 = {
             "apiSettings": {
                 "root": "https:\/\/educationwp.thimpress.com\/wp-json\/contact-form-7\/v1",
                 "namespace": "contact-form-7\/v1"
             },
             "recaptcha": {
                 "messages": {
                     "empty": "Please verify that you are not a robot."
                 }
             },
             "cached": 0
         };
      </script>
      <script type='text/javascript'>
         var wc_add_to_cart_params = {
             "ajax_url": "\/wp-admin\/admin-ajax.php",
             "wc_ajax_url": "\/?wc-ajax=%%endpoint%%",
             "i18n_view_cart": "View cart",
             "cart_url": "https:\/\/educationwp.thimpress.com\/cart\/",
             "is_cart": "",
             "cart_redirect_after_add": "no"
         };
      </script>
      <script type='text/javascript'>
         var woocommerce_params = {
             "ajax_url": "\/wp-admin\/admin-ajax.php",
             "wc_ajax_url": "\/?wc-ajax=%%endpoint%%"
         };
      </script>
      <script type='text/javascript'>
         var wc_cart_fragments_params = {
             "ajax_url": "\/wp-admin\/admin-ajax.php",
             "wc_ajax_url": "\/?wc-ajax=%%endpoint%%",
             "cart_hash_key": "wc_cart_hash_8653f1d19ec105315cba116db9d7fe2e",
             "fragment_name": "wc_fragments_8653f1d19ec105315cba116db9d7fe2e"
         };
      </script>
      <script type='text/javascript'>
         var _wpUtilSettings = {
             "ajax": {
                 "url": "\/wp-admin\/admin-ajax.php"
             }
         };
      </script>
      <script type='text/javascript'>
         var WPEMS = {
             "gmt_offset": "0",
             "current_time": "Nov 11, 2018 12:18:00 +0000",
             "l18n": {
                 "labels": ["Years", "Months", "Weeks", "Days", "Hours", "Minutes", "Seconds"],
                 "labels1": ["Year", "Month", "Week", "Day", "Hour", "Minute", "Second"]
             },
             "ajaxurl": "https:\/\/educationwp.thimpress.com\/wp-admin\/admin-ajax.php",
             "something_wrong": "Something went wrong",
             "register_button": "c20eb61364"
         };
      </script>
      <script type='text/javascript'>
         var thim_js_translate = {
             "login": "Username",
             "password": "Password",
             "close": "Close"
         };
      </script>
      <script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js?ver=3.0.22'></script>
      <script type='text/javascript'>
         WebFont.load({
             google: {
                 families: ['Roboto Slab:300,400,700', 'Roboto:300,300i,400,400i,500,500i,700,700i']
             }
         });
      </script>
      <script type='text/javascript'>
         var panelsStyles = {
             "fullContainer": "body"
         };
      </script>
      <script type='text/javascript'>
         var mc4wp_forms_config = [];
      </script>
      <!--[if lte IE 9]> <script type='text/javascript' src='localhost/wp-content/plugins/mailchimp-for-wp/assets/js/third-party/placeholders.min.js?ver=4.2.4'></script> <![endif]-->
      <script type='text/javascript'>
         var lpGlobalSettings = {
             "url": null,
             "siteurl": "https:\/\/educationwp.thimpress.com",
             "ajax": "https:\/\/educationwp.thimpress.com\/wp-admin\/admin-ajax.php",
             "theme": "eduma",
             "localize": {
                 "button_ok": "OK",
                 "button_cancel": "Cancel",
                 "button_yes": "Yes",
                 "button_no": "No"
             }
         };
      </script>
      <script type='text/javascript'>
         var lpCheckoutSettings = {
             "ajaxurl": "https:\/\/educationwp.thimpress.com",
             "user_waiting_payment": null,
             "user_checkout": "",
             "i18n_processing": "Processing",
             "i18n_redirecting": "Redirecting",
             "i18n_invalid_field": "Invalid field",
             "i18n_unknown_error": "Unknown error",
             "i18n_place_order": "Place order"
         };
      </script>
      <script type='text/javascript'>
         var lpProfileUserSettings = {
             "processing": "Processing",
             "redirecting": "Redirecting",
             "avatar_size": {
                 "width": 150,
                 "height": 150
             }
         };
      </script>
      <script type='text/javascript'>
         var lpCourseSettings = [""];
      </script>
      <script type='text/javascript'>
         var lpQuizSettings = [];
      </script>
      <script type="text/javascript">
         document.body.className = document.body.className.replace("siteorigin-panels-before-js", "");
      </script>
      <div class="tp_chameleon_overlay">
         <div class="tp_chameleon_progress">
            <div class="tp_chameleon_heading">Processing!</div>
         </div>
      </div>
      <script type="text/javascript">
         var tp_chameleon_url_stylesheet = 'wp-content/themes/eduma/style-1.html';
         var tp_chameleon_url_admin_ajax = 'wp-admin/admin-ajax.html';
         var tp_chameleon_wp_nonce = 'e39f6066ef';
         var tp_chameleon_primary_color = 'rgb(255, 182, 6)';
         var tp_chameleon_selector_wrapper_box = '.content-pusher';
         var tp_chameleon_class_boxed = 'boxed-area';
         var tp_chameleon_src_patterns = 'wp-content/plugins/tp-chameleon/images/patterns/index.html';
         var tp_chameleon_setting = {
             layout: tp_chameleon_getCookie('tp_chameleon_layout'),
             pattern_type: tp_chameleon_getCookie('tp_chameleon_pattern_type'),
             pattern_src: tp_chameleon_getCookie('tp_chameleon_pattern_src'),
             primary_color: tp_chameleon_getCookie('tp_chameleon_primary_color'),
             primary_color_rgb: tp_chameleon_getCookie('tp_chameleon_primary_color_rgb'),
             body_font: tp_chameleon_getCookie('tp_chameleon_body_font'),
             body_font_code: tp_chameleon_getCookie('tp_chameleon_body_font_code'),
             heading_font: tp_chameleon_getCookie('tp_chameleon_heading_font'),
             heading_font_code: tp_chameleon_getCookie('tp_chameleon_heading_font_code')
         };



         var tp_chameleon_site_url = 'index.html';

         function tp_chameleon_setCookie(cname, cvalue, exdays) {
             var d = new Date();
             d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
             var expires = "expires=" + d.toUTCString();
             document.cookie = cname + "=" + cvalue + "; " + expires;
         }

         function tp_chameleon_deleteCookie(cname) {
             var d = new Date();
             d.setTime(d.getTime() + (24 * 60 * 60 * 1000));
             var expires = "expires=" + d.toUTCString();
             document.cookie = cname + "=; " + expires;
         }

         function tp_chameleon_deleteAllCookie() {
             var all_cookie = [
                 'tp_chameleon_layout',
                 'tp_chameleon_pattern_type',
                 'tp_chameleon_pattern_src',
                 'tp_chameleon_primary_color',
                 'tp_chameleon_primary_color_rgb',
                 'tp_chameleon_body_font',
                 'tp_chameleon_body_font_code',
                 'tp_chameleon_heading_font',
                 'tp_chameleon_heading_font_code'
             ];

             for (var i = 0; i < all_cookie.length; i++) {
                 tp_chameleon_deleteCookie(all_cookie[i]);
             }
         }

         function tp_chameleon_getCookie(cname) {
             var name = cname + "=";
             var ca = document.cookie.split(';');
             for (var i = 0; i < ca.length; i++) {
                 var c = ca[i];
                 while (c.charAt(0) == ' ') c = c.substring(1);
                 if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
             }

             return '';
         }

         function tp_chameleon_set_first_visit() {
             tp_chameleon_setCookie('tp_chameleon_first_visit', 1, 1);
         }

         function tp_chameleon_check_first_visit() {
             return !(tp_chameleon_getCookie('tp_chameleon_first_visit') == '1');
         }

         jQuery(document).ready(function ($) {
             var $preview = $('.tp-preview-images');

             $('.tp_demo').hover(
                 function (event) {
                     var url_prewview = $(this).attr('data-preview');
                     if (url_prewview) {
                         $preview.find('img').attr('src', url_prewview);
                         $preview.show();
                     }
                 },
                 function () {
                     $preview.hide();
                 }
             );

             $('.tp_demo').mousemove(function (event) {
                 var y = (event.clientY);
                 $preview.css('top', y - 250);
             });

             function tp_chameleon_open() {
                 tp_chameleon_set_first_visit();
                 $('#tp_style_selector').addClass('show').animate({
                     right: '0px'
                 }, 'medium');
                 $('#tp_style_selector .open').hide();
                 $('#tp_style_selector .close').show();
             }

             function tp_chameleon_close() {
                 $('#tp_style_selector').removeClass('show').animate({
                     right: '-300px'
                 }, 'medium');
                 $('#tp_style_selector .close').hide();
                 $('#tp_style_selector .open').show();
             }

             function tp_form_newsletter_show() {
                 tp_chameleon_set_first_visit();
                 $(window).scroll(function () {
                     if ($(this).scrollTop() > 200) {
                         $('.tp-email-form .button-email').addClass('bounceInUp animated active');
                         var cookie_name = 'hide_form_email1';
                         $.cookie(cookie_name, '1', {
                             expires: 3,
                             path: '/'
                         });
                     }
                 });
             }

             function tp_change_background_pattern(url_pattern) {
                 var $body = $('body');
                 $body.removeClass('tp_background_image');
                 $body.addClass('tp_background_pattern');
                 $body.css('background-image', 'url("' + url_pattern + '")')
             }

             function tp_change_background_image(url_image) {
                 var $body = $('body');
                 $body.removeClass('tp_background_pattern');
                 $body.addClass('tp_background_image');
                 $body.css('background-image', 'url("' + url_image + '")')
             }

             function tp_chameleon_change_layout_wide() {
                 tp_chameleon_setCookie('tp_chameleon_layout', 'wide', 1);

                 var $body = $('body');
                 $('.tp-change-layout').removeClass('active');
                 $('.tp-change-layout.layout-wide').addClass('active');
                 $('#tp_style_selector .boxed-mode').slideUp(300);
                 $(tp_chameleon_selector_wrapper_box).removeClass(tp_chameleon_class_boxed);
                 $body.css('background-image', 'none');
             }

             function tp_chameleon_change_layout_boxed() {
                 tp_chameleon_setCookie('tp_chameleon_layout', 'boxed', 1);
                 $('.tp-change-layout').removeClass('active');
                 $('.tp-change-layout.layout-boxed').addClass('active');
                 $('#tp_style_selector .boxed-mode').slideDown(300);
                 $(tp_chameleon_selector_wrapper_box).addClass(tp_chameleon_class_boxed);
             }

             function tp_chameleon_change_background_pattern(pattern_src) {
                 tp_chameleon_setCookie('tp_chameleon_pattern_src', pattern_src, 1);
                 tp_chameleon_setCookie('tp_chameleon_pattern_type', 'pattern', 1);
                 var pattern_url = tp_chameleon_src_patterns + pattern_src;
                 tp_change_background_pattern(pattern_url);
             }

             function tp_chameleon_change_background_image(pattern_src) {
                 tp_chameleon_setCookie('tp_chameleon_pattern_src', pattern_src, 1);
                 tp_chameleon_setCookie('tp_chameleon_pattern_type', 'image', 1);
                 var pattern_url = tp_chameleon_src_patterns + pattern_src;
                 tp_change_background_image(pattern_url);
             }

             var $body_font = '<style id="tp_chameleon_body_font" type="text/css"></style>';
             var $heading_font = '<style id="tp_chameleon_heading_font" type="text/css"></style>';
             var $stylesheet = '<link id="tp_chameleon_stylesheet" type="text/css" rel="stylesheet">';

             var $tp_head = $('head');
             $tp_head.append($stylesheet);

             var $tp_body = $('body');
             $tp_body.append($body_font);
             $tp_body.append($heading_font);

             if (tp_chameleon_setting.layout == 'wide') {
                 tp_chameleon_change_layout_wide();
             }
             if (tp_chameleon_setting.layout == 'boxed') {
                 tp_chameleon_change_layout_boxed();

                 if (tp_chameleon_setting.pattern_type == 'pattern' && tp_chameleon_setting.pattern_src != '') {
                     tp_chameleon_change_background_pattern(tp_chameleon_setting.pattern_src);
                     $('.tp_pattern[data-src="' + tp_chameleon_setting.pattern_src + '"]').addClass('active');
                 }

                 if (tp_chameleon_setting.pattern_type == 'image' && tp_chameleon_setting.pattern_src != '') {
                     tp_chameleon_change_background_image(tp_chameleon_setting.pattern_src);
                     $('.tp_image[data-src="' + tp_chameleon_setting.pattern_src + '"]').addClass('active');
                 }
             }

             $('.tp-chameleon-clear').click(function (event) {
                 event.preventDefault();
                 tp_chameleon_deleteAllCookie();
                 document.location.reload();
             });

             $('.tp-btn.tp-change-layout').click(function (event) {
                 event.preventDefault();

                 if ($(this).hasClass('layout-wide')) {
                     tp_chameleon_change_layout_wide();

                 } else {
                     tp_chameleon_change_layout_boxed();

                 }
             });

             $('.tp_pattern').click(function (event) {
                 event.preventDefault();
                 $('.tp_pattern').removeClass('active');
                 $('.tp_image').removeClass('active');
                 $(this).addClass('active');
                 var pattern_src = $(this).attr('data-src');
                 tp_chameleon_change_background_pattern(pattern_src);
             });

             $('.tp_image').click(function (event) {
                 event.preventDefault();
                 $('.tp_pattern').removeClass('active');
                 $('.tp_image').removeClass('active');
                 $(this).addClass('active');
                 var pattern_src = $(this).attr('data-src');
                 tp_chameleon_change_background_image(pattern_src);
             });

             /**
              * Open/Close box
              */
             $('#tp_style_selector .close').click(function (e) {
                 e.preventDefault();
                 tp_chameleon_close();
             });

             $('#tp_style_selector .open').click(function (e) {
                 e.preventDefault();
                 tp_chameleon_open();
             });

             $(window).load(function () {
                 var $btn = $('.tp-chameleon-btn-buy');
                 $btn.animate({
                     bottom: $btn.attr('data-bottom')
                 }, 500);

                 //Set view-more-slider button
                 $('.view-more-slider .text:last-child').hide();
                 setInterval(function () {
                     $('.view-more-slider .text:first-child').hide();
                     $('.view-more-slider .text:last-child').show();
                     setTimeout(function () {
                         $('.view-more-slider .text:first-child').show();
                         $('.view-more-slider .text:last-child').hide();
                     }, 2000);
                 }, 4500);

             });

             /**
              * Check firt visit
              */
             if (tp_chameleon_check_first_visit()) {
                 //set visit is second
                 setTimeout(tp_chameleon_open, 5000);
             } else {
                 $('#tp_style_selector').click(function (event) {
                     tp_chameleon_set_first_visit();
                 });
             }
             if (tp_chameleon_check_first_visit()) {
                 tp_form_newsletter_show();
             }

             if (top !== self && !$.browser.chrome) top.location.replace(self.location.href);

             $('.tp-email-form .button-email').click(function (e) {
                 e.preventDefault();
                 $('.tp-email-form .email-form-popup').addClass('show');
             });
             $('.tp-email-form .email-form-popup .close-popup').click(function (e) {
                 e.preventDefault();
                 $('.tp-email-form .email-form-popup').removeClass('show');
                 $('.tp-email-form .button-email').addClass('hide');
             });

             $('.tp-email-form .email-form-popup .email-form-subscribe form ').submit(function (e) {
                 e.preventDefault();
                 var form = $(this);
                 var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                 form.append('<div class="loading"><i class="fa fa-spinner fa-pulse"></i></div>');
                 if (form.find('input[type="email"]').val()) {
                     $.ajax({
                         type: "POST",
                         url: 'https://preview.thimpress.com/m/mailster/subscribe',
                         data: form.serialize(),
                         complete: function (xhr, textStatus) {
                             form.find('.loading').remove();
                             form.append(
                                 '<div class="message-success">Please check your inbox or spam folder for confirmation email!</div>');
                             setTimeout(function () {
                                 form.find('.message-success').remove();
                             }, 2000);
                         }
                     });
                 } else {
                     form.find('.loading').remove();
                     form.append('<div class="message-error">Please enter email address</div>');
                     setTimeout(function () {
                         form.find('.message-error').remove();
                     }, 2000);
                     form.find('input[type="email"]').addClass('error');
                 }


             });

             $('.tp-email-form .email-form-popup .email-form-subscribe form input[type="email"]').focus(function () {
                 $(this).removeClass('error');
             });
         });
      </script>
      <!-- <script type="text/javascript" defer src="  {{ asset('assets/wp-content/cache/autoptimize/1/js/autoptimize_947f2883f0c6ec4c291fda0b7357ce10.js')}}
         "></script> -->
   </body>
   <!-- Mirrored from educationwp.thimpress.com/ by  **** Website Copier/3.x [XR&CO'2014], Sun, 11 Nov 2018 12:39:32 GMT -->
</html>

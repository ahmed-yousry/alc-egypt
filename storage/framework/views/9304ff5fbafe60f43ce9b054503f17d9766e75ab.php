  <header id="masthead" class="site-header affix-top bg-custom-sticky sticky-header header_overlay header_v1">
                <div id="toolbar" class="toolbar">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="toolbar-container">
                                    <aside id="text-2" class="widget widget_text">
                                        <div class="textwidget">
                                            <div class="thim-have-any-question"> Have any question?<div class="mobile"><i class="fa fa-phone"></i><a
                                                     href="tel:00123456789" class="value">(03) 3931507</a></div>
                                                <div class="email"><i class="fa fa-envelope"></i><a ><span
                                                         class="__cf_email__" >info@acl-egypt.com</span></a></div>
                                            </div>
                                        </div>
                                    </aside>


                                    <aside id="login-popup-3" class="widget widget_login-popup">


                                        <div class="thim-widget-login-popup thim-widget-login-popup-base">





            <?php if(Route::has('login')): ?>

               <?php if(auth()->guard()->check()): ?>
               <div class="thim-link-login thim-login-popup">
               <a class="login" href="<?php echo e(url('/')); ?>"><?php echo e(\Illuminate\Support\Facades\Auth::guard('web')->user()->name); ?></a>

                                           <a class="login" href="<?php echo e(url('home/user/logout')); ?>">Logout</a>
                                           </div>




               <?php else: ?>
               <!-- <div class="thim-link-login thim-login-popup"> <a class="register" href="<?php echo e(url('/register')); ?>">Register</a>
                                                <a class="register" href="<?php echo e(url('/login')); ?>">Login</a>



                                                </div> -->

               <?php endif; ?>

       <?php endif; ?>





                                            <div id="thim-popup-login" class="has-shortcode">
                                                <div class="thim-login-container">


                                                    <script>



                                                        jQuery(".btn-mo").prop("disabled", false);
                                                    </script>
                                                    <script type="text/javascript">
                                                        function mo_openid_on_consent_change(checkbox, value) {

                                                            if (value == 0) {
                                                                jQuery('#mo_openid_consent_checkbox').val(1);
                                                                jQuery(".btn-mo").attr("disabled", true);
                                                                jQuery(".login-button").addClass("dis");
                                                            } else {
                                                                jQuery('#mo_openid_consent_checkbox').val(0);
                                                                jQuery(".btn-mo").attr("disabled", false);
                                                                jQuery(".login-button").removeClass("dis");
                                                                //jQuery(".btn-mo").removeAttr("disabled");
                                                            }
                                                        }


                                                    </script> <br> <br />
                                                    <div class='mo-openid-app-icons'>
                                                        <p style='color:#000000'> Login with social networks</p><a rel='nofollow' style='width: 240px !important;padding-top:11px !important;padding-bottom:11px !important;margin-bottom: -1px !important;border-radius: 4px !important;'
                                                         class='btn btn-mo btn-block btn-social btn-facebook btn-custom-dec login-button' onClick="moOpenIdLogin('facebook','false');">
                                                            <i style='padding-top:5px !important' class='fa fa-facebook'></i> Facebook</a><a rel='nofollow' style='width: 240px !important;padding-top:11px !important;padding-bottom:11px !important;margin-bottom: -1px !important;border-radius: 4px !important;'
                                                         class='btn btn-mo btn-block btn-social btn-google btn-custom-dec login-button' onClick="moOpenIdLogin('google','false');">
                                                            <i style='padding-top:5px !important' class='fa fa-google-plus'></i> Google</a><a rel='nofollow' style='width: 240px !important;padding-top:11px !important;padding-bottom:11px !important;margin-bottom: -1px !important;border-radius: 4px !important;'
                                                         class='btn btn-mo btn-block btn-social btn-twitter btn-custom-dec login-button' onClick="moOpenIdLogin('twitter','false');">
                                                            <i style='padding-top:5px !important' class='fa fa-twitter'></i> Twitter</a><a rel='nofollow' style='width: 240px !important;padding-top:11px !important;padding-bottom:11px !important;margin-bottom: -1px !important;border-radius: 4px !important;'
                                                         class='btn btn-mo btn-block btn-social btn-linkedin btn-custom-dec login-button' onClick="moOpenIdLogin('linkedin','false');">
                                                            <i style='padding-top:5px !important' class='fa fa-linkedin'></i> LinkedIn</a>
                                                    </div> <br>
                                                    <div class="thim-login">
                                                        <h2 class="title">Login with your site account</h2>
  <!-- ahmed  -->     <form name="loginform" id="loginform" method="POST" action="<?php echo e(route('login')); ?>">
           <?php echo csrf_field(); ?>



                    <p class="login-username">
                    <input type="email" name="email" placeholder="Email" id="thim_login" class="input <?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>" value="" size="20" required autofocus />




                                     <?php if($errors->has('email')): ?>
                                         <span class="invalid-feedback" role="alert">
                                             <strong><?php echo e($errors->first('email')); ?></strong>
                                         </span>
                                     <?php endif; ?>







 <p class="login-password">
    <input type="password" name="password" placeholder="Password" id="thim_pass" class="input <?php echo e($errors->has('password') ? ' is-invalid' : ''); ?>" value="" size="20"/>

 <?php if($errors->has('password')): ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('password')); ?></strong>
                                    </span>
                                <?php endif; ?>



  <a class="lost-pass-link" href="account/indexc2b6.html?action=lostpassword"
                                                             title="Lost Password">Lost your password?</a>
                                                            <p class="forgetmenot login-remember"> <label for="rememberme"><input name="rememberme" type="checkbox"
                                                                     id="rememberme" value="forever" /> Remember Me </label></p>
                                                            <p class="submit login-submit"> <input type="submit" name="wp-submit" id="wp-submit" class="button button-primary button-large"
                                                                 value="Login" /> <input type="hidden" name="redirect_to" value="index.html" /> <input type="hidden"
                                                                 name="testcookie" value="1" /></p>
                                                        </form>
                                                        <p class="link-bottom">Not a member yet? <a class="register" href="account/index0ddc.html?action=register">Register
                                                                now</a></p>
                                                    </div> <span class="close-popup"><i class="fa fa-times" aria-hidden="true"></i></span>
                                                </div>
                                            </div>
                                        </div>






















                                    </aside>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="navigation col-sm-12">
                            <div class="tm-table">
                                <div class="width-logo table-cell sm-logo"> <a href="<?php echo e(url('/')); ?>" title="Education WP - Education WordPress Theme"
                                     rel="home" class="no-sticky-logo">

                                     <img src="<?php echo e(asset('assets/wp-content/uploads/2015/11/logo.png')); ?>" alt="Education WP">




                                 </a><a
                                     href="<?php echo e(url('/')); ?>" rel="home" class="sticky-logo">

                                     <img src="<?php echo e(asset('assets/wp-content/uploads/2015/11/logo-sticky.png')); ?>" alt="Education WP">

                                 </a></div>
                                <nav class="width-navigation table-cell table-right">
                                    <ul class="nav navbar-nav menu-main-menu">


                                        <li id="menu-item-7679" class="menu-item menu-item-type-post_type_archive menu-item-object-tp_event menu-item-7679 tc-menu-item tc-menu-depth-0 tc-menu-align-left tc-menu-layout-default"><a
                                             href="<?php echo e(url('/')); ?>" class="tc-menu-inner">Home</a></li>

                                             <li id="menu-item-7679" class="menu-item menu-item-type-post_type_archive menu-item-object-tp_event menu-item-7679 tc-menu-item tc-menu-depth-0 tc-menu-align-left tc-menu-layout-default"><a
                                             href="<?php echo e(url('courses')); ?>" class="tc-menu-inner">Courses</a></li>

                                   <!--      <li id="menu-item-95" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-95 tc-menu-item tc-menu-depth-0 tc-menu-align-left tc-menu-layout-default"><span
                                             class="tc-menu-inner">Features</span>
                                            <ul class="sub-menu">
                                                <li id="menu-item-6550" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6550 tc-menu-item tc-menu-depth-1 tc-menu-align-left"><a
                                                     href="membership-account/membership-levels/index.html" class="tc-menu-inner tc-megamenu-title">Membership</a></li>
                                                <li id="menu-item-4451" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4451 tc-menu-item tc-menu-depth-1 tc-menu-align-left"><a
                                                     href="portfolio-masonry/index.html" class="tc-menu-inner tc-megamenu-title">Portfolio</a></li>
                                                <li id="menu-item-3437" class="menu-item menu-item-type-post_type_archive menu-item-object-forum menu-item-3437 tc-menu-item tc-menu-depth-1 tc-menu-align-left"><a
                                                     href="forums/index.html" class="tc-menu-inner tc-megamenu-title">Forums</a></li>
                                                <li id="menu-item-2924" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2924 tc-menu-item tc-menu-depth-1 tc-menu-align-left"><a
                                                     href="about-us/index.html" class="tc-menu-inner tc-megamenu-title">About Us</a></li>
                                                <li id="menu-item-96" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-96 tc-menu-item tc-menu-depth-1 tc-menu-align-left"><a
                                                     href="faqs/index.html" class="tc-menu-inner tc-megamenu-title">FAQs</a></li>
                                                <li id="menu-item-6418" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-6418 tc-menu-item tc-menu-depth-1 tc-menu-align-left"><a
                                                     href="demo-university-2/shop/index.html" class="tc-menu-inner tc-megamenu-title">Sidebar Shop</a></li>
                                                <li id="menu-item-3126" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3126 tc-menu-item tc-menu-depth-1 tc-menu-align-left"><a
                                                     href="404-page.html" class="tc-menu-inner tc-megamenu-title">404 Page</a></li>
                                            </ul>
                                        </li> -->
                                        <li id="menu-item-7679" class="menu-item menu-item-type-post_type_archive menu-item-object-tp_event menu-item-7679 tc-menu-item tc-menu-depth-0 tc-menu-align-left tc-menu-layout-default"><a
                                             href="<?php echo e(url('about')); ?>" class="tc-menu-inner">About Us</a></li>
                                             <li id="menu-item-7679" class="menu-item menu-item-type-post_type_archive menu-item-object-tp_event menu-item-7679 tc-menu-item tc-menu-depth-0 tc-menu-align-left tc-menu-layout-default"><a
                                                  href="<?php echo e(url('services')); ?>" class="tc-menu-inner">Services and Facilities</a></li>


                                        <li id="menu-item-7679" class="menu-item menu-item-type-post_type_archive menu-item-object-tp_event menu-item-7679 tc-menu-item tc-menu-depth-0 tc-menu-align-left tc-menu-layout-default"><a
                                             href="<?php echo e(url('activities')); ?>" class="tc-menu-inner">Activities </a></li>
                                        <li id="menu-item-4528" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4528 tc-menu-item tc-menu-depth-0 tc-menu-align-left tc-menu-layout-default"><a
                                             href="<?php echo e(url('gallery')); ?>" class="tc-menu-inner">Gallery</a></li>
                                        <li id="menu-item-127" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-127 tc-menu-item tc-menu-depth-0 tc-menu-align-left tc-menu-layout-default"><a
                                             href="<?php echo e(url('blog')); ?>" class="tc-menu-inner">Blog</a></li>
                                        <li id="menu-item-99" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-99 tc-menu-item tc-menu-depth-0 tc-menu-align-left tc-menu-layout-default"><a
                                             href="<?php echo e(url('contact')); ?>" class="tc-menu-inner">Contact</a></li>

                                        <li class="menu-right">
                                            <ul>
                                                <li id="courses-searching-3" class="widget widget_courses-searching">
                                                    <div class="thim-widget-courses-searching thim-widget-courses-searching-base">
                                                        <div class="thim-course-search-overlay">
                                                            <div class="search-toggle"><i class="fa fa-search"></i></div>
                                                            <div class="courses-searching layout-overlay">
                                                                <div class="search-popup-bg"></div>
                                                                <form method="get" action="http://localhost/empty-admin/courses/"> <input type="text" value=""
                                                                     name="s" placeholder="Search courses..." class="thim-s form-control courses-search-input"
                                                                     autocomplete="off" /> <input type="hidden" value="course" name="ref" /> <button type="submit"><i
                                                                         class="fa fa-search"></i></button> <span class="widget-search-close"></span></form>
                                                                <ul class="courses-list-search list-unstyled"></ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </nav>
                                <div class="menu-mobile-effect navbar-toggle" data-effect="mobile-effect"> <span class="icon-bar"></span> <span
                                     class="icon-bar"></span> <span class="icon-bar"></span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>

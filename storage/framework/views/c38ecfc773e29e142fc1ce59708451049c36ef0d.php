<?php $__env->startSection('title'); ?>
    Admin | services
<?php $__env->stopSection(); ?>
<?php $__env->startSection('page-header'); ?>
    <section class="content-header">
        <h1>
services

    <small></small>
        </h1>

    </section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <section class="content">

        <div class="row">
            <div class="col-md-12">

                    <a href="<?php echo e(url('/admin/services/create')); ?>" class="btn btn-primary pull-right margin-bottom">
                        <i class="fa fa-plus"></i>
                        Add new
                    </a>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">
                            Show All </h3>
                        <div class="box-tools">
                            <div class="input-group input-group-sm" style="width: 150px;">
                                <input type="text" name="table_search" class="form-control pull-right"
                                       placeholder="Search">

                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-default">
                                        <i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tr>
                                <th>ID</th>

                                  <th>Photo</th>
                                  <th>		dscription</th>


                                <th>Action</th>
                            </tr>
                            <?php $__currentLoopData = $courses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $course): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e($course->id); ?></td>

                                    <a href="<?php echo e(Request::root()); ?>/public/uploads/services/<?php echo e($course->image); ?>" data-lightbox="image-1">
                                        <td><img src="<?php echo e(Request::root()); ?>/public/uploads/services/<?php echo e($course->image); ?>"width="90px" height="90px" alt=""></td>
                                    </a>
                                    <td><?php echo e($course->dscription); ?></td>


                                    <td>
                                            <a href="<?php echo e(url('/admin/services/'.$course->id.'/edit')); ?>" class="btn btn-info btn-circle"><i class="fa fa-edit"></i></a>


                                                <a href="<?php echo e(url('/admin/services/'.$course->id.'/delete')); ?>" class="btn btn-danger btn-circle"><i class="fa fa-trash-o"></i></a>

                                    </td>
                                </tr>


                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix">
                        <ul class="pagination pagination-sm no-margin pull-right">
                            <li><a href="#">&laquo;</a></li>
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">&raquo;</a></li>
                        </ul>
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>

        <br>

    </section>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('assets/bower_components/lightbox2-master/lightbox.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>

    <script src="<?php echo e(asset('assets/bower_components/lightbox2-master/lightbox.js')); ?>"></script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection('title'); ?>
Blog
<?php $__env->stopSection(); ?>
 <?php echo $__env->make('front.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<body class="page-template-default page page-id-4526 courses eduma learnpress learnpress-page woocommerce-no-js pmpro-body-has-access siteorigin-panels siteorigin-panels-before-js group-blog thim-body-preload bg-boxed-image"
 id="thim-body">
	<div id="preload">
		<div class="sk-folding-cube">
			<div class="sk-cube1 sk-cube"></div>
			<div class="sk-cube2 sk-cube"></div>
			<div class="sk-cube4 sk-cube"></div>
			<div class="sk-cube3 sk-cube"></div>
		</div>
	</div>
	<div id="wrapper-container" class="wrapper-container">
		<div class="content-pusher">












<?php echo $__env->make('front.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>














































<?php echo $__env->make('front.includes.nav', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
			<div id="main-content">
				<section class="content-area">
					<div class="top_heading  _out">
						<div class="top_site_main " style="color: #ffffff;background-image:url(<?php echo e(asset('assets/wp-content/themes/eduma/images/bg-page.jpg')); ?>);">
							<span class="overlay-top-header" style="background:rgba(0,0,0,0.5);"></span>
							<div class="page-title-wrapper">
								<div class="banner-wrapper container">
									<h1>Blog</h1>
								</div>
							</div>
						</div>
						<div class="breadcrumbs-wrapper">
							<div class="container">
								<ul itemprop="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList" id="breadcrumbs" class="breadcrumbs">
									<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" href="../index.html"
										 title="Home"><span itemprop="name">Home</span></a></li>
									<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name" title="Gallery">
											Blog</span></li>
								</ul>
							</div>
						</div>
					</div>
				<div class="container site-content sidebar-right">
						<div class="row">
							<main id="main" class="site-main col-sm-9 alignleft">
								<div id="blog-archive" class="blog-content">
									<div class="row">


  <?php $__currentLoopData = $blogs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $blog): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

										<article id="post-3696" class="col-sm-12 post-3696 post type-post status-publish format-standard has-post-thumbnail hentry category-business tag-wordpress pmpro-has-access">
											<div class="content-inner">
												<div class='post-formats-wrapper'><a class="post-image" href="../why-you-should-read-every-day/index.html"><img
														 width="870" height="445" src="<?php echo e(Request::root()); ?>/public/uploads/blog/<?php echo e($blog->image); ?>" class="attachment-full size-full wp-post-image"
														 alt="" srcset="<?php echo e(Request::root()); ?>/public/uploads/blog/<?php echo e($blog->image); ?>"
														 sizes="(max-width: 870px) 100vw, 870px" /></a></div>
												<div class="entry-content">
													<header class="entry-header">
														<div class="date-meta"> 20<i> <?php echo e($blog->created_at); ?></i></div>
														<div class="entry-contain">
															<h2 class="entry-title"><a  rel="bookmark"><?php echo e($blog->title); ?></a></h2>
															<ul class="entry-meta">
																<li class="author"> <span>Posted by</span> <span class="vcard author author_name"><a >Dr/magda</a></span></li>

															</ul>
														</div>
													</header>
													<div class="entry-summary">
														<p><?php echo e($blog->description); ?></p>
													</div>
													<!-- <div class="readmore"> <a href="../why-you-should-read-every-day/index.html">Read More</a></div> -->
												</div>
											</div>
										</article>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php $__currentLoopData = $blogvedio; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $blog): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

										<article id="post-130" class="col-sm-12 post-130 post type-post status-publish format-video hentry category-design-branding tag-designer tag-thimpress post_format-post-format-video pmpro-has-access">
											<div class="content-inner">
												<div class='post-formats-wrapper'><iframe src="<?php echo e($blog->vedio); ?>" width="100%" height="490"
													 frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>
												<div class="entry-content">
													<header class="entry-header">
														<div class="date-meta"> 18<i> October</i></div>
														<div class="entry-contain">
															<h2 class="entry-title"><a  rel="bookmark"><?php echo e($blog->title); ?></a></h2>
															<ul class="entry-meta">
																<li class="author"> <span>Posted by</span> <span class="vcard author author_name"><a>Dr/magada
																			</a></span></li>
																<li class="entry-category"> <span>Categories</span> <a href="../category/design-branding/index.html"
																	 rel="category tag">Design / Branding</a></li>
															</ul>
														</div>
													</header>
													<div class="entry-summary">
														<p><?php echo e($blog->description); ?></p>
													</div>

												</div>
											</div>
										</article>

                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

									</div>
								</div>

							</main>

						</div>
					</div>
				</section>


































<?php echo $__env->make('front.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>




























			</div>
		</div> <a href="#" id="back-to-top"> <i class="fa fa-chevron-up" aria-hidden="true"></i> </a>
	</div>

	<div class="tp-preview-images"> <img src="*"
		 alt="preview image"></div>
	<div id="tp_chameleon_list_google_fonts"></div>
	<div class="gallery-slider-content"></div>
	<script data-cfasync="false" src="../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
	<script data-cfasync="false" type="text/javascript">window.onload = function () {
			var thim_preload = document.getElementById("preload");
			if (thim_preload) {
				setTimeout(function () {
					var body = document.getElementById("thim-body"),
						len = body.childNodes.length,
						class_name = body.className.replace(/(?:^|\s)thim-body-preload(?!\S)/, '').replace(/(?:^|\s)thim-body-load-overlay(?!\S)/, '');

					body.className = class_name;
					if (typeof thim_preload !== 'undefined' && thim_preload !== null) {
						for (var i = 0; i < len; i++) {
							if (body.childNodes[i].id !== 'undefined' && body.childNodes[i].id == "preload") {
								body.removeChild(body.childNodes[i]);
								break;
							}
						}
					}
				}, 500);
			}
		};</script>
	<script>window.addEventListener('load', function () {
			/**
			 * Fix issue there is an empty spacing between image and title of owl-carousel
			 */
			setTimeout(function () {
				var $ = jQuery;
				var $carousel = $('.thim-owl-carousel-post').each(function () {
					$(this).find('.image').css('min-height', 0);
					$(window).trigger('resize');
				});
			}, 500);
		})</script>
	<script data-cfasync="true" type="text/javascript">(function ($) {
				 "use strict";
				 $(document).on('click', 'body:not(".logged-in") .enroll-course .button-enroll-course, body:not(".logged-in") .purchase-course:not(".guest_checkout, .learn-press-pmpro-buy-membership") .button', function (e) {
					 e.preventDefault();
					 $(this).parent().find('[name="redirect_to"]').val('../account/index3957.html?redirect_to=localhost/gallery/?enroll-course=4526');
					 var redirect = $(this).parent().find('[name="redirect_to"]').val();
					 window.location = redirect;
				 });
			 })(jQuery);</script>
	<style type="text/css" media="all" id="siteorigin-panels-layouts-footer">
		/* Layout tc-megamenu-7682 */
		#pgc-tc-megamenu-7682-0-0,
		#pgc-tc-megamenu-7682-0-1 {
			width: 31%;
			width: calc(31% - (0.69 * 30px))
		}

		#pl-tc-megamenu-7682 #panel-tc-megamenu-7682-0-0-0,
		#pl-tc-megamenu-7682 #panel-tc-megamenu-7682-0-1-0,
		#pl-tc-megamenu-7682 #panel-tc-megamenu-7682-0-2-0 {}

		#pgc-tc-megamenu-7682-0-2 {
			width: 38%;
			width: calc(38% - (0.62 * 30px))
		}

		#pl-tc-megamenu-7682 .so-panel {
			margin-bottom: 30px
		}

		#pl-tc-megamenu-7682 .so-panel:last-child {
			margin-bottom: 0px
		}

		@media (max-width:767px) {

			#pg-tc-megamenu-7682-0.panel-no-style,
			#pg-tc-megamenu-7682-0.panel-has-style>.panel-row-style {
				-webkit-flex-direction: column;
				-ms-flex-direction: column;
				flex-direction: column
			}

			#pg-tc-megamenu-7682-0 .panel-grid-cell {
				margin-right: 0
			}

			#pg-tc-megamenu-7682-0 .panel-grid-cell {
				width: 100%
			}

			#pgc-tc-megamenu-7682-0-0,
			#pgc-tc-megamenu-7682-0-1 {
				margin-bottom: 30px
			}

			#pl-tc-megamenu-7682 .panel-grid-cell {
				padding: 0
			}

			#pl-tc-megamenu-7682 .panel-grid .panel-grid-cell-empty {
				display: none
			}

			#pl-tc-megamenu-7682 .panel-grid .panel-grid-cell-mobile-last {
				margin-bottom: 0px
			}
		}

		/* Layout w57e9cc2c86af4 */
		#pgc-w57e9cc2c86af4-0-0 {
			width: 33.3%;
			width: calc(33.3% - (0.667 * 30px))
		}

		#pl-w57e9cc2c86af4 #panel-w57e9cc2c86af4-0-0-0,
		#pl-w57e9cc2c86af4 #panel-w57e9cc2c86af4-0-0-1,
		#pl-w57e9cc2c86af4 #panel-w57e9cc2c86af4-0-1-0,
		#pl-w57e9cc2c86af4 #panel-w57e9cc2c86af4-0-2-0,
		#pl-w57e9cc2c86af4 #panel-w57e9cc2c86af4-0-3-0,
		#pl-w57e9cc2c86af4 #panel-w57e9cc2c86af4-0-4-0 {}

		#pgc-w57e9cc2c86af4-0-1,
		#pgc-w57e9cc2c86af4-0-2,
		#pgc-w57e9cc2c86af4-0-3,
		#pgc-w57e9cc2c86af4-0-4 {
			width: 16.675%;
			width: calc(16.675% - (0.83325 * 30px))
		}

		#pl-w57e9cc2c86af4 .so-panel {
			margin-bottom: 30px
		}

		#pl-w57e9cc2c86af4 .so-panel:last-child {
			margin-bottom: 0px
		}

		@media (max-width:767px) {

			#pg-w57e9cc2c86af4-0.panel-no-style,
			#pg-w57e9cc2c86af4-0.panel-has-style>.panel-row-style {
				-webkit-flex-direction: column;
				-ms-flex-direction: column;
				flex-direction: column
			}

			#pg-w57e9cc2c86af4-0 .panel-grid-cell {
				margin-right: 0
			}

			#pg-w57e9cc2c86af4-0 .panel-grid-cell {
				width: 100%
			}

			#pgc-w57e9cc2c86af4-0-0,
			#pgc-w57e9cc2c86af4-0-1,
			#pgc-w57e9cc2c86af4-0-2,
			#pgc-w57e9cc2c86af4-0-3 {
				margin-bottom: 30px
			}

			#pl-w57e9cc2c86af4 .panel-grid-cell {
				padding: 0
			}

			#pl-w57e9cc2c86af4 .panel-grid .panel-grid-cell-empty {
				display: none
			}

			#pl-w57e9cc2c86af4 .panel-grid .panel-grid-cell-mobile-last {
				margin-bottom: 0px
			}
		}
	</style>

	<script type="text/javascript">var tp_chameleon_url_stylesheet = '../wp-content/themes/eduma/style-1.html';
		var tp_chameleon_url_admin_ajax = '../wp-admin/admin-ajax.html';
		var tp_chameleon_wp_nonce = 'e39f6066ef';
		var tp_chameleon_primary_color = 'rgb(255, 182, 6)';
		var tp_chameleon_selector_wrapper_box = '.content-pusher';
		var tp_chameleon_class_boxed = 'boxed-area';
		var tp_chameleon_src_patterns = '../wp-content/plugins/tp-chameleon/images/patterns/index.html';
		var tp_chameleon_setting = {
			layout: tp_chameleon_getCookie('tp_chameleon_layout'),
			pattern_type: tp_chameleon_getCookie('tp_chameleon_pattern_type'),
			pattern_src: tp_chameleon_getCookie('tp_chameleon_pattern_src'),
			primary_color: tp_chameleon_getCookie('tp_chameleon_primary_color'),
			primary_color_rgb: tp_chameleon_getCookie('tp_chameleon_primary_color_rgb'),
			body_font: tp_chameleon_getCookie('tp_chameleon_body_font'),
			body_font_code: tp_chameleon_getCookie('tp_chameleon_body_font_code'),
			heading_font: tp_chameleon_getCookie('tp_chameleon_heading_font'),
			heading_font_code: tp_chameleon_getCookie('tp_chameleon_heading_font_code')
		};



		var tp_chameleon_site_url = '../index.html';

		function tp_chameleon_setCookie(cname, cvalue, exdays) {
			var d = new Date();
			d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
			var expires = "expires=" + d.toUTCString();
			document.cookie = cname + "=" + cvalue + "; " + expires;
		}

		function tp_chameleon_deleteCookie(cname) {
			var d = new Date();
			d.setTime(d.getTime() + (24 * 60 * 60 * 1000));
			var expires = "expires=" + d.toUTCString();
			document.cookie = cname + "=; " + expires;
		}

		function tp_chameleon_deleteAllCookie() {
			var all_cookie = [
				'tp_chameleon_layout',
				'tp_chameleon_pattern_type',
				'tp_chameleon_pattern_src',
				'tp_chameleon_primary_color',
				'tp_chameleon_primary_color_rgb',
				'tp_chameleon_body_font',
				'tp_chameleon_body_font_code',
				'tp_chameleon_heading_font',
				'tp_chameleon_heading_font_code'
			];

			for (var i = 0; i < all_cookie.length; i++) {
				tp_chameleon_deleteCookie(all_cookie[i]);
			}
		}

		function tp_chameleon_getCookie(cname) {
			var name = cname + "=";
			var ca = document.cookie.split(';');
			for (var i = 0; i < ca.length; i++) {
				var c = ca[i];
				while (c.charAt(0) == ' ') c = c.substring(1);
				if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
			}

			return '';
		}

		function tp_chameleon_set_first_visit() {
			tp_chameleon_setCookie('tp_chameleon_first_visit', 1, 1);
		}

		function tp_chameleon_check_first_visit() {
			return !(tp_chameleon_getCookie('tp_chameleon_first_visit') == '1');
		}

		jQuery(document).ready(function ($) {
			var $preview = $('.tp-preview-images');

			$('.tp_demo').hover(
				function (event) {
					var url_prewview = $(this).attr('data-preview');
					if (url_prewview) {
						$preview.find('img').attr('src', url_prewview);
						$preview.show();
					}
				},
				function () {
					$preview.hide();
				}
			);

			$('.tp_demo').mousemove(function (event) {
				var y = (event.clientY);
				$preview.css('top', y - 250);
			});

			function tp_chameleon_open() {
				tp_chameleon_set_first_visit();
				$('#tp_style_selector').addClass('show').animate({ right: '0px' }, 'medium');
				$('#tp_style_selector .open').hide();
				$('#tp_style_selector .close').show();
			}

			function tp_chameleon_close() {
				$('#tp_style_selector').removeClass('show').animate({ right: '-300px' }, 'medium');
				$('#tp_style_selector .close').hide();
				$('#tp_style_selector .open').show();
			}

			function tp_form_newsletter_show() {
				tp_chameleon_set_first_visit();
				$(window).scroll(function () {
					if ($(this).scrollTop() > 200) {
						$('.tp-email-form .button-email').addClass('bounceInUp animated active');
						var cookie_name = 'hide_form_email1';
						$.cookie(cookie_name, '1', { expires: 3, path: '/' });
					}
				});
			}

			function tp_change_background_pattern(url_pattern) {
				var $body = $('body');
				$body.removeClass('tp_background_image');
				$body.addClass('tp_background_pattern');
				$body.css('background-image', 'url("' + url_pattern + '")')
			}

			function tp_change_background_image(url_image) {
				var $body = $('body');
				$body.removeClass('tp_background_pattern');
				$body.addClass('tp_background_image');
				$body.css('background-image', 'url("' + url_image + '")')
			}

			function tp_chameleon_change_layout_wide() {
				tp_chameleon_setCookie('tp_chameleon_layout', 'wide', 1);

				var $body = $('body');
				$('.tp-change-layout').removeClass('active');
				$('.tp-change-layout.layout-wide').addClass('active');
				$('#tp_style_selector .boxed-mode').slideUp(300);
				$(tp_chameleon_selector_wrapper_box).removeClass(tp_chameleon_class_boxed);
				$body.css('background-image', 'none');
			}

			function tp_chameleon_change_layout_boxed() {
				tp_chameleon_setCookie('tp_chameleon_layout', 'boxed', 1);
				$('.tp-change-layout').removeClass('active');
				$('.tp-change-layout.layout-boxed').addClass('active');
				$('#tp_style_selector .boxed-mode').slideDown(300);
				$(tp_chameleon_selector_wrapper_box).addClass(tp_chameleon_class_boxed);
			}

			function tp_chameleon_change_background_pattern(pattern_src) {
				tp_chameleon_setCookie('tp_chameleon_pattern_src', pattern_src, 1);
				tp_chameleon_setCookie('tp_chameleon_pattern_type', 'pattern', 1);
				var pattern_url = tp_chameleon_src_patterns + pattern_src;
				tp_change_background_pattern(pattern_url);
			}

			function tp_chameleon_change_background_image(pattern_src) {
				tp_chameleon_setCookie('tp_chameleon_pattern_src', pattern_src, 1);
				tp_chameleon_setCookie('tp_chameleon_pattern_type', 'image', 1);
				var pattern_url = tp_chameleon_src_patterns + pattern_src;
				tp_change_background_image(pattern_url);
			}

			var $body_font = '<style id="tp_chameleon_body_font" type="text/css"></style>';
			var $heading_font = '<style id="tp_chameleon_heading_font" type="text/css"></style>';
			var $stylesheet = '<link id="tp_chameleon_stylesheet" type="text/css" rel="stylesheet">';

			var $tp_head = $('head');
			$tp_head.append($stylesheet);

			var $tp_body = $('body');
			$tp_body.append($body_font);
			$tp_body.append($heading_font);

			if (tp_chameleon_setting.layout == 'wide') {
				tp_chameleon_change_layout_wide();
			}
			if (tp_chameleon_setting.layout == 'boxed') {
				tp_chameleon_change_layout_boxed();

				if (tp_chameleon_setting.pattern_type == 'pattern' && tp_chameleon_setting.pattern_src != '') {
					tp_chameleon_change_background_pattern(tp_chameleon_setting.pattern_src);
					$('.tp_pattern[data-src="' + tp_chameleon_setting.pattern_src + '"]').addClass('active');
				}

				if (tp_chameleon_setting.pattern_type == 'image' && tp_chameleon_setting.pattern_src != '') {
					tp_chameleon_change_background_image(tp_chameleon_setting.pattern_src);
					$('.tp_image[data-src="' + tp_chameleon_setting.pattern_src + '"]').addClass('active');
				}
			}

			$('.tp-chameleon-clear').click(function (event) {
				event.preventDefault();
				tp_chameleon_deleteAllCookie();
				document.location.reload();
			});

			$('.tp-btn.tp-change-layout').click(function (event) {
				event.preventDefault();

				if ($(this).hasClass('layout-wide')) {
					tp_chameleon_change_layout_wide();

				} else {
					tp_chameleon_change_layout_boxed();

				}
			});

			$('.tp_pattern').click(function (event) {
				event.preventDefault();
				$('.tp_pattern').removeClass('active');
				$('.tp_image').removeClass('active');
				$(this).addClass('active');
				var pattern_src = $(this).attr('data-src');
				tp_chameleon_change_background_pattern(pattern_src);
			});

			$('.tp_image').click(function (event) {
				event.preventDefault();
				$('.tp_pattern').removeClass('active');
				$('.tp_image').removeClass('active');
				$(this).addClass('active');
				var pattern_src = $(this).attr('data-src');
				tp_chameleon_change_background_image(pattern_src);
			});

			/**
			 * Open/Close box
			 */
			$('#tp_style_selector .close').click(function (e) {
				e.preventDefault();
				tp_chameleon_close();
			});

			$('#tp_style_selector .open').click(function (e) {
				e.preventDefault();
				tp_chameleon_open();
			});

			$(window).load(function () {
				var $btn = $('.tp-chameleon-btn-buy');
				$btn.animate({
					bottom: $btn.attr('data-bottom')
				}, 500);

				//Set view-more-slider button
				$('.view-more-slider .text:last-child').hide();
				setInterval(function () {
					$('.view-more-slider .text:first-child').hide();
					$('.view-more-slider .text:last-child').show();
					setTimeout(function () {
						$('.view-more-slider .text:first-child').show();
						$('.view-more-slider .text:last-child').hide();
					}, 2000);
				}, 4500);

			});

			/**
			 * Check firt visit
			 */
			if (tp_chameleon_check_first_visit()) {
				//set visit is second
				setTimeout(tp_chameleon_open, 5000);
			} else {
				$('#tp_style_selector').click(function (event) {
					tp_chameleon_set_first_visit();
				});
			}
			if (tp_chameleon_check_first_visit()) {
				tp_form_newsletter_show();
			}


			$('.tp-email-form .button-email').click(function (e) {
				e.preventDefault();
				$('.tp-email-form .email-form-popup').addClass('show');
			});
			$('.tp-email-form .email-form-popup .close-popup').click(function (e) {
				e.preventDefault();
				$('.tp-email-form .email-form-popup').removeClass('show');
				$('.tp-email-form .button-email').addClass('hide');
			});

			$('.tp-email-form .email-form-popup .email-form-subscribe form ').submit(function (e) {
				e.preventDefault();
				var form = $(this);
				var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
				form.append('<div class="loading"><i class="fa fa-spinner fa-pulse"></i></div>');
				if (form.find('input[type="email"]').val()) {
					$.ajax({
						type: "POST",
						url: 'https://preview.thimpress.com/m/mailster/subscribe',
						data: form.serialize(),
						complete: function (xhr, textStatus) {
							form.find('.loading').remove();
							form.append('<div class="message-success">Please check your inbox or spam folder for confirmation email!</div>');
							setTimeout(function () {
								form.find('.message-success').remove();
							}, 2000);
						}
					});
				} else {
					form.find('.loading').remove();
					form.append('<div class="message-error">Please enter email address</div>');
					setTimeout(function () { form.find('.message-error').remove(); }, 2000);
					form.find('input[type="email"]').addClass('error');
				}


			});

			$('.tp-email-form .email-form-popup .email-form-subscribe form input[type="email"]').focus(function () {
				$(this).removeClass('error');
			});
		});</script>
    <script type="text/jscript" src="<?php echo e(asset('assets/slider/ahmed2.js')); ?>" ></script>

	<!-- <script type="text/javascript" defer src="https://3ek5k1tux0822q3g83e30fye-wpengine.netdna-ssl.com/wp-content/cache/autoptimize/1/js/autoptimize_704b916c5e3900ea21f1833f144fad05.js"></script> -->
</body>
<!-- Mirrored from educationwp.thimpress.com/gallery/ by   Website Copier/3.x [XR&CO'2014], Sun, 11 Nov 2018 12:44:26 GMT -->

</html>

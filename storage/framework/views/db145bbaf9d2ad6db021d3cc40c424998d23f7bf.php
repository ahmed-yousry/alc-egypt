<?php $__env->startSection('title'); ?>
about
<?php $__env->stopSection(); ?>
 <?php echo $__env->make('front.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


<body class="page-template-default page page-id-2901 courses eduma learnpress learnpress-page woocommerce-no-js pmpro-body-has-access siteorigin-panels siteorigin-panels-before-js group-blog thim-body-preload bg-boxed-image"
 id="thim-body">
	<div id="preload">
		<div class="sk-folding-cube">
			<div class="sk-cube1 sk-cube"></div>
			<div class="sk-cube2 sk-cube"></div>
			<div class="sk-cube4 sk-cube"></div>
			<div class="sk-cube3 sk-cube"></div>
		</div>
	</div>
	<div id="wrapper-container" class="wrapper-container">
		<div class="content-pusher">


          <?php echo $__env->make('front.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>






          <?php echo $__env->make('front.includes.nav', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
			<div id="main-content">
				<section class="content-area">
					<div class="top_heading  _out">
						<div class="top_site_main " style="color: #ffffff;background-image:url(<?php echo e(asset('assets/wp-content/themes/eduma/images/bg-page.jpg')); ?>);">
							<span class="overlay-top-header" style="background:rgba(0,0,0,0.5);"></span>
							<div class="page-title-wrapper">
								<div class="banner-wrapper container">
									<h1>About Us</h1>
								</div>
							</div>
						</div>
						<div class="breadcrumbs-wrapper">
							<div class="container">
								<ul itemprop="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList" id="breadcrumbs" class="breadcrumbs">
									<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" href="../index.html"
										 title="Home"><span itemprop="name">Home</span></a></li>
									<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name" title="About Us">
											About Us</span></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="container site-content no-padding-top">
						<div class="row">
							<main id="main" class="site-main col-sm-12 full-width">
								<article id="post-2901" class="post-2901 page type-page status-publish hentry pmpro-has-access">
									<div class="entry-content">
										<div id="pl-2901" class="panel-layout">
											<div id="pg-2901-0" class="panel-grid panel-no-style">
												<div id="pgc-2901-0-0" class="panel-grid-cell">
													<div id="panel-2901-0-0-0" class="so-panel widget widget_heading panel-first-child panel-last-child"
													 data-index="0">
														<div class="panel-widget-style panel-widget-style-for-2901-0-0-0">
															<div class="thim-widget-heading thim-widget-heading-base">
																<div class="sc_heading   text-center">
																	<h3 class="title">Our Story</h3>
																	<p class="sub-heading" style="">It is a long established fact that a reade.</p><span class="line"></span>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div id="pg-2901-1" class="panel-grid panel-no-style">
												<div id="pgc-2901-1-0" class="panel-grid-cell">
													<div id="panel-2901-1-0-0" class="so-panel widget widget_counters-box panel-first-child panel-last-child"
													 data-index="1">
														<div class="thim-widget-counters-box thim-widget-counters-box-base">
															<div class="counter-box  about-us" style="">
																<div class="content-box-percentage">
																	<div class="wrap-percentage">
																		<div class="display-percentage" data-percentage="55000">55000</div>
																		<div class="text_number"></div>
																	</div>
																	<div class="counter-content-container">
																		<div class="counter-box-content">Foreign Followers</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div id="pgc-2901-1-1" class="panel-grid-cell">
													<div id="panel-2901-1-1-0" class="so-panel widget widget_counters-box panel-first-child panel-last-child"
													 data-index="2">
														<div class="thim-widget-counters-box thim-widget-counters-box-base">
															<div class="counter-box  about-us" style="">
																<div class="content-box-percentage">
																	<div class="wrap-percentage">
																		<div class="display-percentage" data-percentage="60">60</div>
																		<div class="text_number"></div>
																	</div>
																	<div class="counter-content-container">
																		<div class="counter-box-content">Certified Teachers</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div id="pgc-2901-1-2" class="panel-grid-cell">
													<div id="panel-2901-1-2-0" class="so-panel widget widget_counters-box panel-first-child panel-last-child"
													 data-index="3">
														<div class="thim-widget-counters-box thim-widget-counters-box-base">
															<div class="counter-box  about-us" style="">
																<div class="content-box-percentage">
																	<div class="wrap-percentage">
																		<div class="display-percentage" data-percentage="2240">2240</div>
																		<div class="text_number"></div>
																	</div>
																	<div class="counter-content-container">
																		<div class="counter-box-content">Students Enrolled</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div id="pgc-2901-1-3" class="panel-grid-cell">
													<div id="panel-2901-1-3-0" class="so-panel widget widget_counters-box panel-first-child panel-last-child"
													 data-index="4">
														<div class="thim-no-border panel-widget-style panel-widget-style-for-2901-1-3-0">
															<div class="thim-widget-counters-box thim-widget-counters-box-base">
																<div class="counter-box  about-us" style="">
																	<div class="content-box-percentage">
																		<div class="wrap-percentage">
																			<div class="display-percentage" data-percentage="215">215</div>
																			<div class="text_number"></div>
																		</div>
																		<div class="counter-content-container">
																			<div class="counter-box-content">Complete Courses</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div id="pg-2901-2" class="panel-grid panel-no-style">
												<div id="pgc-2901-2-0" class="panel-grid-cell">
													<div id="panel-2901-2-0-0" class="so-panel widget widget_gallery-images panel-first-child" data-index="5">
														<div class="thim-widget-gallery-images thim-widget-gallery-images-base">
															<div class="thim-carousel-wrapper gallery-img  " data-visible="1" data-itemtablet="2" data-itemmobile="1"
															 data-autoplay="0" data-navigation="0" data-pagination="1">
																<div class="item"><img src="https://3ek5k1tux0822q3g83e30fye-wpengine.netdna-ssl.com/wp-content/uploads/2015/11/slider-3.jpg"
																	 width="1170" height="550" alt="" /></div>
																<div class="item"><img src="https://3ek5k1tux0822q3g83e30fye-wpengine.netdna-ssl.com/wp-content/uploads/2015/11/slider-2.jpg"
																	 width="1170" height="550" alt="" /></div>
																<div class="item"><img src="https://3ek5k1tux0822q3g83e30fye-wpengine.netdna-ssl.com/wp-content/uploads/2015/11/slider-1.jpg"
																	 width="1170" height="550" alt="" /></div>
															</div>
														</div>
													</div>
													<div id="panel-2901-2-0-1" class="so-panel widget widget_text panel-last-child" data-index="6">
														<div class="textwidget">
															<div class="thim-about-us-quote">
																<p>“Cras tristique turpis justo, eu consequat sem adipiscing ut. Donec posuere bibendum metus.”</p>
																<div class="agency">Tony Nguyen, Co-Founder</div>
																<hr>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div id="pg-2901-3" class="panel-grid panel-no-style">
												<div id="pgc-2901-3-0" class="panel-grid-cell">
													<div id="panel-2901-3-0-0" class="so-panel widget widget_text panel-first-child panel-last-child"
													 data-index="7">
														<h3 class="widget-title">Who We Are</h3>
														<div class="textwidget">    • The Alexandria Centre for Languages (ACL) has long been recognized as one of the top teaching centre in Egypt and the Middle East in the teaching of Arabic.
                              <br>

    • The Alexandria Centre for Languages is firmly committed to maintaining the highest standard of teaching and research in the field of Arabic language and culture for foreign students. Hence the ACL offers regular and tailored Arabic courses for universities, organizations and private learners, ranging from very Basic “Survival” Arabic courses to Intermediate and Advance level.
<br>
    • Mrs Magda Abou Youssef Program Director and Director of Studies, has been in charge of similar centres for the past 30 years and is still holding the office of Director of studies for British University students. Her extensive experience in the field of language teaching as well as her managerial skills, enable her to fully understand the ingredients for success in any demanding program offered by ACL.
    <br>
    • We insure that both our regular and tailored courses help you achieve your potentials because your learning is our priority.</div>
													</div>
												</div>
												<div id="pgc-2901-3-1" class="panel-grid-cell panel-grid-cell-empty"></div>
												<div id="pgc-2901-3-2" class="panel-grid-cell">
													<div id="panel-2901-3-2-0" class="so-panel widget widget_text panel-first-child panel-last-child"
													 data-index="8">
														<h3 class="widget-title">Services and Facilities</h3>
														<div class="textwidget"> A lot of input and support is given to students on their arrival.
														<br>
•	Hotel booking
	<br>
•	Housing and accommodation
	<br>
•	Passports
	<br>
•	Sports and leisure facilities 
	<br>
	
•	Sightseeing and Entertainment
	<br>
•	Airport arrival pick up</div>
													</div>
												</div>
											</div>
											<div id="pg-2901-4" class="panel-grid panel-has-style">
												<div class="siteorigin-panels-stretch panel-row-style panel-row-style-for-2901-4" data-siteorigin-parallax="{&quot;backgroundUrl&quot;:&quot;https:\/\/educationwp.thimpress.com\/wp-content\/uploads\/2015\/11\/bg-parallax-about-us.jpg&quot;,&quot;backgroundSize&quot;:[1600,650],&quot;backgroundSizing&quot;:&quot;scaled&quot;,&quot;limitMotion&quot;:&quot;auto&quot;}"
												 data-stretch-type="full">
													<div id="pgc-2901-4-0" class="panel-grid-cell panel-grid-cell-empty"></div>
												</div>
											</div>
											<div id="pg-2901-5" class="panel-grid panel-no-style">
												<div id="pgc-2901-5-0" class="panel-grid-cell">
													<div id="panel-2901-5-0-0" class="so-panel widget widget_heading panel-first-child panel-last-child"
													 data-index="9">
														<div class="thim-widget-heading thim-widget-heading-base">
															<div class="sc_heading   text-center">
																<h3 class="title">Meet Our Team</h3>
																<p class="sub-heading" style="">Plugins your themes with even more features.</p><span class="line"></span>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div id="pg-2901-6" class="panel-grid panel-no-style">
												<div id="pgc-2901-6-0" class="panel-grid-cell">
													<div id="panel-2901-6-0-0" class="so-panel widget widget_our-team panel-first-child panel-last-child"
													 data-index="10">
														<div class="thim-widget-our-team thim-widget-our-team-base">
															<div class="wrapper-lists-our-team "><a class="join-our-team" href="#" title="Join Our Team">Join Our
																	Team</a>
																<div class="row">
																	<div class="our-team-item col-sm-3">
																		<div class="our-team-image"> <img src="https://3ek5k1tux0822q3g83e30fye-wpengine.netdna-ssl.com/wp-content/uploads/2015/11/team-7-200x200.jpg"
																			 alt="team-7" title="team-7" width="200" height="200">
																			<div class="social-team"><a target="_blank" href="#"><i class="fa fa-facebook"></i></a><a target="_blank"
																				 href="#"><i class="fa fa-twitter"></i></a><a target="_blank" href="#"><i class="fa fa-dribbble"></i></a><a
																				 target="_blank" href="#"><i class="fa fa-linkedin"></i></a></div>
																		</div>
																		<div class="content-team">
																			<h4 class="title"><a href="../our_team/belinda/index.html">Belinda</a></h4>
																			<div class="regency">Js Developer</div>
																		</div>
																	</div>
																	<div class="our-team-item col-sm-3">
																		<div class="our-team-image"> <img src="https://3ek5k1tux0822q3g83e30fye-wpengine.netdna-ssl.com/wp-content/uploads/2015/11/team-3-200x200.jpg"
																			 alt="team-3" title="team-3" width="200" height="200">
																			<div class="social-team"><a target="_blank" href="#"><i class="fa fa-facebook"></i></a><a target="_blank"
																				 href="#"><i class="fa fa-twitter"></i></a><a target="_blank" href="#"><i class="fa fa-skype"></i></a><a
																				 target="_blank" href="#"><i class="fa fa-linkedin"></i></a></div>
																		</div>
																		<div class="content-team">
																			<h4 class="title"><a href="../our_team/christian/index.html">Christian</a></h4>
																			<div class="regency">Creative Director</div>
																		</div>
																	</div>
																	<div class="our-team-item col-sm-3">
																		<div class="our-team-image"> <img src="https://3ek5k1tux0822q3g83e30fye-wpengine.netdna-ssl.com/wp-content/uploads/2015/11/team-5-200x200.jpg"
																			 alt="team-5" title="team-5" width="200" height="200">
																			<div class="social-team"><a target="_blank" href="#"><i class="fa fa-facebook"></i></a><a target="_blank"
																				 href="#"><i class="fa fa-twitter"></i></a><a target="_blank" href="#"><i class="fa fa-skype"></i></a><a
																				 target="_blank" href="#"><i class="fa fa-linkedin"></i></a></div>
																		</div>
																		<div class="content-team">
																			<h4 class="title"><a href="../our_team/robert/index.html">Robert</a></h4>
																			<div class="regency">Office Manager</div>
																		</div>
																	</div>
																	<div class="our-team-item col-sm-3">
																		<div class="our-team-image"> <img src="https://3ek5k1tux0822q3g83e30fye-wpengine.netdna-ssl.com/wp-content/uploads/2015/11/team-6-200x200.jpg"
																			 alt="team-6" title="team-6" width="200" height="200">
																			<div class="social-team"><a target="_blank" href="#"><i class="fa fa-facebook"></i></a><a target="_blank"
																				 href="#"><i class="fa fa-twitter"></i></a><a target="_blank" href="#"><i class="fa fa-skype"></i></a><a
																				 target="_blank" href="#"><i class="fa fa-linkedin"></i></a></div>
																		</div>
																		<div class="content-team">
																			<h4 class="title"><a href="../our_team/tony-teo/index.html">Tony Teo</a></h4>
																			<div class="regency">Support Manager</div>
																		</div>
																	</div>
																	<div class="our-team-item col-sm-3">
																		<div class="our-team-image"> <img src="https://3ek5k1tux0822q3g83e30fye-wpengine.netdna-ssl.com/wp-content/uploads/2015/11/team-2-200x200.jpg"
																			 alt="team-2" title="team-2" width="200" height="200">
																			<div class="social-team"><a target="_blank" href="#"><i class="fa fa-facebook"></i></a><a target="_blank"
																				 href="#"><i class="fa fa-twitter"></i></a><a target="_blank" href="#"><i class="fa fa-skype"></i></a><a
																				 target="_blank" href="#"><i class="fa fa-linkedin"></i></a></div>
																		</div>
																		<div class="content-team">
																			<h4 class="title"><a href="../our_team/jonathan/index.html">Jonathan</a></h4>
																			<div class="regency">Art Director</div>
																		</div>
																	</div>
																	<div class="our-team-item col-sm-3">
																		<div class="our-team-image"> <img src="https://3ek5k1tux0822q3g83e30fye-wpengine.netdna-ssl.com/wp-content/uploads/2015/11/team-1-200x200.jpg"
																			 alt="team-1" title="team-1" width="200" height="200">
																			<div class="social-team"><a target="_blank" href="#"><i class="fa fa-facebook"></i></a><a target="_blank"
																				 href="#"><i class="fa fa-twitter"></i></a><a target="_blank" href="#"><i class="fa fa-skype"></i></a><a
																				 target="_blank" href="#"><i class="fa fa-linkedin"></i></a></div>
																		</div>
																		<div class="content-team">
																			<h4 class="title"><a href="../our_team/john-doe/index.html">John Doe</a></h4>
																			<div class="regency">Web Developer</div>
																		</div>
																	</div>
																	<div class="our-team-item col-sm-3">
																		<div class="our-team-image"> <img src="https://3ek5k1tux0822q3g83e30fye-wpengine.netdna-ssl.com/wp-content/uploads/2015/11/team-8-200x200.jpg"
																			 alt="team-8" title="team-8" width="200" height="200">
																			<div class="social-team"><a target="_blank" href="#"><i class="fa fa-facebook"></i></a><a target="_blank"
																				 href="#"><i class="fa fa-twitter"></i></a><a target="_blank" href="#"><i class="fa fa-skype"></i></a><a
																				 target="_blank" href="#"><i class="fa fa-linkedin"></i></a></div>
																		</div>
																		<div class="content-team">
																			<h4 class="title"><a href="../our_team/jane-nguyen/index.html">Jane Nguyen</a></h4>
																			<div class="regency">Copyrighter</div>
																		</div>
																	</div>
																	<div class="our-team-item col-sm-3">
																		<div class="our-team-image"> <img src="https://3ek5k1tux0822q3g83e30fye-wpengine.netdna-ssl.com/wp-content/uploads/2015/11/team-4-200x200.jpg"
																			 alt="team-4" title="team-4" width="200" height="200">
																			<div class="social-team"><a target="_blank" href="#"><i class="fa fa-facebook"></i></a><a target="_blank"
																				 href="#"><i class="fa fa-twitter"></i></a><a target="_blank" href="#"><i class="fa fa-skype"></i></a><a
																				 target="_blank" href="#"><i class="fa fa-linkedin"></i></a></div>
																		</div>
																		<div class="content-team">
																			<h4 class="title"><a href="../our_team/ryze-faker/index.html">Ryze Faker</a></h4>
																			<div class="regency">Co-Founder</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</article>
							</main>
						</div>
					</div>
				</section>









       <?php echo $__env->make('front.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>









			</div>
		</div> <a href="#" id="back-to-top"> <i class="fa fa-chevron-up" aria-hidden="true"></i> </a>
	</div>

	<div class="tp-preview-images"> <img src="https://updates.thimpress.com/wp-content/uploads/2017/06/eduma-demo-01.jpg"
		 alt="preview image"></div>
	<div id="tp_chameleon_list_google_fonts"></div>
	<div class="gallery-slider-content"></div>
	<script data-cfasync="false" src="../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
	<script data-cfasync="false" type="text/javascript">
		window.onload = function () {
			var thim_preload = document.getElementById("preload");
			if (thim_preload) {
				setTimeout(function () {
					var body = document.getElementById("thim-body"),
						len = body.childNodes.length,
						class_name = body.className.replace(/(?:^|\s)thim-body-preload(?!\S)/, '').replace(
							/(?:^|\s)thim-body-load-overlay(?!\S)/, '');

					body.className = class_name;
					if (typeof thim_preload !== 'undefined' && thim_preload !== null) {
						for (var i = 0; i < len; i++) {
							if (body.childNodes[i].id !== 'undefined' && body.childNodes[i].id == "preload") {
								body.removeChild(body.childNodes[i]);
								break;
							}
						}
					}
				}, 500);
			}
		};
	</script>
	<script>
		window.addEventListener('load', function () {
			/**
			 * Fix issue there is an empty spacing between image and title of owl-carousel
			 */
			setTimeout(function () {
				var $ = jQuery;
				var $carousel = $('.thim-owl-carousel-post').each(function () {
					$(this).find('.image').css('min-height', 0);
					$(window).trigger('resize');
				});
			}, 500);
		})
	</script>
	<script data-cfasync="true" type="text/javascript">
		(function ($) {
			"use strict";
			$(document).on('click',
				'body:not(".logged-in") .enroll-course .button-enroll-course, body:not(".logged-in") .purchase-course:not(".guest_checkout, .learn-press-pmpro-buy-membership") .button',
				function (e) {
					e.preventDefault();
					$(this).parent().find('[name="redirect_to"]').val(
						'../account/index5e4b.html?redirect_to=localhost/about-us/?enroll-course=2901');
					var redirect = $(this).parent().find('[name="redirect_to"]').val();
					window.location = redirect;
				});
		})(jQuery);
	</script>
	<style type="text/css" media="all" id="siteorigin-panels-layouts-footer">
		/* Layout tc-megamenu-7682 */
		#pgc-tc-megamenu-7682-0-0,
		#pgc-tc-megamenu-7682-0-1 {
			width: 31%;
			width: calc(31% - (0.69 * 30px))
		}

		#pl-tc-megamenu-7682 #panel-tc-megamenu-7682-0-0-0,
		#pl-tc-megamenu-7682 #panel-tc-megamenu-7682-0-1-0,
		#pl-tc-megamenu-7682 #panel-tc-megamenu-7682-0-2-0 {}

		#pgc-tc-megamenu-7682-0-2 {
			width: 38%;
			width: calc(38% - (0.62 * 30px))
		}

		#pl-tc-megamenu-7682 .so-panel {
			margin-bottom: 30px
		}

		#pl-tc-megamenu-7682 .so-panel:last-child {
			margin-bottom: 0px
		}

		@media (max-width:767px) {

			#pg-tc-megamenu-7682-0.panel-no-style,
			#pg-tc-megamenu-7682-0.panel-has-style>.panel-row-style {
				-webkit-flex-direction: column;
				-ms-flex-direction: column;
				flex-direction: column
			}

			#pg-tc-megamenu-7682-0 .panel-grid-cell {
				margin-right: 0
			}

			#pg-tc-megamenu-7682-0 .panel-grid-cell {
				width: 100%
			}

			#pgc-tc-megamenu-7682-0-0,
			#pgc-tc-megamenu-7682-0-1 {
				margin-bottom: 30px
			}

			#pl-tc-megamenu-7682 .panel-grid-cell {
				padding: 0
			}

			#pl-tc-megamenu-7682 .panel-grid .panel-grid-cell-empty {
				display: none
			}

			#pl-tc-megamenu-7682 .panel-grid .panel-grid-cell-mobile-last {
				margin-bottom: 0px
			}
		}

		/* Layout w57e9cc2c86af4 */
		#pgc-w57e9cc2c86af4-0-0 {
			width: 33.3%;
			width: calc(33.3% - (0.667 * 30px))
		}

		#pl-w57e9cc2c86af4 #panel-w57e9cc2c86af4-0-0-0,
		#pl-w57e9cc2c86af4 #panel-w57e9cc2c86af4-0-0-1,
		#pl-w57e9cc2c86af4 #panel-w57e9cc2c86af4-0-1-0,
		#pl-w57e9cc2c86af4 #panel-w57e9cc2c86af4-0-2-0,
		#pl-w57e9cc2c86af4 #panel-w57e9cc2c86af4-0-3-0,
		#pl-w57e9cc2c86af4 #panel-w57e9cc2c86af4-0-4-0 {}

		#pgc-w57e9cc2c86af4-0-1,
		#pgc-w57e9cc2c86af4-0-2,
		#pgc-w57e9cc2c86af4-0-3,
		#pgc-w57e9cc2c86af4-0-4 {
			width: 16.675%;
			width: calc(16.675% - (0.83325 * 30px))
		}

		#pl-w57e9cc2c86af4 .so-panel {
			margin-bottom: 30px
		}

		#pl-w57e9cc2c86af4 .so-panel:last-child {
			margin-bottom: 0px
		}

		@media (max-width:767px) {

			#pg-w57e9cc2c86af4-0.panel-no-style,
			#pg-w57e9cc2c86af4-0.panel-has-style>.panel-row-style {
				-webkit-flex-direction: column;
				-ms-flex-direction: column;
				flex-direction: column
			}

			#pg-w57e9cc2c86af4-0 .panel-grid-cell {
				margin-right: 0
			}

			#pg-w57e9cc2c86af4-0 .panel-grid-cell {
				width: 100%
			}

			#pgc-w57e9cc2c86af4-0-0,
			#pgc-w57e9cc2c86af4-0-1,
			#pgc-w57e9cc2c86af4-0-2,
			#pgc-w57e9cc2c86af4-0-3 {
				margin-bottom: 30px
			}

			#pl-w57e9cc2c86af4 .panel-grid-cell {
				padding: 0
			}

			#pl-w57e9cc2c86af4 .panel-grid .panel-grid-cell-empty {
				display: none
			}

			#pl-w57e9cc2c86af4 .panel-grid .panel-grid-cell-mobile-last {
				margin-bottom: 0px
			}
		}
	</style>
	<script type="text/javascript">
		var c = document.body.className;
		c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
		document.body.className = c;
	</script>
	<script type='text/javascript'>
		var wpcf7 = {
			"apiSettings": {
				"root": "https:\/\/educationwp.thimpress.com\/wp-json\/contact-form-7\/v1",
				"namespace": "contact-form-7\/v1"
			},
			"recaptcha": {
				"messages": {
					"empty": "Please verify that you are not a robot."
				}
			},
			"cached": 0
		};
	</script>
	<script type='text/javascript'>
		var wc_add_to_cart_params = {
			"ajax_url": "\/wp-admin\/admin-ajax.php",
			"wc_ajax_url": "\/?wc-ajax=%%endpoint%%",
			"i18n_view_cart": "View cart",
			"cart_url": "https:\/\/educationwp.thimpress.com\/cart\/",
			"is_cart": "",
			"cart_redirect_after_add": "no"
		};
	</script>
	<script type='text/javascript'>
		var woocommerce_params = {
			"ajax_url": "\/wp-admin\/admin-ajax.php",
			"wc_ajax_url": "\/?wc-ajax=%%endpoint%%"
		};
	</script>
	<script type='text/javascript'>
		var wc_cart_fragments_params = {
			"ajax_url": "\/wp-admin\/admin-ajax.php",
			"wc_ajax_url": "\/?wc-ajax=%%endpoint%%",
			"cart_hash_key": "wc_cart_hash_8653f1d19ec105315cba116db9d7fe2e",
			"fragment_name": "wc_fragments_8653f1d19ec105315cba116db9d7fe2e"
		};
	</script>
	<script type='text/javascript'>
		var _wpUtilSettings = {
			"ajax": {
				"url": "\/wp-admin\/admin-ajax.php"
			}
		};
	</script>
	<script type='text/javascript'>
		var WPEMS = {
			"gmt_offset": "0",
			"current_time": "Nov 11, 2018 12:38:00 +0000",
			"l18n": {
				"labels": ["Years", "Months", "Weeks", "Days", "Hours", "Minutes", "Seconds"],
				"labels1": ["Year", "Month", "Week", "Day", "Hour", "Minute", "Second"]
			},
			"ajaxurl": "https:\/\/educationwp.thimpress.com\/wp-admin\/admin-ajax.php",
			"something_wrong": "Something went wrong",
			"register_button": "c20eb61364"
		};
	</script>
	<script type='text/javascript'>
		var thim_js_translate = {
			"login": "Username",
			"password": "Password",
			"close": "Close"
		};
	</script>
	<script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js?ver=3.0.22'></script>
	<script type='text/javascript'>
		WebFont.load({
			google: {
				families: ['Roboto Slab:300,400,700', 'Roboto:300,300i,400,400i,500,500i,700,700i']
			}
		});
	</script>
	<script type='text/javascript'>
		var lpGlobalSettings = {
			"url": "https:\/\/educationwp.thimpress.com\/about-us\/",
			"siteurl": "https:\/\/educationwp.thimpress.com",
			"ajax": "https:\/\/educationwp.thimpress.com\/wp-admin\/admin-ajax.php",
			"theme": "eduma",
			"localize": {
				"button_ok": "OK",
				"button_cancel": "Cancel",
				"button_yes": "Yes",
				"button_no": "No"
			}
		};
	</script>
	<script type='text/javascript'>
		var lpCheckoutSettings = {
			"ajaxurl": "https:\/\/educationwp.thimpress.com",
			"user_waiting_payment": null,
			"user_checkout": "",
			"i18n_processing": "Processing",
			"i18n_redirecting": "Redirecting",
			"i18n_invalid_field": "Invalid field",
			"i18n_unknown_error": "Unknown error",
			"i18n_place_order": "Place order"
		};
	</script>
	<script type='text/javascript'>
		var lpProfileUserSettings = {
			"processing": "Processing",
			"redirecting": "Redirecting",
			"avatar_size": {
				"width": 150,
				"height": 150
			}
		};
	</script>
	<script type='text/javascript'>
		var lpCourseSettings = [""];
	</script>
	<script type='text/javascript'>
		var lpQuizSettings = [];
	</script>
	<script type="text/javascript">
		document.body.className = document.body.className.replace("siteorigin-panels-before-js", "");
	</script>
	<div class="tp_chameleon_overlay">
		<div class="tp_chameleon_progress">
			<div class="tp_chameleon_heading">Processing!</div>
		</div>
	</div>
	<script type="text/javascript">
		var tp_chameleon_url_stylesheet = '../wp-content/themes/eduma/style-1.html';
		var tp_chameleon_url_admin_ajax = '../wp-admin/admin-ajax.html';
		var tp_chameleon_wp_nonce = 'e39f6066ef';
		var tp_chameleon_primary_color = 'rgb(255, 182, 6)';
		var tp_chameleon_selector_wrapper_box = '.content-pusher';
		var tp_chameleon_class_boxed = 'boxed-area';

		var tp_chameleon_src_patterns = "<?php echo e(asset('assets/wp-content/plugins/tp-chameleon/images/patterns/index.html')); ?>";
		var tp_chameleon_setting = {
			layout: tp_chameleon_getCookie('tp_chameleon_layout'),
			pattern_type: tp_chameleon_getCookie('tp_chameleon_pattern_type'),
			pattern_src: tp_chameleon_getCookie('tp_chameleon_pattern_src'),
			primary_color: tp_chameleon_getCookie('tp_chameleon_primary_color'),
			primary_color_rgb: tp_chameleon_getCookie('tp_chameleon_primary_color_rgb'),
			body_font: tp_chameleon_getCookie('tp_chameleon_body_font'),
			body_font_code: tp_chameleon_getCookie('tp_chameleon_body_font_code'),
			heading_font: tp_chameleon_getCookie('tp_chameleon_heading_font'),
			heading_font_code: tp_chameleon_getCookie('tp_chameleon_heading_font_code')
		};



		var tp_chameleon_site_url = '../index.html';

		function tp_chameleon_setCookie(cname, cvalue, exdays) {
			var d = new Date();
			d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
			var expires = "expires=" + d.toUTCString();
			document.cookie = cname + "=" + cvalue + "; " + expires;
		}

		function tp_chameleon_deleteCookie(cname) {
			var d = new Date();
			d.setTime(d.getTime() + (24 * 60 * 60 * 1000));
			var expires = "expires=" + d.toUTCString();
			document.cookie = cname + "=; " + expires;
		}

		function tp_chameleon_deleteAllCookie() {
			var all_cookie = [
				'tp_chameleon_layout',
				'tp_chameleon_pattern_type',
				'tp_chameleon_pattern_src',
				'tp_chameleon_primary_color',
				'tp_chameleon_primary_color_rgb',
				'tp_chameleon_body_font',
				'tp_chameleon_body_font_code',
				'tp_chameleon_heading_font',
				'tp_chameleon_heading_font_code'
			];

			for (var i = 0; i < all_cookie.length; i++) {
				tp_chameleon_deleteCookie(all_cookie[i]);
			}
		}

		function tp_chameleon_getCookie(cname) {
			var name = cname + "=";
			var ca = document.cookie.split(';');
			for (var i = 0; i < ca.length; i++) {
				var c = ca[i];
				while (c.charAt(0) == ' ') c = c.substring(1);
				if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
			}

			return '';
		}

		function tp_chameleon_set_first_visit() {
			tp_chameleon_setCookie('tp_chameleon_first_visit', 1, 1);
		}

		function tp_chameleon_check_first_visit() {
			return !(tp_chameleon_getCookie('tp_chameleon_first_visit') == '1');
		}

		jQuery(document).ready(function ($) {
			var $preview = $('.tp-preview-images');

			$('.tp_demo').hover(
				function (event) {
					var url_prewview = $(this).attr('data-preview');
					if (url_prewview) {
						$preview.find('img').attr('src', url_prewview);
						$preview.show();
					}
				},
				function () {
					$preview.hide();
				}
			);

			$('.tp_demo').mousemove(function (event) {
				var y = (event.clientY);
				$preview.css('top', y - 250);
			});

			function tp_chameleon_open() {
				tp_chameleon_set_first_visit();
				$('#tp_style_selector').addClass('show').animate({
					right: '0px'
				}, 'medium');
				$('#tp_style_selector .open').hide();
				$('#tp_style_selector .close').show();
			}

			function tp_chameleon_close() {
				$('#tp_style_selector').removeClass('show').animate({
					right: '-300px'
				}, 'medium');
				$('#tp_style_selector .close').hide();
				$('#tp_style_selector .open').show();
			}

			function tp_form_newsletter_show() {
				tp_chameleon_set_first_visit();
				$(window).scroll(function () {
					if ($(this).scrollTop() > 200) {
						$('.tp-email-form .button-email').addClass('bounceInUp animated active');
						var cookie_name = 'hide_form_email1';
						$.cookie(cookie_name, '1', {
							expires: 3,
							path: '/'
						});
					}
				});
			}

			function tp_change_background_pattern(url_pattern) {
				var $body = $('body');
				$body.removeClass('tp_background_image');
				$body.addClass('tp_background_pattern');
				$body.css('background-image', 'url("' + url_pattern + '")')
			}

			function tp_change_background_image(url_image) {
				var $body = $('body');
				$body.removeClass('tp_background_pattern');
				$body.addClass('tp_background_image');
				$body.css('background-image', 'url("' + url_image + '")')
			}

			function tp_chameleon_change_layout_wide() {
				tp_chameleon_setCookie('tp_chameleon_layout', 'wide', 1);

				var $body = $('body');
				$('.tp-change-layout').removeClass('active');
				$('.tp-change-layout.layout-wide').addClass('active');
				$('#tp_style_selector .boxed-mode').slideUp(300);
				$(tp_chameleon_selector_wrapper_box).removeClass(tp_chameleon_class_boxed);
				$body.css('background-image', 'none');
			}

			function tp_chameleon_change_layout_boxed() {
				tp_chameleon_setCookie('tp_chameleon_layout', 'boxed', 1);
				$('.tp-change-layout').removeClass('active');
				$('.tp-change-layout.layout-boxed').addClass('active');
				$('#tp_style_selector .boxed-mode').slideDown(300);
				$(tp_chameleon_selector_wrapper_box).addClass(tp_chameleon_class_boxed);
			}

			function tp_chameleon_change_background_pattern(pattern_src) {
				tp_chameleon_setCookie('tp_chameleon_pattern_src', pattern_src, 1);
				tp_chameleon_setCookie('tp_chameleon_pattern_type', 'pattern', 1);
				var pattern_url = tp_chameleon_src_patterns + pattern_src;
				tp_change_background_pattern(pattern_url);
			}

			function tp_chameleon_change_background_image(pattern_src) {
				tp_chameleon_setCookie('tp_chameleon_pattern_src', pattern_src, 1);
				tp_chameleon_setCookie('tp_chameleon_pattern_type', 'image', 1);
				var pattern_url = tp_chameleon_src_patterns + pattern_src;
				tp_change_background_image(pattern_url);
			}

			var $body_font = '<style id="tp_chameleon_body_font" type="text/css"></style>';
			var $heading_font = '<style id="tp_chameleon_heading_font" type="text/css"></style>';
			var $stylesheet = '<link id="tp_chameleon_stylesheet" type="text/css" rel="stylesheet">';

			var $tp_head = $('head');
			$tp_head.append($stylesheet);

			var $tp_body = $('body');
			$tp_body.append($body_font);
			$tp_body.append($heading_font);

			if (tp_chameleon_setting.layout == 'wide') {
				tp_chameleon_change_layout_wide();
			}
			if (tp_chameleon_setting.layout == 'boxed') {
				tp_chameleon_change_layout_boxed();

				if (tp_chameleon_setting.pattern_type == 'pattern' && tp_chameleon_setting.pattern_src != '') {
					tp_chameleon_change_background_pattern(tp_chameleon_setting.pattern_src);
					$('.tp_pattern[data-src="' + tp_chameleon_setting.pattern_src + '"]').addClass('active');
				}

				if (tp_chameleon_setting.pattern_type == 'image' && tp_chameleon_setting.pattern_src != '') {
					tp_chameleon_change_background_image(tp_chameleon_setting.pattern_src);
					$('.tp_image[data-src="' + tp_chameleon_setting.pattern_src + '"]').addClass('active');
				}
			}

			$('.tp-chameleon-clear').click(function (event) {
				event.preventDefault();
				tp_chameleon_deleteAllCookie();
				document.location.reload();
			});

			$('.tp-btn.tp-change-layout').click(function (event) {
				event.preventDefault();

				if ($(this).hasClass('layout-wide')) {
					tp_chameleon_change_layout_wide();

				} else {
					tp_chameleon_change_layout_boxed();

				}
			});

			$('.tp_pattern').click(function (event) {
				event.preventDefault();
				$('.tp_pattern').removeClass('active');
				$('.tp_image').removeClass('active');
				$(this).addClass('active');
				var pattern_src = $(this).attr('data-src');
				tp_chameleon_change_background_pattern(pattern_src);
			});

			$('.tp_image').click(function (event) {
				event.preventDefault();
				$('.tp_pattern').removeClass('active');
				$('.tp_image').removeClass('active');
				$(this).addClass('active');
				var pattern_src = $(this).attr('data-src');
				tp_chameleon_change_background_image(pattern_src);
			});

			/**
			 * Open/Close box
			 */
			$('#tp_style_selector .close').click(function (e) {
				e.preventDefault();
				tp_chameleon_close();
			});

			$('#tp_style_selector .open').click(function (e) {
				e.preventDefault();
				tp_chameleon_open();
			});

			$(window).load(function () {
				var $btn = $('.tp-chameleon-btn-buy');
				$btn.animate({
					bottom: $btn.attr('data-bottom')
				}, 500);

				//Set view-more-slider button
				$('.view-more-slider .text:last-child').hide();
				setInterval(function () {
					$('.view-more-slider .text:first-child').hide();
					$('.view-more-slider .text:last-child').show();
					setTimeout(function () {
						$('.view-more-slider .text:first-child').show();
						$('.view-more-slider .text:last-child').hide();
					}, 2000);
				}, 4500);

			});

			/**
			 * Check firt visit
			 */
			if (tp_chameleon_check_first_visit()) {
				//set visit is second
				setTimeout(tp_chameleon_open, 5000);
			} else {
				$('#tp_style_selector').click(function (event) {
					tp_chameleon_set_first_visit();
				});
			}
			if (tp_chameleon_check_first_visit()) {
				tp_form_newsletter_show();
			}


			$('.tp-email-form .button-email').click(function (e) {
				e.preventDefault();
				$('.tp-email-form .email-form-popup').addClass('show');
			});
			$('.tp-email-form .email-form-popup .close-popup').click(function (e) {
				e.preventDefault();
				$('.tp-email-form .email-form-popup').removeClass('show');
				$('.tp-email-form .button-email').addClass('hide');
			});

			$('.tp-email-form .email-form-popup .email-form-subscribe form ').submit(function (e) {
				e.preventDefault();
				var form = $(this);
				var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
				form.append('<div class="loading"><i class="fa fa-spinner fa-pulse"></i></div>');
				if (form.find('input[type="email"]').val()) {
					$.ajax({
						type: "POST",
						url: 'https://preview.thimpress.com/m/mailster/subscribe',
						data: form.serialize(),
						complete: function (xhr, textStatus) {
							form.find('.loading').remove();
							form.append(
								'<div class="message-success">Please check your inbox or spam folder for confirmation email!</div>');
							setTimeout(function () {
								form.find('.message-success').remove();
							}, 2000);
						}
					});
				} else {
					form.find('.loading').remove();
					form.append('<div class="message-error">Please enter email address</div>');
					setTimeout(function () {
						form.find('.message-error').remove();
					}, 2000);
					form.find('input[type="email"]').addClass('error');
				}


			});

			$('.tp-email-form .email-form-popup .email-form-subscribe form input[type="email"]').focus(function () {
				$(this).removeClass('error');
			});
		});
	</script>
  <script type="text/jscript" src="<?php echo e(asset('assets/slider/ahmed2.js')); ?>" ></script>

	<!-- <script type="text/javascript" defer src="https://3ek5k1tux0822q3g83e30fye-wpengine.netdna-ssl.com/wp-content/cache/autoptimize/1/js/autoptimize_1548f79fc76da4dc9fd6eccf84405180.js"></script> -->
</body>

</html>

<?php $__env->startSection('title'); ?>
    Edit activities
<?php $__env->stopSection(); ?>
<?php $__env->startSection('page-header'); ?>
    <section class="content-header">
        <h1>
            edit activities
            <small></small>
        </h1>

    </section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">activities Form</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->

                    <form class="form-horizontal" method="post" action="<?php echo e(url('/admin/event/'.$events->id)); ?>" enctype="multipart/form-data">
                        <?php echo e(csrf_field()); ?>

                        <input type="hidden" name="_method" value="patch">

                        <div class="box-body">

                          <div class="form-group">

                              <label for="title" class="col-sm-1 control-label">title</label>
                              <div class="col-sm-5 <?php echo e($errors->has('title') ? ' has-error' : ''); ?>">
                                  <input type="text" name="title" class="form-control" id="title" placeholder="title" value="<?php echo e($events->title); ?>" required autofocus>
                                  <?php if($errors->has('title')): ?>
                                      <span class="help-block">
                                          <strong><?php echo e($errors->first('title')); ?></strong>
                                      </span>
                                  <?php endif; ?>
                              </div>
                                </div>


                            <div class="form-group">

                                <label for="title" class="col-sm-1 control-label">start time</label>
                                <div class="col-sm-5 <?php echo e($errors->has('title') ? ' has-error' : ''); ?>">
                                    <input type="text" name="start_time" class="form-control" id="title" placeholder="start time" value="<?php echo e($events->start_time); ?>" required autofocus>
                                    <?php if($errors->has('start_time')): ?>
                                        <span class="help-block">
                                            <strong><?php echo e($errors->first('start_time')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>
                                  </div>

                                  <div class="form-group">
                                      <label for="title" class="col-sm-1 control-label">finish time</label>
                                      <div class="col-sm-5 <?php echo e($errors->has('title') ? ' has-error' : ''); ?>">
                                          <input type="text" name="finish_time" class="form-control" id="title" placeholder="finish time" value="<?php echo e($events->finish_time); ?>" required autofocus>
                                          <?php if($errors->has('finish_time')): ?>
                                              <span class="help-block">
                                                  <strong><?php echo e($errors->first('finish_time')); ?></strong>
                                              </span>
                                          <?php endif; ?>
                                      </div>
                                        </div>


                                        <div class="form-group">
                                            <label for="title" class="col-sm-1 control-label">address</label>
                                            <div class="col-sm-5 <?php echo e($errors->has('title') ? ' has-error' : ''); ?>">
                                                <input type="text" name="address" class="form-control" id="address" placeholder="address" value="<?php echo e($events->address); ?>" required autofocus>
                                                <?php if($errors->has('address')): ?>
                                                    <span class="help-block">
                                                        <strong><?php echo e($errors->first('address')); ?></strong>
                                                    </span>
                                                <?php endif; ?>
                                            </div>
                                              </div>



                                <div class="form-group">
                                <label for="photo" class="col-sm-1 control-label">Photo</label>
                                <div class="col-sm-5 <?php echo e($errors->has('photo') ? ' has-error' : ''); ?>">
                                    <input type="file" name="photo" id="photo" class="form-control">
                                    <?php if($errors->has('photo')): ?>
                                        <span class="help-block">
                                            <strong><?php echo e($errors->first('photo')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>
                                </div>

                            <div class="form-group">

                                <label for="description" class="col-sm-1 control-label">Description</label>
                                <div class="col-sm-11 <?php echo e($errors->has('description') ? ' has-error' : ''); ?>">
                                    <div class="box-body pad">
                                        <textarea name="description" class="form-control" placeholder="Description"  >  <?php echo e($events->dscription); ?></textarea>
                                        <?php if($errors->has('description')): ?>
                                            <span class="help-block">
                                                    <strong><?php echo e($errors->first('description')); ?></strong>
                                                </span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>



                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-info center-block">Save <i class="fa fa-save" style="margin-left: 5px"></i></button>
                        </div>
                        <!-- /.box-footer -->

                    </form>

                </div>
                <!-- /.box -->
                <!-- general form elements disabled -->

                <!-- /.box -->
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script src="<?php echo e(asset('assets/bower_components/ckeditor/ckeditor.js')); ?>"></script>
    <script>
        $(function () {
            // Replace the <textarea id="editor1"> with a CKEditor
            // instance, using default configuration.
            CKEDITOR.replace('editor1')
            //bootstrap WYSIHTML5 - text editor
            $('.textarea').wysihtml5()
        })
    </script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
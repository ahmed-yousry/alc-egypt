<?php $__env->startSection('title'); ?>
GALLERY
<?php $__env->stopSection(); ?>
 <?php echo $__env->make('front.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

	<script type="text/javascript">function tc_insert_internal_css(css) {
			var tc_style = document.createElement("style");
			tc_style.type = "text/css";
			tc_style.setAttribute('data-type', 'tc-internal-css');
			var tc_style_content = document.createTextNode(css);
			tc_style.appendChild(tc_style_content);
			document.head.appendChild(tc_style);
		}</script>
	<meta name="generator" content="Powered by Slider Revolution 5.4.8 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
	<style type="text/css" media="all" id="siteorigin-panels-layouts-head">
		/* Layout 4526 */
		#pgc-4526-0-0 {
			width: 100%;
			width: calc(100% - (0 * 30px))
		}

		#pl-4526 #panel-4526-0-0-0 {}

		#pg-4526-0 {
			margin-bottom: -30px
		}

		#pl-4526 .so-panel {
			margin-bottom: 30px
		}

		#pl-4526 .so-panel:last-child {
			margin-bottom: 0px
		}

		@media (max-width:767px) {

			#pg-4526-0.panel-no-style,
			#pg-4526-0.panel-has-style>.panel-row-style {
				-webkit-flex-direction: column;
				-ms-flex-direction: column;
				flex-direction: column
			}

			#pg-4526-0 .panel-grid-cell {
				margin-right: 0
			}

			#pg-4526-0 .panel-grid-cell {
				width: 100%
			}

			#pl-4526 .panel-grid-cell {
				padding: 0
			}

			#pl-4526 .panel-grid .panel-grid-cell-empty {
				display: none
			}

			#pl-4526 .panel-grid .panel-grid-cell-mobile-last {
				margin-bottom: 0px
			}
		}
	</style>
	<link rel="icon" href="https://3ek5k1tux0822q3g83e30fye-wpengine.netdna-ssl.com/wp-content/uploads/2015/10/favicon.png"
	 sizes="32x32" />
	<link rel="icon" href="https://3ek5k1tux0822q3g83e30fye-wpengine.netdna-ssl.com/wp-content/uploads/2015/10/favicon.png"
	 sizes="192x192" />
	<link rel="apple-touch-icon-precomposed" href="https://3ek5k1tux0822q3g83e30fye-wpengine.netdna-ssl.com/wp-content/uploads/2015/10/favicon.png" />
	<meta name="msapplication-TileImage" content="https://educationwp.thimpress.com/wp-content/uploads/2015/10/favicon.png" />
	<script type="text/javascript">function setREVStartSize(e) {
			try {
			e.c = jQuery(e.c); var i = jQuery(window).width(), t = 9999, r = 0, n = 0, l = 0, f = 0, s = 0, h = 0;
				if (e.responsiveLevels && (jQuery.each(e.responsiveLevels, function (e, f) { f > i && (t = r = f, l = e), i > f && f > r && (r = f, n = e) }), t > r && (l = n)), f = e.gridheight[l] || e.gridheight[0] || e.gridheight, s = e.gridwidth[l] || e.gridwidth[0] || e.gridwidth, h = i / s, h = h > 1 ? 1 : h, f = Math.round(h * f), "fullscreen" == e.sliderLayout) { var u = (e.c.width(), jQuery(window).height()); if (void 0 != e.fullScreenOffsetContainer) { var c = e.fullScreenOffsetContainer.split(","); if (c) jQuery.each(c, function (e, i) { u = jQuery(i).length > 0 ? u - jQuery(i).outerHeight(!0) : u }), e.fullScreenOffset.split("%").length > 1 && void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 ? u -= jQuery(window).height() * parseInt(e.fullScreenOffset, 0) / 100 : void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 && (u -= parseInt(e.fullScreenOffset, 0)) } f = u } else void 0 != e.minHeight && f < e.minHeight && (f = e.minHeight); e.c.closest(".rev_slider_wrapper").css({ height: f })
			} catch (d) { console.log("Failure at Presize of Slider:" + d) }
		};</script>
	<script type="text/javascript">if (typeof ajaxurl === 'undefined') {

			var ajaxurl = "../wp-admin/admin-ajax.html";

		}</script>
	<script>!function (f, b, e, v, n, t, s) {
			if (f.fbq) return;
			n = f.fbq = function () {
				n.callMethod ?
					n.callMethod.apply(n, arguments) : n.queue.push(arguments);
			};
			if (!f._fbq) f._fbq = n;
			n.push = n;
			n.loaded = !0;
			n.version = '2.0';
			n.queue = [];
			t = b.createElement(e);
			t.async = !0;
			t.src = v;
			s = b.getElementsByTagName(e)[0];
			s.parentNode.insertBefore(t, s);
		}(window, document, 'script',
			'https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '978118969000044');
		fbq('track', 'PageView');</script> <noscript> <img height="1" width="1" src="https://www.facebook.com/tr?id=978118969000044&amp;ev=PageView&amp;noscript=1" />
	</noscript></head>

<body class="page-template-default page page-id-4526 courses eduma learnpress learnpress-page woocommerce-no-js pmpro-body-has-access siteorigin-panels siteorigin-panels-before-js group-blog thim-body-preload bg-boxed-image"
 id="thim-body">
	<div id="preload">
		<div class="sk-folding-cube">
			<div class="sk-cube1 sk-cube"></div>
			<div class="sk-cube2 sk-cube"></div>
			<div class="sk-cube4 sk-cube"></div>
			<div class="sk-cube3 sk-cube"></div>
		</div>
	</div>
	<div id="wrapper-container" class="wrapper-container">
		<div class="content-pusher">
	<?php echo $__env->make('front.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('front.includes.nav', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
			<div id="main-content">
				<section class="content-area">
					<div class="top_heading  _out">
						<div class="top_site_main " style="color: #ffffff;background-image:url(<?php echo e(asset('assets/wp-content/themes/eduma/images/bg-page.jpg')); ?>);">
							<span class="overlay-top-header" style="background:rgba(0,0,0,0.5);"></span>
							<div class="page-title-wrapper">
								<div class="banner-wrapper container">
									<h1>Gallery</h1>
								</div>
							</div>
						</div>
						<div class="breadcrumbs-wrapper">
							<div class="container">
								<ul itemprop="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList" id="breadcrumbs" class="breadcrumbs">
									<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" href="../index.html"
										 title="Home"><span itemprop="name">Home</span></a></li>
									<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name" title="Gallery">
											Gallery</span></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="container site-content">
						<div class="row">
							<main id="main" class="site-main col-sm-12 full-width">
								<article id="post-4526" class="post-4526 page type-page status-publish hentry pmpro-has-access">
									<div class="entry-content">
										<div id="pl-4526" class="panel-layout">
											<div id="pg-4526-0" class="panel-grid panel-no-style">
												<div id="pgc-4526-0-0" class="panel-grid-cell">
													<div id="panel-4526-0-0-0" class="so-panel widget widget_gallery-posts panel-first-child panel-last-child"
													 data-index="0">
														<div class="thim-widget-gallery-posts thim-widget-gallery-posts-base">
															<div class="wrapper-filter-controls">
															      <ul class="filter-controls">
                                                      <li> <a class="filter active" data-filter="*" href="javascript:;">All</a></li>
                                                      <li><a class="filter" href="javascript:;" data-filter=".filter-11">what people say about us</a></li>
                                                      <li><a class="filter" href="javascript:;" data-filter=".filter-8">in class</a></li>
                                                      <li><a class="filter" href="javascript:;" data-filter=".filter-9">out class</a></li>
                                                   </ul>
															</div>

														        <div class="wrapper-gallery-filter row" itemscope itemtype="http://schema.org/ItemList">

      <?php $__currentLoopData = $gallerypeolesay; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gallerypeolesays): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                   <div class="item_gallery col-sm-3 item_post filter-11"><a class="thim-gallery-popup" href="#" data-id="4515"><img src="<?php echo e(Request::root()); ?>/public/uploads/gallerypeolesay/<?php echo e($gallerypeolesays->image); ?>" alt="Working" title="kid-course-6" width="440" height="440"></a></div>

			  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

  <?php $__currentLoopData = $galleryinclass; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $galleryinclasss): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                   <div class="item_gallery col-sm-3 item_post filter-8"><a class="thim-gallery-popup" href="#" data-id="4517"><img src="<?php echo e(Request::root()); ?>/public/uploads/gallerypeolesay/<?php echo e($galleryinclasss->image); ?>" alt="Our Classes" title="book-3" width="440" height="440"></a></div>

	  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

  <?php $__currentLoopData = $galleryoutclass; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $galleryoutclasss): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
         <div class="item_gallery col-sm-3 item_post filter-9"><a class="thim-gallery-popup" href="#" data-id="4512"><img src="<?php echo e(Request::root()); ?>/public/uploads/gallerypeolesay/<?php echo e($galleryoutclasss->image); ?>" alt="Life is Good" title="kid-course-3" width="440" height="440"></a></div>
   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


                                                   <!--<div class="item_gallery col-sm-3 item_post filter-8"><a class="thim-gallery-popup" href="#" data-id="4518"><img src="https://3ek5k1tux0822q3g83e30fye-wpengine.netdna-ssl.com/wp-content/uploads/2015/11/course-4-440x440.jpg" alt="EducationWP" title="course-4" width="440" height="440"></a></div>-->
                                                   <!--<div class="item_gallery col-sm-3 item_post filter-11"><a class="thim-gallery-popup" href="#" data-id="4516"><img src="https://3ek5k1tux0822q3g83e30fye-wpengine.netdna-ssl.com/wp-content/uploads/2016/03/book-2-440x440.jpg" alt="Our Books" title="book-2" width="440" height="440"></a></div>-->
                                                   <!--<div class="item_gallery col-sm-3 item_post filter-8"><a class="thim-gallery-popup" href="#" data-id="4517"><img src="https://3ek5k1tux0822q3g83e30fye-wpengine.netdna-ssl.com/wp-content/uploads/2016/03/book-3-440x440.jpg" alt="Our Classes" title="book-3" width="440" height="440"></a></div>-->
                                                   <!--<div class="item_gallery col-sm-3 item_post filter-9"><a class="thim-gallery-popup" href="#" data-id="4512"><img src="https://3ek5k1tux0822q3g83e30fye-wpengine.netdna-ssl.com/wp-content/uploads/2015/07/kid-course-3-440x440.jpg" alt="Life is Good" title="kid-course-3" width="440" height="440"></a></div>-->
                                                   <!--<div class="item_gallery col-sm-3 item_post filter-9"><a class="thim-gallery-popup" href="#" data-id="4513"><img src="https://3ek5k1tux0822q3g83e30fye-wpengine.netdna-ssl.com/wp-content/uploads/2016/03/presentation-3-440x440.jpg" alt="Presentation 2015" title="presentation-3" width="440" height="440"></a></div>-->
                                                   <!--<div class="item_gallery col-sm-3 item_post filter-8"><a class="thim-gallery-popup" href="#" data-id="4510"><img src="https://3ek5k1tux0822q3g83e30fye-wpengine.netdna-ssl.com/wp-content/uploads/2016/03/portfolio-course-3-440x440.jpg" alt="Learning" title="portfolio-course-3" width="440" height="440"></a></div>-->


                                                </div>


															<div class="thim-gallery-show"></div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</article>
							</main>
						</div>
					</div>
				</section>
			<?php echo $__env->make('front.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
			</div>
		</div> <a href="#" id="back-to-top"> <i class="fa fa-chevron-up" aria-hidden="true"></i> </a>
	</div>

	<div class="gallery-slider-content"></div>
	<script data-cfasync="false" src="../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
	<script data-cfasync="false" type="text/javascript">window.onload = function () {
			var thim_preload = document.getElementById("preload");
			if (thim_preload) {
				setTimeout(function () {
					var body = document.getElementById("thim-body"),
						len = body.childNodes.length,
						class_name = body.className.replace(/(?:^|\s)thim-body-preload(?!\S)/, '').replace(/(?:^|\s)thim-body-load-overlay(?!\S)/, '');

					body.className = class_name;
					if (typeof thim_preload !== 'undefined' && thim_preload !== null) {
						for (var i = 0; i < len; i++) {
							if (body.childNodes[i].id !== 'undefined' && body.childNodes[i].id == "preload") {
								body.removeChild(body.childNodes[i]);
								break;
							}
						}
					}
				}, 500);
			}
		};</script>
	<script>window.addEventListener('load', function () {
			/**
			 * Fix issue there is an empty spacing between image and title of owl-carousel
			 */
			setTimeout(function () {
				var $ = jQuery;
				var $carousel = $('.thim-owl-carousel-post').each(function () {
					$(this).find('.image').css('min-height', 0);
					$(window).trigger('resize');
				});
			}, 500);
		})</script>

	<script type='text/javascript'>var wc_add_to_cart_params = { "ajax_url": "\/wp-admin\/admin-ajax.php", "wc_ajax_url": "\/?wc-ajax=%%endpoint%%", "i18n_view_cart": "View cart", "cart_url": "https:\/\/educationwp.thimpress.com\/cart\/", "is_cart": "", "cart_redirect_after_add": "no" };</script>
	<script type='text/javascript'>var woocommerce_params = { "ajax_url": "\/wp-admin\/admin-ajax.php", "wc_ajax_url": "\/?wc-ajax=%%endpoint%%" };</script>
	<script type='text/javascript'>var wc_cart_fragments_params = { "ajax_url": "\/wp-admin\/admin-ajax.php", "wc_ajax_url": "\/?wc-ajax=%%endpoint%%", "cart_hash_key": "wc_cart_hash_8653f1d19ec105315cba116db9d7fe2e", "fragment_name": "wc_fragments_8653f1d19ec105315cba116db9d7fe2e" };</script>
	<script type='text/javascript'>var _wpUtilSettings = { "ajax": { "url": "\/wp-admin\/admin-ajax.php" } };</script>
	<script type='text/javascript'>var WPEMS = { "gmt_offset": "0", "current_time": "Nov 11, 2018 12:39:00 +0000", "l18n": { "labels": ["Years", "Months", "Weeks", "Days", "Hours", "Minutes", "Seconds"], "labels1": ["Year", "Month", "Week", "Day", "Hour", "Minute", "Second"] }, "ajaxurl": "https:\/\/educationwp.thimpress.com\/wp-admin\/admin-ajax.php", "something_wrong": "Something went wrong", "register_button": "c20eb61364" };</script>
	<script type='text/javascript'>var thim_js_translate = { "login": "Username", "password": "Password", "close": "Close" };</script>
	<script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js?ver=3.0.22'></script>
	<script type='text/javascript'>WebFont.load({ google: { families: ['Roboto Slab:300,400,700', 'Roboto:300,300i,400,400i,500,500i,700,700i'] } });</script>
	<script type='text/javascript'>var lpGlobalSettings = { "url": "https:\/\/educationwp.thimpress.com\/gallery\/", "siteurl": "https:\/\/educationwp.thimpress.com", "ajax": "https:\/\/educationwp.thimpress.com\/wp-admin\/admin-ajax.php", "theme": "eduma", "localize": { "button_ok": "OK", "button_cancel": "Cancel", "button_yes": "Yes", "button_no": "No" } };</script>
	<script type='text/javascript'>var lpCheckoutSettings = { "ajaxurl": "https:\/\/educationwp.thimpress.com", "user_waiting_payment": null, "user_checkout": "", "i18n_processing": "Processing", "i18n_redirecting": "Redirecting", "i18n_invalid_field": "Invalid field", "i18n_unknown_error": "Unknown error", "i18n_place_order": "Place order" };</script>
	<script type='text/javascript'>var lpProfileUserSettings = { "processing": "Processing", "redirecting": "Redirecting", "avatar_size": { "width": 150, "height": 150 } };</script>
	<script type='text/javascript'>var lpCourseSettings = [""];</script>
	<script type='text/javascript'>var lpQuizSettings = [];</script>
	<script type="text/javascript">document.body.className = document.body.className.replace("siteorigin-panels-before-js", "");</script>
	<div class="tp_chameleon_overlay">
		<div class="tp_chameleon_progress">
			<div class="tp_chameleon_heading">Processing!</div>
		</div>
	</div>
	<script type="text/javascript">var tp_chameleon_url_stylesheet = '../wp-content/themes/eduma/style-1.html';
		var tp_chameleon_url_admin_ajax = '../wp-admin/admin-ajax.html';
		var tp_chameleon_wp_nonce = 'e39f6066ef';
		var tp_chameleon_primary_color = 'rgb(255, 182, 6)';
		var tp_chameleon_selector_wrapper_box = '.content-pusher';
		var tp_chameleon_class_boxed = 'boxed-area';
		var tp_chameleon_src_patterns = '../wp-content/plugins/tp-chameleon/images/patterns/index.html';
		var tp_chameleon_setting = {
			layout: tp_chameleon_getCookie('tp_chameleon_layout'),
			pattern_type: tp_chameleon_getCookie('tp_chameleon_pattern_type'),
			pattern_src: tp_chameleon_getCookie('tp_chameleon_pattern_src'),
			primary_color: tp_chameleon_getCookie('tp_chameleon_primary_color'),
			primary_color_rgb: tp_chameleon_getCookie('tp_chameleon_primary_color_rgb'),
			body_font: tp_chameleon_getCookie('tp_chameleon_body_font'),
			body_font_code: tp_chameleon_getCookie('tp_chameleon_body_font_code'),
			heading_font: tp_chameleon_getCookie('tp_chameleon_heading_font'),
			heading_font_code: tp_chameleon_getCookie('tp_chameleon_heading_font_code')
		};



		var tp_chameleon_site_url = '../index.html';

		function tp_chameleon_setCookie(cname, cvalue, exdays) {
			var d = new Date();
			d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
			var expires = "expires=" + d.toUTCString();
			document.cookie = cname + "=" + cvalue + "; " + expires;
		}

		function tp_chameleon_deleteCookie(cname) {
			var d = new Date();
			d.setTime(d.getTime() + (24 * 60 * 60 * 1000));
			var expires = "expires=" + d.toUTCString();
			document.cookie = cname + "=; " + expires;
		}

		function tp_chameleon_deleteAllCookie() {
			var all_cookie = [
				'tp_chameleon_layout',
				'tp_chameleon_pattern_type',
				'tp_chameleon_pattern_src',
				'tp_chameleon_primary_color',
				'tp_chameleon_primary_color_rgb',
				'tp_chameleon_body_font',
				'tp_chameleon_body_font_code',
				'tp_chameleon_heading_font',
				'tp_chameleon_heading_font_code'
			];

			for (var i = 0; i < all_cookie.length; i++) {
				tp_chameleon_deleteCookie(all_cookie[i]);
			}
		}

		function tp_chameleon_getCookie(cname) {
			var name = cname + "=";
			var ca = document.cookie.split(';');
			for (var i = 0; i < ca.length; i++) {
				var c = ca[i];
				while (c.charAt(0) == ' ') c = c.substring(1);
				if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
			}

			return '';
		}

		function tp_chameleon_set_first_visit() {
			tp_chameleon_setCookie('tp_chameleon_first_visit', 1, 1);
		}

		function tp_chameleon_check_first_visit() {
			return !(tp_chameleon_getCookie('tp_chameleon_first_visit') == '1');
		}

		jQuery(document).ready(function ($) {
			var $preview = $('.tp-preview-images');

			$('.tp_demo').hover(
				function (event) {
					var url_prewview = $(this).attr('data-preview');
					if (url_prewview) {
						$preview.find('img').attr('src', url_prewview);
						$preview.show();
					}
				},
				function () {
					$preview.hide();
				}
			);

			$('.tp_demo').mousemove(function (event) {
				var y = (event.clientY);
				$preview.css('top', y - 250);
			});

			function tp_chameleon_open() {
				tp_chameleon_set_first_visit();
				$('#tp_style_selector').addClass('show').animate({ right: '0px' }, 'medium');
				$('#tp_style_selector .open').hide();
				$('#tp_style_selector .close').show();
			}

			function tp_chameleon_close() {
				$('#tp_style_selector').removeClass('show').animate({ right: '-300px' }, 'medium');
				$('#tp_style_selector .close').hide();
				$('#tp_style_selector .open').show();
			}

			function tp_form_newsletter_show() {
				tp_chameleon_set_first_visit();
				$(window).scroll(function () {
					if ($(this).scrollTop() > 200) {
						$('.tp-email-form .button-email').addClass('bounceInUp animated active');
						var cookie_name = 'hide_form_email1';
						$.cookie(cookie_name, '1', { expires: 3, path: '/' });
					}
				});
			}

			function tp_change_background_pattern(url_pattern) {
				var $body = $('body');
				$body.removeClass('tp_background_image');
				$body.addClass('tp_background_pattern');
				$body.css('background-image', 'url("' + url_pattern + '")')
			}

			function tp_change_background_image(url_image) {
				var $body = $('body');
				$body.removeClass('tp_background_pattern');
				$body.addClass('tp_background_image');
				$body.css('background-image', 'url("' + url_image + '")')
			}

			function tp_chameleon_change_layout_wide() {
				tp_chameleon_setCookie('tp_chameleon_layout', 'wide', 1);

				var $body = $('body');
				$('.tp-change-layout').removeClass('active');
				$('.tp-change-layout.layout-wide').addClass('active');
				$('#tp_style_selector .boxed-mode').slideUp(300);
				$(tp_chameleon_selector_wrapper_box).removeClass(tp_chameleon_class_boxed);
				$body.css('background-image', 'none');
			}

			function tp_chameleon_change_layout_boxed() {
				tp_chameleon_setCookie('tp_chameleon_layout', 'boxed', 1);
				$('.tp-change-layout').removeClass('active');
				$('.tp-change-layout.layout-boxed').addClass('active');
				$('#tp_style_selector .boxed-mode').slideDown(300);
				$(tp_chameleon_selector_wrapper_box).addClass(tp_chameleon_class_boxed);
			}

			function tp_chameleon_change_background_pattern(pattern_src) {
				tp_chameleon_setCookie('tp_chameleon_pattern_src', pattern_src, 1);
				tp_chameleon_setCookie('tp_chameleon_pattern_type', 'pattern', 1);
				var pattern_url = tp_chameleon_src_patterns + pattern_src;
				tp_change_background_pattern(pattern_url);
			}

			function tp_chameleon_change_background_image(pattern_src) {
				tp_chameleon_setCookie('tp_chameleon_pattern_src', pattern_src, 1);
				tp_chameleon_setCookie('tp_chameleon_pattern_type', 'image', 1);
				var pattern_url = tp_chameleon_src_patterns + pattern_src;
				tp_change_background_image(pattern_url);
			}

			var $body_font = '<style id="tp_chameleon_body_font" type="text/css"></style>';
			var $heading_font = '<style id="tp_chameleon_heading_font" type="text/css"></style>';
			var $stylesheet = '<link id="tp_chameleon_stylesheet" type="text/css" rel="stylesheet">';

			var $tp_head = $('head');
			$tp_head.append($stylesheet);

			var $tp_body = $('body');
			$tp_body.append($body_font);
			$tp_body.append($heading_font);

			if (tp_chameleon_setting.layout == 'wide') {
				tp_chameleon_change_layout_wide();
			}
			if (tp_chameleon_setting.layout == 'boxed') {
				tp_chameleon_change_layout_boxed();

				if (tp_chameleon_setting.pattern_type == 'pattern' && tp_chameleon_setting.pattern_src != '') {
					tp_chameleon_change_background_pattern(tp_chameleon_setting.pattern_src);
					$('.tp_pattern[data-src="' + tp_chameleon_setting.pattern_src + '"]').addClass('active');
				}

				if (tp_chameleon_setting.pattern_type == 'image' && tp_chameleon_setting.pattern_src != '') {
					tp_chameleon_change_background_image(tp_chameleon_setting.pattern_src);
					$('.tp_image[data-src="' + tp_chameleon_setting.pattern_src + '"]').addClass('active');
				}
			}

			$('.tp-chameleon-clear').click(function (event) {
				event.preventDefault();
				tp_chameleon_deleteAllCookie();
				document.location.reload();
			});

			$('.tp-btn.tp-change-layout').click(function (event) {
				event.preventDefault();

				if ($(this).hasClass('layout-wide')) {
					tp_chameleon_change_layout_wide();

				} else {
					tp_chameleon_change_layout_boxed();

				}
			});

			$('.tp_pattern').click(function (event) {
				event.preventDefault();
				$('.tp_pattern').removeClass('active');
				$('.tp_image').removeClass('active');
				$(this).addClass('active');
				var pattern_src = $(this).attr('data-src');
				tp_chameleon_change_background_pattern(pattern_src);
			});

			$('.tp_image').click(function (event) {
				event.preventDefault();
				$('.tp_pattern').removeClass('active');
				$('.tp_image').removeClass('active');
				$(this).addClass('active');
				var pattern_src = $(this).attr('data-src');
				tp_chameleon_change_background_image(pattern_src);
			});

			/**
			 * Open/Close box
			 */
			$('#tp_style_selector .close').click(function (e) {
				e.preventDefault();
				tp_chameleon_close();
			});

			$('#tp_style_selector .open').click(function (e) {
				e.preventDefault();
				tp_chameleon_open();
			});

			$(window).load(function () {
				var $btn = $('.tp-chameleon-btn-buy');
				$btn.animate({
					bottom: $btn.attr('data-bottom')
				}, 500);

				//Set view-more-slider button
				$('.view-more-slider .text:last-child').hide();
				setInterval(function () {
					$('.view-more-slider .text:first-child').hide();
					$('.view-more-slider .text:last-child').show();
					setTimeout(function () {
						$('.view-more-slider .text:first-child').show();
						$('.view-more-slider .text:last-child').hide();
					}, 2000);
				}, 4500);

			});

			/**
			 * Check firt visit
			 */
			if (tp_chameleon_check_first_visit()) {
				//set visit is second
				setTimeout(tp_chameleon_open, 5000);
			} else {
				$('#tp_style_selector').click(function (event) {
					tp_chameleon_set_first_visit();
				});
			}
			if (tp_chameleon_check_first_visit()) {
				tp_form_newsletter_show();
			}


			$('.tp-email-form .button-email').click(function (e) {
				e.preventDefault();
				$('.tp-email-form .email-form-popup').addClass('show');
			});
			$('.tp-email-form .email-form-popup .close-popup').click(function (e) {
				e.preventDefault();
				$('.tp-email-form .email-form-popup').removeClass('show');
				$('.tp-email-form .button-email').addClass('hide');
			});

			$('.tp-email-form .email-form-popup .email-form-subscribe form ').submit(function (e) {
				e.preventDefault();
				var form = $(this);
				var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
				form.append('<div class="loading"><i class="fa fa-spinner fa-pulse"></i></div>');
				if (form.find('input[type="email"]').val()) {
					$.ajax({
						type: "POST",
						url: 'https://preview.thimpress.com/m/mailster/subscribe',
						data: form.serialize(),
						complete: function (xhr, textStatus) {
							form.find('.loading').remove();
							form.append('<div class="message-success">Please check your inbox or spam folder for confirmation email!</div>');
							setTimeout(function () {
								form.find('.message-success').remove();
							}, 2000);
						}
					});
				} else {
					form.find('.loading').remove();
					form.append('<div class="message-error">Please enter email address</div>');
					setTimeout(function () { form.find('.message-error').remove(); }, 2000);
					form.find('input[type="email"]').addClass('error');
				}


			});

			$('.tp-email-form .email-form-popup .email-form-subscribe form input[type="email"]').focus(function () {
				$(this).removeClass('error');
			});
		});</script>
    <script type="text/jscript" src="<?php echo e(asset('assets/slider/ahmed2.js')); ?>" ></script>

	<!-- <script type="text/javascript" defer src="https://3ek5k1tux0822q3g83e30fye-wpengine.netdna-ssl.com/wp-content/cache/autoptimize/1/js/autoptimize_704b916c5e3900ea21f1833f144fad05.js"></script> -->
</body>
<!-- Mirrored from educationwp.thimpress.com/gallery/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 11 Nov 2018 12:44:26 GMT -->

</html>

add|videos
<?php $__env->startSection('page-header'); ?>
    <section class="content-header">
        <h1>
add videos           <small></small>
        </h1>

    </section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">   </h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->


                    <form class="form-horizontal" method="post"  action ="<?php echo e(url('/admin/videos')); ?>"enctype="multipart/form-data">
                        <?php echo e(csrf_field()); ?>

                    <div class="box-body">





                      <div class="form-group">

                    <label for="education_year" class="col-sm-4 control-label"> course name </label>

                      <div class="col-sm-4 <?php echo e($errors->has('education_year') ? ' has-error' : ''); ?>">


                          <!--<input type="text" name="education_year" class="form-control" id="education_year" placeholder="قم الموبيل" value="<?php echo e(old('education_year')); ?>">-->
                          <select class="form-control" id="course" name="course">
                              <option> </option>

                                    <?php $__currentLoopData = $courses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $course): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                              <option value="<?php echo e($course->id); ?>"> <?php echo e($course->courses_title); ?> </option>


                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


                    </select>


                          <?php if($errors->has('course')): ?>
                              <span class="help-block">
                    <strong><?php echo e($errors->first('course')); ?></strong>
                    </span>
                          <?php endif; ?>
                      </div>

                    </div>




















                        <div class="form-group">
                    <label for="education_year" class="col-sm-4 control-label">  video name</label>

                        <div class="col-sm-4 <?php echo e($errors->has('education_year') ? ' has-error' : ''); ?>">


              <input type="text" name="video_name" class="form-control"  placeholder="video name" value="<?php echo e(old('video_name')); ?>">



                  </select>


                            <?php if($errors->has('video_name')): ?>
                                <span class="help-block">
                    <strong><?php echo e($errors->first('video_name')); ?></strong>
                    </span>
                            <?php endif; ?>
                        </div>

                    </div>




                    <div class="form-group">

                  <label for="education_year" class="col-sm-4 control-label"> video_type </label>

                    <div class="col-sm-4 <?php echo e($errors->has('education_year') ? ' has-error' : ''); ?>">


                        <!--<input type="text" name="education_year" class="form-control" id="education_year" placeholder="قم الموبيل" value="<?php echo e(old('education_year')); ?>">-->
                        <select class="form-control"  name="video_type">
                            <option value="normal"> normal</option>
                            <option value="intro"> intro</option>





                  </select>


                        <?php if($errors->has('video_type')): ?>
                            <span class="help-block">
                  <strong><?php echo e($errors->first('video_type')); ?></strong>
                  </span>
                        <?php endif; ?>
                    </div>

                  </div>








                  <div class="form-group">
              <label for="education_year" class="col-sm-4 control-label">  video link</label>

                  <div class="col-sm-4 <?php echo e($errors->has('video') ? ' has-error' : ''); ?>">


        <input type="text" name="video" class="form-control"  placeholder="video link" value="<?php echo e(old('video')); ?>">



            </select>


                      <?php if($errors->has('video')): ?>
                          <span class="help-block">
              <strong><?php echo e($errors->first('video')); ?></strong>
              </span>
                      <?php endif; ?>
                  </div>

              </div>









<div >

     </div>







                    </div>







                </div>










                    </div>



                    <div class="box-footer">
                    <button type="submit" class="btn btn-info center-block">save</button>
                    </div>



                    </form>
                </div>

            </div>
        </div>
    </section>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>

    <link rel="stylesheet" href="<?php echo e(asset('assets/bower_components/select2/dist/css/select2.min.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>

    <script src="<?php echo e(asset('assets/bower_components/select2/dist/js/select2.min.js')); ?>"></script>


    <script>

  $('.select2').select2()


    </script>

    <script>
        $('#classes').change(function(event){

 var classes =  $('#classes').val();
$.ajax({
url:'<?php echo e(url('admin/student/classes/classes').'/'); ?>'+classes,
data:classes,
async:false,
type:'get',
processData: false,
contentType: false,
success:function(data){
                $('#show_cities').html(data.html);
                console.log(data);


},

error: function (error) {
console.log(error);
}

});
});
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
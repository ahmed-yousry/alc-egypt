<?php $__env->startSection('title'); ?>
leassone
<?php $__env->stopSection(); ?>
 <?php echo $__env->make('front.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<body class="lp_course-template-default single single-lp_course postid-5428 eduma learnpress learnpress-page woocommerce-no-js pmpro-body-has-access group-blog thim-body-preload bg-boxed-image"
    id="thim-body">
    <div id="preload">
        <div class="sk-folding-cube">
            <div class="sk-cube1 sk-cube"></div>
            <div class="sk-cube2 sk-cube"></div>
            <div class="sk-cube4 sk-cube"></div>
            <div class="sk-cube3 sk-cube"></div>
        </div>
    </div>
    <div id="wrapper-container" class="wrapper-container">
        <div class="content-pusher">


 <?php echo $__env->make('front.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
 <?php echo $__env->make('front.includes.nav', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


            <div id="main-content">
                <section class="content-area">

                    <div class="top_heading  _out">
                        <div class="top_site_main " style="color: #ffffff;background-image:url(<?php echo e(asset('assets/wp-content/themes/eduma/images/bg-page.jpg')); ?>);">
                            <span class="overlay-top-header" style="background:rgba(0,0,0,0.5);"></span>
                            <div class="page-title-wrapper">
                                <div class="banner-wrapper container">
                                    <h2>General</h2>
                                </div>
                            </div>
                        </div>
                        <div class="breadcrumbs-wrapper">
                            <div class="container">
                                <ul itemprop="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList" id="breadcrumbs"
                                    class="breadcrumbs">
                                    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a
                                            itemprop="item" href="../../index.html" title="Home"><span itemprop="name">Home</span></a></li>
                                    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a
                                            itemprop="item" href="../index.html" title="All courses"><span itemprop="name">All
                                                courses</span></a></li>
                                    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a
                                            itemprop="item" href="../../course-category/general/index.html" title="General"><span
                                                itemprop="name">General</span></a></li>
                                    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span
                                            itemprop="name" title="Introduction LearnPress &#8211; LMS plugin">Introduction
                                            LearnPress &#8211; LMS plugin</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>




                    <div class="container site-content sidebar-right">
                        <div class="row">
                            <main id="main" class="site-main col-sm-9 alignleft">
                                <article id="post-5428" class="post-5428 lp_course type-lp_course status-publish has-post-thumbnail hentry course_category-general pmpro-has-access course">
                                    <div class="entry-content">
                                        <div id="lp-single-course" class="lp-single-course">
                                            <div id="learn-press-course" class="course-summary learn-press">
                                                <h1 class="entry-title" itemprop="name">          <?php if( ! empty($videos)): ?> <?php echo e($videos->video_name); ?>      <?php endif; ?> &#8211;
                                                  </h1>
                                                <div class="course-meta">
                                                    <div class="course-author"> <img alt="Admin bar avatar" src="https://3ek5k1tux0822q3g83e30fye-wpengine.netdna-ssl.com/wp-content/uploads/learn-press-profile/7/9c081444f942cc8fe0ddf55631b584e2.jpg"
                                                            class="avatar avatar-96 photo" height="96" width="96" />
                                                        <div class="author-contain"> <label itemprop="jobTitle">Teacher</label>
                                                            <div class="value" itemprop="name"> <a href="../../profile/keny/index.html">Keny
                                                                    White</a></div>
                                                        </div>
                                                    </div>
                                                    <div class="course-categories"> <label>Categories</label>
                                                        <div class="value"> <span class="cat-links"><a href="../../course-category/general/index.html"
                                                                    rel="tag">General</a></span></div>
                                                    </div>
                                                    <div class="course-review"> <label>Review</label>
                                                        <div class="value">
                                                            <div class="review-stars-rated">
                                                                <ul class="review-stars">
                                                                    <li><span class="fa fa-star-o"></span></li>
                                                                    <li><span class="fa fa-star-o"></span></li>
                                                                    <li><span class="fa fa-star-o"></span></li>
                                                                    <li><span class="fa fa-star-o"></span></li>
                                                                    <li><span class="fa fa-star-o"></span></li>
                                                                </ul>
                                                                <ul class="review-stars filled" style="width: calc(100% - 2px)">
                                                                    <li><span class="fa fa-star"></span></li>
                                                                    <li><span class="fa fa-star"></span></li>
                                                                    <li><span class="fa fa-star"></span></li>
                                                                    <li><span class="fa fa-star"></span></li>
                                                                    <li><span class="fa fa-star"></span></li>
                                                                </ul>
                                                            </div> <span>(3 reviews)</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="course-payment">
                                                    <div class="course-price">
                                                        <div class="value  free-course"> Free</div>
                                                    </div>
                                                    <div class="lp-course-buttons">
                                                        <form name="enroll-course" class="enroll-course form-purchase-course"
                                                            method="post" enctype="multipart/form-data"> <input type="hidden"
                                                                name="enroll-course" value="5428" /> <input type="hidden"
                                                                name="enroll-course-nonce" value="c42ba65daa" />
                                                            <button class="lp-button button button-enroll-course"> Take
                                                                this course </button> <input type="hidden" name="redirect_to"
                                                                value=""></form>
                                                    </div>
                                                </div>





                                                <div class="course-summary">
                                                    <div class="course-thumbnail">
                                                        <div class="media-intro"> <iframe width="900" height="500" src=" <?php if( ! empty($videos->video_link)): ?>  <?php echo e($videos->video_link); ?>  <?php endif; ?>"
                                                                frameborder="0" allow="autoplay; encrypted-media"
                                                                allowfullscreen></iframe></div>
                                                    </div>






                                                    <div id="course-landing" class="course-landing-summary">
                                                        <div id="learn-press-course-tabs" class="course-tabs">
                                                            <ul class="nav nav-tabs">
                                                                <li role="presentation" class="course-nav-tab-overview active">
                                                                    <a href="#tab-overview" data-toggle="tab"> <i class="fa fa-bookmark"></i>
                                                                        <span>Overview</span> </a></li>
                                                                <li role="presentation" class="course-nav-tab-curriculum">
                                                                    <a href="#tab-curriculum" data-toggle="tab"> <i
                                                                            class="fa fa-cube"></i> <span>Curriculum</span>
                                                                    </a></li>
                                                                <li role="presentation" class="course-nav-tab-instructor">
                                                                    <a href="#tab-instructor" data-toggle="tab"> <i
                                                                            class="fa fa-user"></i> <span>Instructor</span>
                                                                    </a></li>
                                                                <li role="presentation" class="course-nav-tab-reviews">
                                                                    <a href="#tab-reviews" data-toggle="tab"> <i class="fa fa-comments"></i>
                                                                        <span>Reviews</span> </a></li>
                                                            </ul>
                                                            <div class="tab-content">
                                                                <div class="tab-pane course-tab-panel-overview course-tab-panel active"
                                                                    id="tab-overview">
                                                                    <div class="course-description" id="learn-press-course-description">
                                                                        <div class="thim-course-content">
                                                                            <h4>COURSE DESCRIPTION</h4>
                                                                            <p><?php echo e($courses->first()->courses_description); ?>

                                                       </p>

                                                                        </div>

                                                                    </div>
                                                                </div>
                                                                <div class="tab-pane course-tab-panel-curriculum course-tab-panel"
                                                                    id="tab-curriculum">
                                                                    <div class="thim-curriculum-buttons"></div>
                                                                    <div class="course-curriculum" id="learn-press-course-curriculum">
                                                                        <div class="curriculum-scrollable">
                                                                            <nav class="thim-font-heading learn-press-breadcrumb"
                                                                                itemprop="breadcrumb"><a href="../../course-category/general/index.html">General</a><i
                                                                                    class="fa-angle-right fa"></i><span
                                                                                    class="item-name">Introduction
                                                                                    LearnPress &#8211; LMS plugin</span></nav>
                                                                            <ul class="curriculum-sections">
                                                                                <li class="section" id="section-learnpress-getting-started-219"
                                                                                    data-id="learnpress-getting-started-219"
                                                                                    data-section-id="219">
                                                                                    <h4 class="section-header"> <span
                                                                                            class="collapse"></span>
                                                                                        LearnPress Getting Started
                                                                                        <span class="meta"> <span class="step">0/2</span>
                                                                                        </span></h4>
                                                                                            <?php $__currentLoopData = $allvideos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $video): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                                    <ul class="section-content">



                                                                                        <li class="course-item course-item-lp_lesson course-item-5429 course-item-type-video item-preview has-status course-lesson"
                                                                                            data-type="lp_lesson">
                                                                                            <div class="meta-left">
                                                                                                <span class="course-format-icon"><i
                                                                                                        class="fa fa-play-circle"></i></span>
                                                                                                <div class="index"><span
                                                                                                        class="label">Lecture</span>1.1</div>
                                                                                            </div> <a class="lesson-title course-item-title button-load-item"
                                                                                                href="list/<?php echo e($video->id); ?>">
                                                                                              <?php echo e($video->video_name); ?></a>
                                                                                            <span class="meta duration">50
                                                                                                min</span>
                                                                                            <div class="course-item-meta">
                                                                                                <a title="Previews"
                                                                                                    class="lesson-preview button-load-item"

                                                                                                    data-id="5429"
                                                                                                    data-complete-nonce="a1a9fe8c4f">
                                                                                                    <i class="fa fa-eye"
                                                                                                        data-preview="Preview"></i>
                                                                                                </a></div>
                                                                                        </li>






                                                                                    </ul>
                                                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="tab-pane course-tab-panel-instructor course-tab-panel"
                                                                    id="tab-instructor">
                                                                    <div class="thim-about-author">
                                                                        <div class="author-wrapper">
                                                                            <div class="author-avatar"> <img alt="Admin bar avatar"
                                                                                    src="https://3ek5k1tux0822q3g83e30fye-wpengine.netdna-ssl.com/wp-content/uploads/learn-press-profile/7/9c081444f942cc8fe0ddf55631b584e2.jpg"
                                                                                    class="avatar avatar-110 photo"
                                                                                    height="110" width="110" /></div>
                                                                            <div class="author-bio">
                                                                                <div class="author-top"> <a class="name"
                                                                                        href="../../profile/keny/index.html">
                                                                                        Keny White </a>
                                                                                    <p class="job">Professor</p>
                                                                                </div>
                                                                                <ul class="thim-author-social">
                                                                                    <li> <a href="http://example.com/eduma-the-best-lms-wordpress/theme"
                                                                                            class="facebook"><i class="fa fa-facebook"></i></a></li>
                                                                                    <li> <a href="http://example.com/eduma-the-best-lms-wordpress/theme"
                                                                                            class="twitter"><i class="fa fa-twitter"></i></a></li>
                                                                                    <li> <a href="http://example.com/eduma-the-best-lms-wordpress/theme"
                                                                                            class="google-plus"><i
                                                                                                class="fa fa-google-plus"></i></a></li>
                                                                                    <li> <a href="http://example.com/eduma-the-best-lms-wordpress/theme"
                                                                                            class="linkedin"><i class="fa fa-linkedin"></i></a></li>
                                                                                    <li> <a href="http://example.com/eduma-the-best-lms-wordpress/theme"
                                                                                            class="youtube"><i class="fa fa-youtube"></i></a></li>
                                                                                </ul>
                                                                            </div>
                                                                            <div class="author-description"> Keny White
                                                                                is Professor of the Department of
                                                                                Computer Science at Boston University,
                                                                                where he has been since 2004. He also
                                                                                currently serves as Chief Scientist of
                                                                                Guavus, Inc. During 2003-2004 he was a
                                                                                Visiting Associate Professor at the
                                                                                Laboratoire d'Infomatique de Paris VI
                                                                                (LIP6). He received a B.S. from Cornell
                                                                                University in 1992, and an M.S. from
                                                                                the State University of New York at
                                                                                Buffalo.</div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="thim-about-author thim-co-instructor"
                                                                        itemprop="contributor" itemscope itemtype="http://schema.org/Person">
                                                                        <div class="author-wrapper">
                                                                            <div class="author-avatar"> <img alt='' src='https://secure.gravatar.com/avatar/17445360b49029117e3e61d7fc65c959?s=110&amp;r=g'
                                                                                    srcset='https://secure.gravatar.com/avatar/17445360b49029117e3e61d7fc65c959?s=220&#038;r=g 2x'
                                                                                    class='avatar avatar-110 photo'
                                                                                    height='110' width='110' /></div>
                                                                            <div class="author-bio">
                                                                                <div class="author-top"> <a itemprop="url"
                                                                                        class="name" href="../../profile/johndoe/index.html">
                                                                                        <span itemprop="name">John Doe</span>
                                                                                    </a>
                                                                                    <p class="job" itemprop="jobTitle">Bachelor</p>
                                                                                </div>
                                                                                <ul class="thim-author-social">
                                                                                    <li> <a href="http://example.com/eduma-the-best-lms-wordpress/theme"
                                                                                            class="facebook"><i class="fa fa-facebook"></i></a></li>
                                                                                    <li> <a href="http://example.com/eduma-the-best-lms-wordpress/theme"
                                                                                            class="twitter"><i class="fa fa-twitter"></i></a></li>
                                                                                    <li> <a href="http://example.com/eduma-the-best-lms-wordpress/theme"
                                                                                            class="google-plus"><i
                                                                                                class="fa fa-google-plus"></i></a></li>
                                                                                    <li> <a href="http://example.com/eduma-the-best-lms-wordpress/theme"
                                                                                            class="linkedin"><i class="fa fa-linkedin"></i></a></li>
                                                                                    <li> <a href="http://example.com/eduma-the-best-lms-wordpress/theme"
                                                                                            class="youtube"><i class="fa fa-youtube"></i></a></li>
                                                                                </ul>
                                                                            </div>
                                                                            <div class="author-description" itemprop="description">
                                                                                After working as a software developer
                                                                                and contractor for over 8 years for a
                                                                                whole bunch of companies including ABX,
                                                                                Proit, SACC and AT&amp;T in the US, He
                                                                                decided to work full-time as a private
                                                                                software trainer.</div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="thim-about-author thim-co-instructor"
                                                                        itemprop="contributor" itemscope itemtype="http://schema.org/Person">
                                                                        <div class="author-wrapper">
                                                                            <div class="author-avatar"> <img alt="Admin bar avatar"
                                                                                    src="https://3ek5k1tux0822q3g83e30fye-wpengine.netdna-ssl.com/wp-content/uploads/learn-press-profile/1/21232f297a57a5a743894a0e4a801fc3.jpg"
                                                                                    class="avatar avatar-110 photo"
                                                                                    height="110" width="110" /></div>
                                                                            <div class="author-bio">
                                                                                <div class="author-top"> <a itemprop="url"
                                                                                        class="name" href="../../profile/admin/index.html">
                                                                                        <span itemprop="name">Hinata
                                                                                            Hyuga</span> </a>
                                                                                    <p class="job" itemprop="jobTitle">Web
                                                                                        Developer</p>
                                                                                </div>
                                                                                <ul class="thim-author-social">
                                                                                    <li> <a href="http://example.com/eduma-the-best-lms-wordpress/theme"
                                                                                            class="facebook"><i class="fa fa-facebook"></i></a></li>
                                                                                    <li> <a href="http://example.com/eduma-the-best-lms-wordpress/theme"
                                                                                            class="twitter"><i class="fa fa-twitter"></i></a></li>
                                                                                    <li> <a href="http://example.com/eduma-the-best-lms-wordpress/theme"
                                                                                            class="google-plus"><i
                                                                                                class="fa fa-google-plus"></i></a></li>
                                                                                    <li> <a href="http://example.com/eduma-the-best-lms-wordpress/theme"
                                                                                            class="linkedin"><i class="fa fa-linkedin"></i></a></li>
                                                                                    <li> <a href="http://example.com/eduma-the-best-lms-wordpress/theme"
                                                                                            class="youtube"><i class="fa fa-youtube"></i></a></li>
                                                                                </ul>
                                                                            </div>
                                                                            <div class="author-description" itemprop="description">
                                                                                Hello, I’m a spider man, real spider
                                                                                man you alway see in your TV. Surprise,
                                                                                i’m also a photographer and a
                                                                                photography teacher. You know, in my
                                                                                films, i alway take a lot of photos of
                                                                                me_spider man.</div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="tab-pane course-tab-panel-reviews course-tab-panel"
                                                                    id="tab-reviews">
                                                                    <div class="course-rating">
                                                                        <h3>Reviews</h3>
                                                                        <div class="average-rating" itemprop="aggregateRating"
                                                                            itemscope="" itemtype="http://schema.org/AggregateRating">
                                                                            <p class="rating-title">Average Rating</p>
                                                                            <div class="rating-box">
                                                                                <div class="average-value" itemprop="ratingValue">5</div>
                                                                                <div class="review-star">
                                                                                    <div class="review-stars-rated">
                                                                                        <ul class="review-stars">
                                                                                            <li><span class="fa fa-star-o"></span></li>
                                                                                            <li><span class="fa fa-star-o"></span></li>
                                                                                            <li><span class="fa fa-star-o"></span></li>
                                                                                            <li><span class="fa fa-star-o"></span></li>
                                                                                            <li><span class="fa fa-star-o"></span></li>
                                                                                        </ul>
                                                                                        <ul class="review-stars filled"
                                                                                            style="width: calc(100% - 2px)">
                                                                                            <li><span class="fa fa-star"></span></li>
                                                                                            <li><span class="fa fa-star"></span></li>
                                                                                            <li><span class="fa fa-star"></span></li>
                                                                                            <li><span class="fa fa-star"></span></li>
                                                                                            <li><span class="fa fa-star"></span></li>
                                                                                        </ul>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="review-amount" itemprop="ratingCount">
                                                                                    3 ratings</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="detailed-rating">
                                                                            <p class="rating-title">Detailed Rating</p>
                                                                            <div class="rating-box">
                                                                                <div class="detailed-rating">
                                                                                    <div class="stars">
                                                                                        <div class="key">5 Star</div>
                                                                                        <div class="bar">
                                                                                            <div class="full_bar">
                                                                                                <div style="width:100% "></div>
                                                                                            </div>
                                                                                        </div> <span>100%</span>
                                                                                    </div>
                                                                                    <div class="stars">
                                                                                        <div class="key">4 Star</div>
                                                                                        <div class="bar">
                                                                                            <div class="full_bar">
                                                                                                <div style="width:0% "></div>
                                                                                            </div>
                                                                                        </div> <span>0%</span>
                                                                                    </div>
                                                                                    <div class="stars">
                                                                                        <div class="key">3 Star</div>
                                                                                        <div class="bar">
                                                                                            <div class="full_bar">
                                                                                                <div style="width:0% "></div>
                                                                                            </div>
                                                                                        </div> <span>0%</span>
                                                                                    </div>
                                                                                    <div class="stars">
                                                                                        <div class="key">2 Star</div>
                                                                                        <div class="bar">
                                                                                            <div class="full_bar">
                                                                                                <div style="width:0% "></div>
                                                                                            </div>
                                                                                        </div> <span>0%</span>
                                                                                    </div>
                                                                                    <div class="stars">
                                                                                        <div class="key">1 Star</div>
                                                                                        <div class="bar">
                                                                                            <div class="full_bar">
                                                                                                <div style="width:0% "></div>
                                                                                            </div>
                                                                                        </div> <span>0%</span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="review-stars-rated">
                                                                            <div class="review-stars empty"></div>
                                                                            <div class="review-stars filled" style="width:100%;"></div>
                                                                        </div>
                                                                        <div></div>
                                                                    </div>
                                                                    <div class="course-review">
                                                                        <div id="course-reviews" class="content-review">
                                                                            <ul class="course-reviews-list">
                                                                                <li>
                                                                                    <div class="review-container"
                                                                                        itemprop="review" itemscope
                                                                                        itemtype="http://schema.org/Review">
                                                                                        <div class="review-author">
                                                                                            <img alt="Admin bar avatar"
                                                                                                src="https://3ek5k1tux0822q3g83e30fye-wpengine.netdna-ssl.com/wp-content/uploads/learn-press-profile/12/athony1-thumb.jpg"
                                                                                                class="avatar avatar-96 photo"
                                                                                                height="96" width="96" /></div>
                                                                                        <div class="review-text">
                                                                                            <h4 class="author-name"
                                                                                                itemprop="author">
                                                                                                minhluu</h4>
                                                                                            <div class="review-star">
                                                                                                <div class="review-stars-rated">
                                                                                                    <ul class="review-stars">
                                                                                                        <li><span class="fa fa-star-o"></span></li>
                                                                                                        <li><span class="fa fa-star-o"></span></li>
                                                                                                        <li><span class="fa fa-star-o"></span></li>
                                                                                                        <li><span class="fa fa-star-o"></span></li>
                                                                                                        <li><span class="fa fa-star-o"></span></li>
                                                                                                    </ul>
                                                                                                    <ul class="review-stars filled"
                                                                                                        style="width: calc(100% - 2px)">
                                                                                                        <li><span class="fa fa-star"></span></li>
                                                                                                        <li><span class="fa fa-star"></span></li>
                                                                                                        <li><span class="fa fa-star"></span></li>
                                                                                                        <li><span class="fa fa-star"></span></li>
                                                                                                        <li><span class="fa fa-star"></span></li>
                                                                                                    </ul>
                                                                                                </div>
                                                                                            </div>
                                                                                            <p class="review-title">
                                                                                                Beautiful theme -
                                                                                                Awesome plugin</p>
                                                                                            <div class="description"
                                                                                                itemprop="reviewBody">
                                                                                                <p> 5 stars for this
                                                                                                    theme too.
                                                                                                    Education WP theme
                                                                                                    brings the best LMS
                                                                                                    experience ever
                                                                                                    with super friendly
                                                                                                    UX and complete
                                                                                                    eLearning features.
                                                                                                    Really satisfied.</p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="review-container"
                                                                                        itemprop="review" itemscope
                                                                                        itemtype="http://schema.org/Review">
                                                                                        <div class="review-author">
                                                                                            <img alt="Admin bar avatar"
                                                                                                src="https://3ek5k1tux0822q3g83e30fye-wpengine.netdna-ssl.com/wp-content/uploads/learn-press-profile/1/21232f297a57a5a743894a0e4a801fc3.jpg"
                                                                                                class="avatar avatar-96 photo"
                                                                                                height="96" width="96" /></div>
                                                                                        <div class="review-text">
                                                                                            <h4 class="author-name"
                                                                                                itemprop="author">
                                                                                                admin</h4>
                                                                                            <div class="review-star">
                                                                                                <div class="review-stars-rated">
                                                                                                    <ul class="review-stars">
                                                                                                        <li><span class="fa fa-star-o"></span></li>
                                                                                                        <li><span class="fa fa-star-o"></span></li>
                                                                                                        <li><span class="fa fa-star-o"></span></li>
                                                                                                        <li><span class="fa fa-star-o"></span></li>
                                                                                                        <li><span class="fa fa-star-o"></span></li>
                                                                                                    </ul>
                                                                                                    <ul class="review-stars filled"
                                                                                                        style="width: calc(100% - 2px)">
                                                                                                        <li><span class="fa fa-star"></span></li>
                                                                                                        <li><span class="fa fa-star"></span></li>
                                                                                                        <li><span class="fa fa-star"></span></li>
                                                                                                        <li><span class="fa fa-star"></span></li>
                                                                                                        <li><span class="fa fa-star"></span></li>
                                                                                                    </ul>
                                                                                                </div>
                                                                                            </div>
                                                                                            <p class="review-title">
                                                                                                Incredible</p>
                                                                                            <div class="description"
                                                                                                itemprop="reviewBody">
                                                                                                <p> LearnPress
                                                                                                    WordPress LMS
                                                                                                    Plugin designed
                                                                                                    with flexible &
                                                                                                    scalable eLearning
                                                                                                    system in mind.
                                                                                                    This WordPress
                                                                                                    eLearning Plugin
                                                                                                    comes up with 10+
                                                                                                    addons (and
                                                                                                    counting) to extend
                                                                                                    the ability of this
                                                                                                    WordPress Learning
                                                                                                    Management System.
                                                                                                    This is incredible.</p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="review-container"
                                                                                        itemprop="review" itemscope
                                                                                        itemtype="http://schema.org/Review">
                                                                                        <div class="review-author">
                                                                                            <img alt="Admin bar avatar"
                                                                                                src="https://3ek5k1tux0822q3g83e30fye-wpengine.netdna-ssl.com/wp-content/uploads/learn-press-profile/7/9c081444f942cc8fe0ddf55631b584e2.jpg"
                                                                                                class="avatar avatar-96 photo"
                                                                                                height="96" width="96" /></div>
                                                                                        <div class="review-text">
                                                                                            <h4 class="author-name"
                                                                                                itemprop="author"> keny</h4>
                                                                                            <div class="review-star">
                                                                                                <div class="review-stars-rated">
                                                                                                    <ul class="review-stars">
                                                                                                        <li><span class="fa fa-star-o"></span></li>
                                                                                                        <li><span class="fa fa-star-o"></span></li>
                                                                                                        <li><span class="fa fa-star-o"></span></li>
                                                                                                        <li><span class="fa fa-star-o"></span></li>
                                                                                                        <li><span class="fa fa-star-o"></span></li>
                                                                                                    </ul>
                                                                                                    <ul class="review-stars filled"
                                                                                                        style="width: calc(100% - 2px)">
                                                                                                        <li><span class="fa fa-star"></span></li>
                                                                                                        <li><span class="fa fa-star"></span></li>
                                                                                                        <li><span class="fa fa-star"></span></li>
                                                                                                        <li><span class="fa fa-star"></span></li>
                                                                                                        <li><span class="fa fa-star"></span></li>
                                                                                                    </ul>
                                                                                                </div>
                                                                                            </div>
                                                                                            <p class="review-title">
                                                                                                The best LMS WordPress
                                                                                                plugin</p>
                                                                                            <div class="description"
                                                                                                itemprop="reviewBody">
                                                                                                <p> I really love the
                                                                                                    course editor in
                                                                                                    LearnPress. It is
                                                                                                    never easier when
                                                                                                    creating courses,
                                                                                                    lessons, quizzes
                                                                                                    with this one.
                                                                                                    It's the most
                                                                                                    useful LMS
                                                                                                    WordPress plugin I
                                                                                                    have ever used.
                                                                                                    Thank a lot!
                                                                                                    Testing quiz is
                                                                                                    funny, I like the
                                                                                                    sorting choice
                                                                                                    question type most.</p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="thim-course-menu-landing">
                                                            <div class="container">
                                                                <ul class="thim-course-landing-tab">
                                                                    <li role="presentation" class="course-nav-tab-overview active">
                                                                        <a href="#tab-overview">Overview</a></li>
                                                                    <li role="presentation" class="course-nav-tab-curriculum">
                                                                        <a href="#tab-curriculum">Curriculum</a></li>
                                                                    <li role="presentation" class="course-nav-tab-instructor">
                                                                        <a href="#tab-instructor">Instructor</a></li>
                                                                    <li role="presentation" class="course-nav-tab-reviews">
                                                                        <a href="#tab-reviews">Reviews</a></li>
                                                                </ul>
                                                                <div class="thim-course-landing-button">
                                                                    <div class="course-price">
                                                                        <div class="value  free-course"> Free</div>
                                                                    </div>
                                                                    <div class="lp-course-buttons">
                                                                        <form name="enroll-course" class="enroll-course form-purchase-course"
                                                                            method="post" enctype="multipart/form-data">
                                                                            <input type="hidden" name="enroll-course"
                                                                                value="5428" /> <input type="hidden"
                                                                                name="enroll-course-nonce" value="c42ba65daa" />
                                                                            <button class="lp-button button button-enroll-course">
                                                                                Take this course </button> <input type="hidden"
                                                                                name="redirect_to" value=""></form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="social_share">
                                                        <ul class="thim-social-share">
                                                            <li class="heading">Share:</li>
                                                            <li>
                                                                <div class="facebook-social"><a target="_blank" class="facebook"
                                                                        href="https://www.facebook.com/sharer.php?u=https%3A%2F%2Feducationwp.thimpress.com%2Fcourses%2Flearnpress-101%2F"
                                                                        title="Facebook"><i class="fa fa-facebook"></i></a></div>
                                                            </li>
                                                            <li>
                                                                <div class="googleplus-social"><a target="_blank" class="googleplus"
                                                                        href="https://plus.google.com/share?url=https%3A%2F%2Feducationwp.thimpress.com%2Fcourses%2Flearnpress-101%2F&amp;title=Introduction%20LearnPress%20%26%238211%3B%20LMS%20plugin"
                                                                        title="Google Plus" onclick='javascript:window.open(this.href, "", "menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600");return false;'><i
                                                                            class="fa fa-google"></i></a></div>
                                                            </li>
                                                            <li>
                                                                <div class="twitter-social"><a target="_blank" class="twitter"
                                                                        href="https://twitter.com/share?url=https%3A%2F%2Feducationwp.thimpress.com%2Fcourses%2Flearnpress-101%2F&amp;text=Introduction%20LearnPress%20%26%238211%3B%20LMS%20plugin"
                                                                        title="Twitter"><i class="fa fa-twitter"></i></a></div>
                                                            </li>
                                                            <li>
                                                                <div class="pinterest-social"><a target="_blank" class="pinterest"
                                                                        href="https://pinterest.com/pin/create/button/?url=https%3A%2F%2Feducationwp.thimpress.com%2Fcourses%2Flearnpress-101%2F&amp;description=A%20WordPress%20LMS%20Plugin%20to%20create%20WordPress%20Learning%20Management%20System.%20Turn%20your%20WP%20to%20LMS%20WordPress%20with%20Courses%2C%20Lessons%2C%20Quizzes%20%26amp%3B%20more.%20&amp;media=https%3A%2F%2Feducationwp.thimpress.com%2Fwp-content%2Fuploads%2F2015%2F11%2Fcourse-4.jpg"
                                                                        onclick="window.open(this.href); return false;"
                                                                        title="Pinterest"><i class="fa fa-pinterest-p"></i></a></div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                              
                                            </div>
                                        </div>
                                        <script type="text/javascript">
                                            function moOpenIdLogin(app_name, is_custom_app) {
                                                var base_url = '../../index.html';
                                                var request_uri = 'index.html';
                                                var http = 'https:///';
                                                var http_host = 'educationwp.thimpress.com'
                                                if (is_custom_app == 'false') {
                                                    if (request_uri.indexOf('wp-login.html') != -1) {
                                                        var redirect_url = base_url +
                                                            '/?option=getmosociallogin&app_name=';

                                                    } else {
                                                        var redirect_url = http + http_host + request_uri;
                                                        if (redirect_url.indexOf('?') != -1) {
                                                            redirect_url = redirect_url +
                                                                '&option=getmosociallogin&app_name=';
                                                        } else {
                                                            redirect_url = redirect_url +
                                                                '?option=getmosociallogin&app_name=';
                                                        }
                                                    }
                                                } else {
                                                    var current_url = window.location.href;
                                                    var cname = "redirect_current_url";
                                                    var d = new Date();
                                                    d.setTime(d.getTime() + (2 * 24 * 60 * 60 * 1000));
                                                    var expires = "expires=" + d.toUTCString();
                                                    document.cookie = cname + "=" + current_url + ";" + expires +
                                                        ";path=/"; //path = root path(/)
                                                    if (request_uri.indexOf('wp-login.html') != -1) {
                                                        var redirect_url = base_url +
                                                            '/?option=oauthredirect&app_name=';
                                                    } else {
                                                        var redirect_url = http + http_host + request_uri;
                                                        if (redirect_url.indexOf('?') != -1)
                                                            redirect_url = redirect_url +
                                                            '&option=oauthredirect&app_name=';
                                                        else
                                                            redirect_url = redirect_url +
                                                            '?option=oauthredirect&app_name=';
                                                    }
                                                }
                                                var redirect_to = jQuery('#loginform input[name="redirect_to"]').val();
                                                redirect_url = redirect_url + app_name + '&redirect_to=' +
                                                    encodeURIComponent(redirect_to);
                                                window.location.href = redirect_url;
                                            }
                                        </script>
                                        <script type="text/javascript">
                                            function moOpenIdLogin(app_name, is_custom_app) {
                                                var base_url = '../../index.html';
                                                var request_uri = 'index.html';
                                                var http = 'https:///';
                                                var http_host = 'educationwp.thimpress.com'
                                                if (is_custom_app == 'false') {
                                                    if (request_uri.indexOf('wp-login.html') != -1) {
                                                        var redirect_url = base_url +
                                                            '/?option=getmosociallogin&app_name=';

                                                    } else {
                                                        var redirect_url = http + http_host + request_uri;
                                                        if (redirect_url.indexOf('?') != -1) {
                                                            redirect_url = redirect_url +
                                                                '&option=getmosociallogin&app_name=';
                                                        } else {
                                                            redirect_url = redirect_url +
                                                                '?option=getmosociallogin&app_name=';
                                                        }
                                                    }
                                                } else {
                                                    var current_url = window.location.href;
                                                    var cname = "redirect_current_url";
                                                    var d = new Date();
                                                    d.setTime(d.getTime() + (2 * 24 * 60 * 60 * 1000));
                                                    var expires = "expires=" + d.toUTCString();
                                                    document.cookie = cname + "=" + current_url + ";" + expires +
                                                        ";path=/"; //path = root path(/)
                                                    if (request_uri.indexOf('wp-login.html') != -1) {
                                                        var redirect_url = base_url +
                                                            '/?option=oauthredirect&app_name=';
                                                    } else {
                                                        var redirect_url = http + http_host + request_uri;
                                                        if (redirect_url.indexOf('?') != -1)
                                                            redirect_url = redirect_url +
                                                            '&option=oauthredirect&app_name=';
                                                        else
                                                            redirect_url = redirect_url +
                                                            '?option=oauthredirect&app_name=';
                                                    }
                                                }
                                                var redirect_to = jQuery('#loginform input[name="redirect_to"]').val();
                                                redirect_url = redirect_url + app_name + '&redirect_to=' +
                                                    encodeURIComponent(redirect_to);
                                                window.location.href = redirect_url;
                                            }
                                        </script>
                                    </div>
                                </article>
                            </main>

                        </div>
                    </div>
                </section>


<?php echo $__env->make('front.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>



            </div>
        </div> <a href="#" id="back-to-top"> <i class="fa fa-chevron-up" aria-hidden="true"></i> </a>
    </div>
    <div id="tp_style_selector">
        <div class="tp_style_selector_container">
            <div class="tp_chameleon_customize">
                <div class="tp-buy-theme"> <a class="link-buy" href="https://themeforest.net/item/education-wordpress-theme-education-wp/14058034?ref=ThimPress&amp;utm_campaign=chameleon&amp;utm_medium=buynow&amp;utm_source=eduma"
                        title="Buy Now"><i class="fa fa-shopping-cart"></i>Buy Now</a> <a class="view-more-slider"
                        target="_blank" href="https://thimpress.com/learnpress/learnpress-premium-add-ons-request-form/?utm_source=demo&amp;utm_medium=chameleon"
                        title="Get Bundle"> <span class="text">Click Here To Redeem</span> <span class="text">Included
                            LearnPress Bundle (<i>$433</i>)</span> </a></div>
            </div>

        </div>

    </div>
    <div class="tp-preview-images"> <img src="https://updates.thimpress.com/wp-content/uploads/2017/06/eduma-demo-01.jpg"
            alt="preview image"></div>
    <div id="tp_chameleon_list_google_fonts"></div>
    <div class="gallery-slider-content"></div>
    <script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
    <script data-cfasync="false" type="text/javascript">
        window.onload = function () {
            var thim_preload = document.getElementById("preload");
            if (thim_preload) {
                setTimeout(function () {
                    var body = document.getElementById("thim-body"),
                        len = body.childNodes.length,
                        class_name = body.className.replace(/(?:^|\s)thim-body-preload(?!\S)/, '').replace(
                            /(?:^|\s)thim-body-load-overlay(?!\S)/, '');

                    body.className = class_name;
                    if (typeof thim_preload !== 'undefined' && thim_preload !== null) {
                        for (var i = 0; i < len; i++) {
                            if (body.childNodes[i].id !== 'undefined' && body.childNodes[i].id == "preload") {
                                body.removeChild(body.childNodes[i]);
                                break;
                            }
                        }
                    }
                }, 500);
            }
        };
    </script>
    <script>
        window.addEventListener('load', function () {
            /**
             * Fix issue there is an empty spacing between image and title of owl-carousel
             */
            setTimeout(function () {
                var $ = jQuery;
                var $carousel = $('.thim-owl-carousel-post').each(function () {
                    $(this).find('.image').css('min-height', 0);
                    $(window).trigger('resize');
                });
            }, 500);
        })
    </script>
    <script data-cfasync="true" type="text/javascript">
        (function ($) {
            "use strict";
            $(document).on('click',
                'body:not(".logged-in") .enroll-course .button-enroll-course, body:not(".logged-in") form.purchase-course:not(".guest_checkout") .button',
                function (e) {
                    if ($(window).width() > 767) {
                        if ($('.thim-login-popup .login').length > 0) {
                            e.preventDefault();
                            $('#thim-popup-login #loginform [name="redirect_to"]').val(
                                'indexaada.html?enroll-course=5428');
                            $('.thim-login-popup .login').trigger('click');
                        }
                    } else {
                        e.preventDefault();
                        $(this).parent().find('[name="redirect_to"]').val(
                            '../../account/index1a35.html?redirect_to=https%3A%2F%2Feducationwp.thimpress.com%2Fcourses%2Flearnpress-101%2F%3Fenroll-course%3D5428'
                        );
                        var redirect = $(this).parent().find('[name="redirect_to"]').val();
                        window.location = redirect;
                    }
                    if ($('#thim-popup-login .register').length > 0) {
                        $('#thim-popup-login .register').each(function () {
                            var link = $(this).attr('href'),
                                new_link = link +
                                '&redirect_to=localhost/courses/learnpress-101/?enroll-course=5428';
                            $(this).prop('href', new_link);
                        });
                    }
                });
        })(jQuery);
    </script>
    <script type="text/javascript">
        var c = document.body.className;
        c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
        document.body.className = c;
    </script>
    <style type="text/css" media="all" id="siteorigin-panels-layouts-footer">
        /* Layout tc-megamenu-7682 */
        #pgc-tc-megamenu-7682-0-0,
        #pgc-tc-megamenu-7682-0-1 {
            width: 31%;
            width: calc(31% - (0.69 * 30px))
        }

        #pl-tc-megamenu-7682 #panel-tc-megamenu-7682-0-0-0,
        #pl-tc-megamenu-7682 #panel-tc-megamenu-7682-0-1-0,
        #pl-tc-megamenu-7682 #panel-tc-megamenu-7682-0-2-0 {}

        #pgc-tc-megamenu-7682-0-2 {
            width: 38%;
            width: calc(38% - (0.62 * 30px))
        }

        #pl-tc-megamenu-7682 .so-panel {
            margin-bottom: 30px
        }

        #pl-tc-megamenu-7682 .so-panel:last-child {
            margin-bottom: 0px
        }

        @media (max-width:767px) {

            #pg-tc-megamenu-7682-0.panel-no-style,
            #pg-tc-megamenu-7682-0.panel-has-style>.panel-row-style {
                -webkit-flex-direction: column;
                -ms-flex-direction: column;
                flex-direction: column
            }

            #pg-tc-megamenu-7682-0 .panel-grid-cell {
                margin-right: 0
            }

            #pg-tc-megamenu-7682-0 .panel-grid-cell {
                width: 100%
            }

            #pgc-tc-megamenu-7682-0-0,
            #pgc-tc-megamenu-7682-0-1 {
                margin-bottom: 30px
            }

            #pl-tc-megamenu-7682 .panel-grid-cell {
                padding: 0
            }

            #pl-tc-megamenu-7682 .panel-grid .panel-grid-cell-empty {
                display: none
            }

            #pl-tc-megamenu-7682 .panel-grid .panel-grid-cell-mobile-last {
                margin-bottom: 0px
            }
        }

        /* Layout w57e9cc2c86af4 */
        #pgc-w57e9cc2c86af4-0-0 {
            width: 33.3%;
            width: calc(33.3% - (0.667 * 30px))
        }

        #pl-w57e9cc2c86af4 #panel-w57e9cc2c86af4-0-0-0,
        #pl-w57e9cc2c86af4 #panel-w57e9cc2c86af4-0-0-1,
        #pl-w57e9cc2c86af4 #panel-w57e9cc2c86af4-0-1-0,
        #pl-w57e9cc2c86af4 #panel-w57e9cc2c86af4-0-2-0,
        #pl-w57e9cc2c86af4 #panel-w57e9cc2c86af4-0-3-0,
        #pl-w57e9cc2c86af4 #panel-w57e9cc2c86af4-0-4-0 {}

        #pgc-w57e9cc2c86af4-0-1,
        #pgc-w57e9cc2c86af4-0-2,
        #pgc-w57e9cc2c86af4-0-3,
        #pgc-w57e9cc2c86af4-0-4 {
            width: 16.675%;
            width: calc(16.675% - (0.83325 * 30px))
        }

        #pl-w57e9cc2c86af4 .so-panel {
            margin-bottom: 30px
        }

        #pl-w57e9cc2c86af4 .so-panel:last-child {
            margin-bottom: 0px
        }

        @media (max-width:767px) {

            #pg-w57e9cc2c86af4-0.panel-no-style,
            #pg-w57e9cc2c86af4-0.panel-has-style>.panel-row-style {
                -webkit-flex-direction: column;
                -ms-flex-direction: column;
                flex-direction: column
            }

            #pg-w57e9cc2c86af4-0 .panel-grid-cell {
                margin-right: 0
            }

            #pg-w57e9cc2c86af4-0 .panel-grid-cell {
                width: 100%
            }

            #pgc-w57e9cc2c86af4-0-0,
            #pgc-w57e9cc2c86af4-0-1,
            #pgc-w57e9cc2c86af4-0-2,
            #pgc-w57e9cc2c86af4-0-3 {
                margin-bottom: 30px
            }

            #pl-w57e9cc2c86af4 .panel-grid-cell {
                padding: 0
            }

            #pl-w57e9cc2c86af4 .panel-grid .panel-grid-cell-empty {
                display: none
            }

            #pl-w57e9cc2c86af4 .panel-grid .panel-grid-cell-mobile-last {
                margin-bottom: 0px
            }
        }
    </style>
    <script type='text/javascript'>
        var wpcf7 = {
            "apiSettings": {
                "root": "https:\/\/educationwp.thimpress.com\/wp-json\/contact-form-7\/v1",
                "namespace": "contact-form-7\/v1"
            },
            "recaptcha": {
                "messages": {
                    "empty": "Please verify that you are not a robot."
                }
            },
            "cached": 0
        };
    </script>
    <script type='text/javascript'>
        var wc_add_to_cart_params = {
            "ajax_url": "\/wp-admin\/admin-ajax.php",
            "wc_ajax_url": "\/?wc-ajax=%%endpoint%%",
            "i18n_view_cart": "View cart",
            "cart_url": "https:\/\/educationwp.thimpress.com\/cart\/",
            "is_cart": "",
            "cart_redirect_after_add": "no"
        };
    </script>
    <script type='text/javascript'>
        var woocommerce_params = {
            "ajax_url": "\/wp-admin\/admin-ajax.php",
            "wc_ajax_url": "\/?wc-ajax=%%endpoint%%"
        };
    </script>
    <script type='text/javascript'>
        var wc_cart_fragments_params = {
            "ajax_url": "\/wp-admin\/admin-ajax.php",
            "wc_ajax_url": "\/?wc-ajax=%%endpoint%%",
            "cart_hash_key": "wc_cart_hash_8653f1d19ec105315cba116db9d7fe2e",
            "fragment_name": "wc_fragments_8653f1d19ec105315cba116db9d7fe2e"
        };
    </script>
    <script type='text/javascript'>
        var _wpUtilSettings = {
            "ajax": {
                "url": "\/wp-admin\/admin-ajax.php"
            }
        };
    </script>
    <script type='text/javascript'>
        var WPEMS = {
            "gmt_offset": "0",
            "current_time": "Nov 11, 2018 12:38:00 +0000",
            "l18n": {
                "labels": ["Years", "Months", "Weeks", "Days", "Hours", "Minutes", "Seconds"],
                "labels1": ["Year", "Month", "Week", "Day", "Hour", "Minute", "Second"]
            },
            "ajaxurl": "https:\/\/educationwp.thimpress.com\/wp-admin\/admin-ajax.php",
            "something_wrong": "Something went wrong",
            "register_button": "c20eb61364"
        };
    </script>
    <script type='text/javascript'>
        var thim_js_translate = {
            "login": "Username",
            "password": "Password",
            "close": "Close"
        };
    </script>
    <script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js?ver=3.0.22'></script>
    <script type='text/javascript'>
        WebFont.load({
            google: {
                families: ['Roboto Slab:300,400,700', 'Roboto:300,300i,400,400i,500,500i,700,700i']
            }
        });
    </script>
    <script type='text/javascript'>
        var lpGlobalSettings = {
            "url": "https:\/\/educationwp.thimpress.com\/courses\/learnpress-101\/",
            "siteurl": "https:\/\/educationwp.thimpress.com",
            "ajax": "https:\/\/educationwp.thimpress.com\/wp-admin\/admin-ajax.php",
            "theme": "eduma",
            "localize": {
                "button_ok": "OK",
                "button_cancel": "Cancel",
                "button_yes": "Yes",
                "button_no": "No"
            }
        };
    </script>
    <script type='text/javascript'>
        var lpCheckoutSettings = {
            "ajaxurl": "https:\/\/educationwp.thimpress.com",
            "user_waiting_payment": null,
            "user_checkout": "",
            "i18n_processing": "Processing",
            "i18n_redirecting": "Redirecting",
            "i18n_invalid_field": "Invalid field",
            "i18n_unknown_error": "Unknown error",
            "i18n_place_order": "Place order"
        };
    </script>
    <script type='text/javascript'>
        var lpProfileUserSettings = {
            "processing": "Processing",
            "redirecting": "Redirecting",
            "avatar_size": {
                "width": 150,
                "height": 150
            }
        };
    </script>
    <script type='text/javascript'>
        var lpCourseSettings = [""];
    </script>
    <script type='text/javascript'>
        var lpQuizSettings = [];
    </script>
    <div class="tp_chameleon_overlay">
        <div class="tp_chameleon_progress">
            <div class="tp_chameleon_heading">Processing!</div>
        </div>
    </div>
    <script type="text/javascript">
        var tp_chameleon_url_stylesheet = '../../wp-content/themes/eduma/style-1.html';
        var tp_chameleon_url_admin_ajax = '../../wp-admin/admin-ajax.html';
        var tp_chameleon_wp_nonce = 'e39f6066ef';
        var tp_chameleon_primary_color = 'rgb(255, 182, 6)';
        var tp_chameleon_selector_wrapper_box = '.content-pusher';
        var tp_chameleon_class_boxed = 'boxed-area';
        var tp_chameleon_src_patterns = '../../wp-content/plugins/tp-chameleon/images/patterns/index.html';
        var tp_chameleon_setting = {
            layout: tp_chameleon_getCookie('tp_chameleon_layout'),
            pattern_type: tp_chameleon_getCookie('tp_chameleon_pattern_type'),
            pattern_src: tp_chameleon_getCookie('tp_chameleon_pattern_src'),
            primary_color: tp_chameleon_getCookie('tp_chameleon_primary_color'),
            primary_color_rgb: tp_chameleon_getCookie('tp_chameleon_primary_color_rgb'),
            body_font: tp_chameleon_getCookie('tp_chameleon_body_font'),
            body_font_code: tp_chameleon_getCookie('tp_chameleon_body_font_code'),
            heading_font: tp_chameleon_getCookie('tp_chameleon_heading_font'),
            heading_font_code: tp_chameleon_getCookie('tp_chameleon_heading_font_code')
        };



        var tp_chameleon_site_url = '../../index.html';

        function tp_chameleon_setCookie(cname, cvalue, exdays) {
            var d = new Date();
            d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
            var expires = "expires=" + d.toUTCString();
            document.cookie = cname + "=" + cvalue + "; " + expires;
        }

        function tp_chameleon_deleteCookie(cname) {
            var d = new Date();
            d.setTime(d.getTime() + (24 * 60 * 60 * 1000));
            var expires = "expires=" + d.toUTCString();
            document.cookie = cname + "=; " + expires;
        }

        function tp_chameleon_deleteAllCookie() {
            var all_cookie = [
                'tp_chameleon_layout',
                'tp_chameleon_pattern_type',
                'tp_chameleon_pattern_src',
                'tp_chameleon_primary_color',
                'tp_chameleon_primary_color_rgb',
                'tp_chameleon_body_font',
                'tp_chameleon_body_font_code',
                'tp_chameleon_heading_font',
                'tp_chameleon_heading_font_code'
            ];

            for (var i = 0; i < all_cookie.length; i++) {
                tp_chameleon_deleteCookie(all_cookie[i]);
            }
        }

        function tp_chameleon_getCookie(cname) {
            var name = cname + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') c = c.substring(1);
                if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
            }

            return '';
        }

        function tp_chameleon_set_first_visit() {
            tp_chameleon_setCookie('tp_chameleon_first_visit', 1, 1);
        }

        function tp_chameleon_check_first_visit() {
            return !(tp_chameleon_getCookie('tp_chameleon_first_visit') == '1');
        }

        jQuery(document).ready(function ($) {
            var $preview = $('.tp-preview-images');

            $('.tp_demo').hover(
                function (event) {
                    var url_prewview = $(this).attr('data-preview');
                    if (url_prewview) {
                        $preview.find('img').attr('src', url_prewview);
                        $preview.show();
                    }
                },
                function () {
                    $preview.hide();
                }
            );

            $('.tp_demo').mousemove(function (event) {
                var y = (event.clientY);
                $preview.css('top', y - 250);
            });

            function tp_chameleon_open() {
                tp_chameleon_set_first_visit();
                $('#tp_style_selector').addClass('show').animate({
                    right: '0px'
                }, 'medium');
                $('#tp_style_selector .open').hide();
                $('#tp_style_selector .close').show();
            }

            function tp_chameleon_close() {
                $('#tp_style_selector').removeClass('show').animate({
                    right: '-300px'
                }, 'medium');
                $('#tp_style_selector .close').hide();
                $('#tp_style_selector .open').show();
            }

            function tp_form_newsletter_show() {
                tp_chameleon_set_first_visit();
                $(window).scroll(function () {
                    if ($(this).scrollTop() > 200) {
                        $('.tp-email-form .button-email').addClass('bounceInUp animated active');
                        var cookie_name = 'hide_form_email1';
                        $.cookie(cookie_name, '1', {
                            expires: 3,
                            path: '/'
                        });
                    }
                });
            }

            function tp_change_background_pattern(url_pattern) {
                var $body = $('body');
                $body.removeClass('tp_background_image');
                $body.addClass('tp_background_pattern');
                $body.css('background-image', 'url("' + url_pattern + '")')
            }

            function tp_change_background_image(url_image) {
                var $body = $('body');
                $body.removeClass('tp_background_pattern');
                $body.addClass('tp_background_image');
                $body.css('background-image', 'url("' + url_image + '")')
            }

            function tp_chameleon_change_layout_wide() {
                tp_chameleon_setCookie('tp_chameleon_layout', 'wide', 1);

                var $body = $('body');
                $('.tp-change-layout').removeClass('active');
                $('.tp-change-layout.layout-wide').addClass('active');
                $('#tp_style_selector .boxed-mode').slideUp(300);
                $(tp_chameleon_selector_wrapper_box).removeClass(tp_chameleon_class_boxed);
                $body.css('background-image', 'none');
            }

            function tp_chameleon_change_layout_boxed() {
                tp_chameleon_setCookie('tp_chameleon_layout', 'boxed', 1);
                $('.tp-change-layout').removeClass('active');
                $('.tp-change-layout.layout-boxed').addClass('active');
                $('#tp_style_selector .boxed-mode').slideDown(300);
                $(tp_chameleon_selector_wrapper_box).addClass(tp_chameleon_class_boxed);
            }

            function tp_chameleon_change_background_pattern(pattern_src) {
                tp_chameleon_setCookie('tp_chameleon_pattern_src', pattern_src, 1);
                tp_chameleon_setCookie('tp_chameleon_pattern_type', 'pattern', 1);
                var pattern_url = tp_chameleon_src_patterns + pattern_src;
                tp_change_background_pattern(pattern_url);
            }

            function tp_chameleon_change_background_image(pattern_src) {
                tp_chameleon_setCookie('tp_chameleon_pattern_src', pattern_src, 1);
                tp_chameleon_setCookie('tp_chameleon_pattern_type', 'image', 1);
                var pattern_url = tp_chameleon_src_patterns + pattern_src;
                tp_change_background_image(pattern_url);
            }

            var $body_font = '<style id="tp_chameleon_body_font" type="text/css"></style>';
            var $heading_font = '<style id="tp_chameleon_heading_font" type="text/css"></style>';
            var $stylesheet = '<link id="tp_chameleon_stylesheet" type="text/css" rel="stylesheet">';

            var $tp_head = $('head');
            $tp_head.append($stylesheet);

            var $tp_body = $('body');
            $tp_body.append($body_font);
            $tp_body.append($heading_font);

            if (tp_chameleon_setting.layout == 'wide') {
                tp_chameleon_change_layout_wide();
            }
            if (tp_chameleon_setting.layout == 'boxed') {
                tp_chameleon_change_layout_boxed();

                if (tp_chameleon_setting.pattern_type == 'pattern' && tp_chameleon_setting.pattern_src != '') {
                    tp_chameleon_change_background_pattern(tp_chameleon_setting.pattern_src);
                    $('.tp_pattern[data-src="' + tp_chameleon_setting.pattern_src + '"]').addClass('active');
                }

                if (tp_chameleon_setting.pattern_type == 'image' && tp_chameleon_setting.pattern_src != '') {
                    tp_chameleon_change_background_image(tp_chameleon_setting.pattern_src);
                    $('.tp_image[data-src="' + tp_chameleon_setting.pattern_src + '"]').addClass('active');
                }
            }

            $('.tp-chameleon-clear').click(function (event) {
                event.preventDefault();
                tp_chameleon_deleteAllCookie();
                document.location.reload();
            });

            $('.tp-btn.tp-change-layout').click(function (event) {
                event.preventDefault();

                if ($(this).hasClass('layout-wide')) {
                    tp_chameleon_change_layout_wide();

                } else {
                    tp_chameleon_change_layout_boxed();

                }
            });

            $('.tp_pattern').click(function (event) {
                event.preventDefault();
                $('.tp_pattern').removeClass('active');
                $('.tp_image').removeClass('active');
                $(this).addClass('active');
                var pattern_src = $(this).attr('data-src');
                tp_chameleon_change_background_pattern(pattern_src);
            });

            $('.tp_image').click(function (event) {
                event.preventDefault();
                $('.tp_pattern').removeClass('active');
                $('.tp_image').removeClass('active');
                $(this).addClass('active');
                var pattern_src = $(this).attr('data-src');
                tp_chameleon_change_background_image(pattern_src);
            });

            /**
             * Open/Close box
             */
            $('#tp_style_selector .close').click(function (e) {
                e.preventDefault();
                tp_chameleon_close();
            });

            $('#tp_style_selector .open').click(function (e) {
                e.preventDefault();
                tp_chameleon_open();
            });

            $(window).load(function () {
                var $btn = $('.tp-chameleon-btn-buy');
                $btn.animate({
                    bottom: $btn.attr('data-bottom')
                }, 500);

                //Set view-more-slider button
                $('.view-more-slider .text:last-child').hide();
                setInterval(function () {
                    $('.view-more-slider .text:first-child').hide();
                    $('.view-more-slider .text:last-child').show();
                    setTimeout(function () {
                        $('.view-more-slider .text:first-child').show();
                        $('.view-more-slider .text:last-child').hide();
                    }, 2000);
                }, 4500);

            });

            /**
             * Check firt visit
             */
            if (tp_chameleon_check_first_visit()) {
                //set visit is second
                setTimeout(tp_chameleon_open, 5000);
            } else {
                $('#tp_style_selector').click(function (event) {
                    tp_chameleon_set_first_visit();
                });
            }
            if (tp_chameleon_check_first_visit()) {
                tp_form_newsletter_show();
            }


            $('.tp-email-form .button-email').click(function (e) {
                e.preventDefault();
                $('.tp-email-form .email-form-popup').addClass('show');
            });
            $('.tp-email-form .email-form-popup .close-popup').click(function (e) {
                e.preventDefault();
                $('.tp-email-form .email-form-popup').removeClass('show');
                $('.tp-email-form .button-email').addClass('hide');
            });

            $('.tp-email-form .email-form-popup .email-form-subscribe form ').submit(function (e) {
                e.preventDefault();
                var form = $(this);
                var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                form.append('<div class="loading"><i class="fa fa-spinner fa-pulse"></i></div>');
                if (form.find('input[type="email"]').val()) {
                    $.ajax({
                        type: "POST",
                        url: 'https://preview.thimpress.com/m/mailster/subscribe',
                        data: form.serialize(),
                        complete: function (xhr, textStatus) {
                            form.find('.loading').remove();
                            form.append(
                                '<div class="message-success">Please check your inbox or spam folder for confirmation email!</div>'
                            );
                            setTimeout(function () {
                                form.find('.message-success').remove();
                            }, 2000);
                        }
                    });
                } else {
                    form.find('.loading').remove();
                    form.append('<div class="message-error">Please enter email address</div>');
                    setTimeout(function () {
                        form.find('.message-error').remove();
                    }, 2000);
                    form.find('input[type="email"]').addClass('error');
                }


            });

            $('.tp-email-form .email-form-popup .email-form-subscribe form input[type="email"]').focus(function () {
                $(this).removeClass('error');
            });
        });
    </script>
    <script type="text/javascript" src="<?php echo e(asset('assets/slider/ahmed2.js')); ?>" ></script>

    <script type="text/javascript" defer src="https://3ek5k1tux0822q3g83e30fye-wpengine.netdna-ssl.com/wp-content/cache/autoptimize/1/js/autoptimize_c9deaa1b9ac89fc63d0d2d70ce9703ec.js"></script>
</body>
<!-- Mirrored from educationwp.thimpress.com/courses/learnpress-101/ by  **** Website Copier/3.x [XR&CO'2014], Sun, 11 Nov 2018 12:40:53 GMT -->

</html>

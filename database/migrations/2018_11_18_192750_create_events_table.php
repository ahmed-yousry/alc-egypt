<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('event_partner')->unsigned();
           $table->foreign('event_partner')->references('id')->on('partners')
              ->onUpdate('cascade')
              ->onDelete('cascade');
              $table->integer('start_time');
              $table->integer('finish_time');

                $table->string('title');
              $table->string('address');
              $table->string('image');

              $table->string('dscription');









            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
